package StepDefinations.PrePost_Actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.beans.factory.annotation.Autowired;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.cucumber.listener.Reporter;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.ReportEvents;
import FrameworkSource.global.reporter.ReportGenerator;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PrePostActions {

	public ClsInitialize objInit;

/*	@Before("@Android")
	public void fnSetADevice() {
		ClsInitialize.strDevice = "Android";
	}

	@Before("@IOS")
	public void fnSetIDevice() {
		ClsInitialize.strDevice = "IOS";
	}*/

	@Before
	public void func_Before(Scenario strTestCase)
			throws IOException, InterruptedException, ClassNotFoundException, SQLException {

		//new ReportEvents(strTestCase.getName().toString());

		try {
			String sysTagExecuted = System.getProperty("testconfig");
			ClsInitialize.strDevice = sysTagExecuted.split("-")[0].replace("@", "");
			if (ClsInitialize.strDevice.equalsIgnoreCase("ios") || ClsInitialize.strDevice.equalsIgnoreCase("android"))
				ClsInitialize.strPlatform = "Mobile";
			else
				ClsInitialize.strPlatform = "Web";
			ClsInitialize.strLanguage = sysTagExecuted.split("-")[1].replace("@", "");
			Reporter.loadXMLConfig(new File(ClsInitialize.reportConfigPath));
		} catch (Exception e) {
			if (strTestCase.getSourceTagNames().contains("@French")
					|| strTestCase.getSourceTagNames().contains("@Fr")) {
				ClsInitialize.strLanguage = "French";
			} else {
				ClsInitialize.strLanguage = "English";
			}

			/*
			 * Capturing the Platform for the execution from the Tags
			 */
			if (strTestCase.getSourceTagNames().contains("@Web") || strTestCase.getSourceTagNames().contains("@web")) {
				ClsInitialize.strPlatform = "Web";
			} else {
				ClsInitialize.strPlatform = "Mobile";
			}

		}

		/*
		 * Capturing the Language for the execution from the Tags
		 */

		/*
		 * Intialising the common class for: Initiating Platform Objects Reading
		 * Properties file Reading Configuration CommonData Execution time
		 * Initiating Report Objects
		 */
		objInit = new ClsInitialize(strTestCase.getName(), strTestCase.getId().split(";")[0]);

	}

	@After
	public void func_After(Scenario strTestCase) throws IOException, InterruptedException {

		
		/*
		 * Closing the objects for the test cases Updating the DB Metrices
		 * HashMap
		 */
		ClsInitialize.fnCloseObjects(strTestCase.getStatus().toString());

		/*
		 * Appending the snapshot of the failure in the extent Reports
		 */
		if (strTestCase.isFailed()) {

			String screenshotName = strTestCase.getName().replaceAll(" ", "_");
			try {
				File sourcePath = null, destinationPath = null;
				if (ClsInitialize.strPlatform.equalsIgnoreCase("web"))
					sourcePath = ((TakesScreenshot) ClsInitialize.iobjBrowser.driver).getScreenshotAs(OutputType.FILE);
				else
					sourcePath = ((TakesScreenshot) ClsInitialize.iobjDevice.driver).getScreenshotAs(OutputType.FILE);
				destinationPath = new File(System.getProperty("user.dir") + "\\target\\cucumber-reports\\screenshots\\"
						+ screenshotName + ".jpg");
				FileUtils.copyFile(sourcePath, destinationPath);

			} catch (Exception eMis) {
			}
		}

	}

}
