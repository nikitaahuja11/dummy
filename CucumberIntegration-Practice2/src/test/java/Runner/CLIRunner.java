package Runner;

import java.io.File;

import com.cucumber.listener.Reporter;

import FrameworkSource.global.Intialize.ClsInitialize;
import cucumber.api.cli.Main;

public class CLIRunner {

	public static void main(String[] args) throws Throwable {

		//ClsDeviceFetch.fnGetDevice();
		String strFeatureFilePath = System.getProperty("user.dir") + "\\src\\test\\resources\\FeatureFiles";
		String[] arConfig = new String[] { "@android-english","@iOS-English", "@web-english"/*, "@web-french"*/ };

		for (String config : arConfig) {
			new CLIRunner().multiRunner(strFeatureFilePath, config);
		}

	}

	private void multiRunner(String featureFilePath, String config) throws Exception {
		String[] argv = null;
		ClassLoader contextClassLoader;

		switch (config.toLowerCase().trim()) {
		case "@android-english":
			System.setProperty("testconfig", "android-english");
			argv = new String[] { "--glue", "StepDefinations", // the package
																// which
																// contains the
																// glue classes
					"-t", "@Android-English",
					"-p","com.cucumber.listener.ExtentCucumberFormatter:target/ATF_Report_AndroidEnglish.html",
					featureFilePath };

			break;

		case "@android-french":
			System.setProperty("testconfig", "android-french");
			argv = new String[] { "--glue", "StepDefinations", // the package
																// which
																// contains the
																// glue classes
					"-t", "@Android-French",
					"-p","com.cucumber.listener.ExtentCucumberFormatter:target/ATF_Report_AndroidFrench.html",
					featureFilePath };
			
			break;

		case "@ios-english":
			System.setProperty("testconfig", "ios-english");
			argv = new String[] { "--glue", "StepDefinations", // the package
																// which
																// contains the
																// glue classes
					"-t", "@iOS-English",
					"-p","com.cucumber.listener.ExtentCucumberFormatter:target/ATF_Report_IOSEnglish.html",
					featureFilePath };
			
			break;

		case "@ios-french":
			System.setProperty("testconfig", "ios-french");
			argv = new String[] { "--glue", "StepDefinations", // the package
																// which
																// contains the
																// glue classes
					"-t", "@iOS-French",
					"-p","com.cucumber.listener.ExtentCucumberFormatter:target/ATF_Report_IOSFrench.html",
					featureFilePath };
			
			break;

		case "@web-english":
			System.setProperty("testconfig", "web-english");
			argv = new String[] { "--glue", "StepDefinations", // the package
																// which
																// contains the
																// glue classes
					"-t", "@Web-English", 
					"-p","com.cucumber.listener.ExtentCucumberFormatter:target/ATF_Report_WebEnglish.html",
					featureFilePath };
			
			break;

		case "@web-french":
			System.setProperty("testconfig", "web-french");
			argv = new String[] { "--glue", "StepDefinations", // the package
																// which
																// contains the
																// glue classes
					"-t", "@Web-French",
					"-p","com.cucumber.listener.ExtentCucumberFormatter:target/ATF_Report_WebEnglish.html",
					featureFilePath };
			
			break;
		}

		try {
			contextClassLoader = Thread.currentThread().getContextClassLoader();
			Main.run(argv, contextClassLoader);
			
		} catch (Exception e) {

		}
	}
}
