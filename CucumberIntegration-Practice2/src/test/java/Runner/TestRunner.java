package Runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import com.hpe.alm.octane.OctaneCucumber;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.model.Test;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.cucumber.listener.ExtentCucumberFormatter;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import FrameworkSource.*;
import FrameworkSource.global.DBMetrices.DB_UpdateMetrics;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.almbridge.ALMBridge;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.HashMap;

@RunWith(Cucumber.class)

@CucumberOptions(
		
		features = "src/test/resources/FeatureFiles",
		glue = {"StepDefinations.PrePost_Actions","StepDefinations.Mobile_Tests","StepDefinations.Web_Tests"},
		dryRun = false, // check if the step definitions has the code. it should be marked false
		monochrome = true,//Readable Console Report 
		plugin = { "html:target/cucumber-html-report",
				        "json:target/cucumber.json", "pretty:target/cucumber-pretty.txt",
				        "usage:target/cucumber-usage.json", "junit:target/cucumber-results.xml",
				        "com.cucumber.listener.ExtentCucumberFormatter:target/ATF_Report.html"},
		strict = false, //if strict option is set to false then at execution time if cucumber encounters any undefined/pending steps then cucumber does not fail the execution and undefined steps are skipped and BUILD is SUCCESSFUL
		snippets = SnippetType.CAMELCASE,
		tags = {"@TC_01"} // {","} - OR {"",""} - AND
		)

public class TestRunner {
	
		@BeforeClass
		public static void fnBeforeClass()
		{
			
			
			/*
			 * Deleting the execution folder before the start of the execution
			 */
			FrameworkSource.global.reporter.ReportEvents.DeleteFolder(new File(System.getProperty("user.dir")+ "//ExecutionReports"));
			
			/*Initialising the DB Metrices Data HashMap*/
			ClsInitialize.mapDBMetricesData = new HashMap<String,String>();
		}
		
		
		
		@AfterClass
		public static void fnAfterClass() throws ParseException, IOException, URISyntaxException
		{
			
			
			
			/*
			 * Closing the Automation WebDriver and Mobile Driver
			 * Closing the friver.exe by calling quit function
			 * Freeing the memory by assigning it to null
			 */
			try{
				ClsInitialize.objWeb.automationWebDriver.close();
				ClsInitialize.objWeb.automationWebDriver.quit();
				ClsInitialize.objWeb.automationWebDriver=null;
			}catch(Exception eCloseWeb)
			{}
			
			/*
			 * Closing the Automation Mobile Driver
			 * Closing the friver.exe by calling quit function
			 * Freeing the memory by assigning it to null
			 */
			
			try{
				ClsInitialize.objMobile.automationMobileDriver.close();
				ClsInitialize.objMobile.automationMobileDriver.quit();
				ClsInitialize.objMobile.automationMobileDriver = null;
			}catch(Exception eCloseMobile)
			{}
			
			/*Creating an Extent Report using Cucumber API*/
			
			Reporter.loadXMLConfig(new File(ClsInitialize.reportConfigPath));
			
			/*Updating the Database Metrices*/
			//if(ClsInitialize.properties.getProperty("Debug").toString().trim().equalsIgnoreCase("No"))
			boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().
				    getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
		    if(!isDebug)
				new DB_UpdateMetrics().updateDBResult("true");
			
			/*Opening the extended Report based on the inputs in Config File*/
			if(ClsInitialize.properties.getProperty("OpenReportAfterExecution").toString().trim().equalsIgnoreCase("Yes"))
			{
				try{
					URI oURL = new File (System.getProperty("user.dir") + "\\target\\ATF_Report.html").toURI();
					Desktop.getDesktop().browse(oURL);
				}catch(Exception e)
				{
				}
				
			}
			
			try{
				if(ClsInitialize.objMobile.automationTempWebDriver != null)
				{
					ClsInitialize.objMobile.automationTempWebDriver.close();
					ClsInitialize.objMobile.automationTempWebDriver.quit();
				}
				if(ClsInitialize.objMobile.automationMobileDriver != null)
				{
					ClsInitialize.objMobile.automationMobileDriver.close();
					ClsInitialize.objMobile.automationMobileDriver.quit();
				}
				
				if(ClsInitialize.objWeb.automationWebDriver != null)
				{
					ClsInitialize.objWeb.automationWebDriver.close();
					ClsInitialize.objWeb.automationWebDriver.quit();
				}
				
			}catch(Exception objCloseException)
			{
				
			}
			
			ALMBridge.SendStatus("false");
			
			/*if(clsInitalize.properties.getProperty("CleanObjects").toString().trim().equalsIgnoreCase("Yes"))
			{
				try{
					Runtime.getRuntime().exec(new String[] {"cmd /c start cmd.exe /K \"taskkill /f /im excel.exe\" "});
					Runtime.getRuntime().exec(new String[] {"cmd", "taskkill /f /im", "chromedriver.exe"});
				}catch(Exception eCloseObject)
				{}
			}*/
			
			/*
			 * Freeing the memory of objects called in class Initalize
			 */
			ClsInitialize.reader = null;
			ClsInitialize.properties = null;
			ClsInitialize.capabilities = null;
			ClsInitialize.strTestCaseName = null;
			ClsInitialize.strFilePathOR = null;
			ClsInitialize.strPlatform = null;
			ClsInitialize.strDevice = null;
			
			ClsInitialize.objWeb = null;
			ClsInitialize.objMobile = null;
			ClsInitialize.iobjPage = null;
			ClsInitialize.iobjBrowser = null;
			ClsInitialize.iobjDevice = null;
			ClsInitialize.iobjApp = null;
			
			
		}
}
