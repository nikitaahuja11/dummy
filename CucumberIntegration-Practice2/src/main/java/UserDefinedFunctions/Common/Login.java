package UserDefinedFunctions.Common;

import java.io.IOException;

import FrameworkSource.UI.*;
import FrameworkSource.UI.Common.Button;
import FrameworkSource.UI.Common.TextBox;
import FrameworkSource.UI.Mobile.App;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.UI.Web.Page;
import FrameworkSource.global.Intialize.ClsInitialize;

public class Login extends ClsInitialize{
	
	public Login() throws IOException,InterruptedException
	{
		if(strPlatform.equalsIgnoreCase("Web"))
		{
			iobjPage.SetCurrentPage("Login");
		}
		else
		{
			iobjApp.SetCurrentApp("Login");
		}
	}

	
	public Login(Page oobjPage) throws IOException, InterruptedException
	{
		oobjPage.SetCurrentPage("Login");
	}
	
	public Login(App oobjApp) throws IOException, InterruptedException
	{
		oobjApp.SetCurrentApp("Login");
	}
	
	
	public boolean func_EnterCredentials(String strUserName, String strPassword) throws IOException, InterruptedException
	{
		
		new TextBox("txt_Username").SendKeys(strUserName);
		try
		{
			iobjDevice.HideKeyboard();
			
		}
		catch(Exception e)
		{
		}
		new TextBox("txt_Password").SendKeys(strPassword);
		try
		{
			iobjDevice.HideKeyboard();
			
		}
		catch(Exception e)
		{
		}
		new Button("SignInBtn").Click();
		
		return false;
		
	}
	
	public boolean func_ClickNext() throws IOException, InterruptedException
	{
		boolean blnResult = false;
		
		new Button("btn_Next").Click();
		
		return blnResult;
	}
}
