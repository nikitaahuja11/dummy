package FrameworkSource.global.DBMetrices;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;



import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.ReportEvents;
import FrameworkSource.global.reporter.ReportGenerator;


public class DB_UpdateMetrics 
{
	
	static String Computername;
	static String UserName;	
	static Connection conn = null;
    static Statement stmt = null;
    static int rs ;
    // Constant for Database URL
    //public static String DB_URL = "SQC8RG09P.ca.sunlife\\v4p813";
    public static String DB_URL = "SQC6R12P.sunlifecorp.com\\V4P612";
    
   
    // Constant for Database Username
    public static String SQL_User = "IVNfHXl2ITI=dGNvZQ==";
    
    // Constant for Database Password
    public static String SQL_Password = "J04zQSpcms0=VGUkVENvRTE=";
    
    static String LOB_Type;
	static String TestType;
	static String DB_USER,DB_PASSWORD;
	
	@SuppressWarnings("restriction")
	private static String getaccess(String SQL_credential) throws IOException
	{
		String SQLString = null;
		if (SQL_credential.length() > 12) {
			String cipher = SQL_credential.substring(12);
			BASE64Decoder decoder = new BASE64Decoder();
			SQLString =  new String(decoder.decodeBuffer(cipher));
		}
		return SQLString;
	}

    /***
     * Initiate DB Connection
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     */
	public static void InitiateDBconn() throws SQLException, ClassNotFoundException, IOException
	{
		String ClassName = new Exception().getStackTrace()[2].getClassName();
		BufferedReader reader;
		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			DB_USER = getaccess(SQL_User);
			DB_PASSWORD = getaccess(SQL_Password);
			String dbURL = "jdbc:sqlserver://"+DB_URL+";user="+DB_USER+";password="+DB_PASSWORD+"";
			conn = DriverManager.getConnection(dbURL);			
			UserName = System.getProperty("user.name"); 
			Computername = getComputerName();	
			LOB_Type = ClsInitialize.properties.getProperty("LOB_Type");
			TestType = ClsInitialize.properties.getProperty("TestType");
		}
		catch(Exception e)
		{
			ReportEvents.Error("Initiating DB Connection", e);
		}
	}
	
	/*
	 * Getting the computerName on which the execution is taking place
	 */
	private static String getComputerName() throws IOException
	{
		String hostname = "Unknown";
	
		try
		{
		    InetAddress addr;
		    addr = InetAddress.getLocalHost();
		    hostname = addr.getHostName();
		}
		catch (UnknownHostException ex)
		{
			ReportEvents.Error("Hostname can not be resolved", ex);
		}
		return hostname;
	}
	
	
	
	/**
	 * 
	 * @param status
	 * @throws ParseException
	 * @throws IOException
	 */
	public static void updateDBResult(String status) throws ParseException, IOException
	{
		int runtimes = 0;
		String value = null;
		Timestamp startingTime = null, EndingTime = null;
		Calendar cal = null;
		String strStatus = "";
		
		try{
			if(status=="true")
			{
				ReportEvents.FlagScreenShot = "false";
				ReportEvents.FlagStepScreenShot = "false";
				DB_UpdateMetrics.InitiateDBconn();
				HashMap<String, String> Resultrec =  ClsInitialize.mapDBMetricesData;	//Return the Test record details with execution status
					
			for(Entry<String, String> entry : Resultrec.entrySet()) 
			{			    
			    	String key = entry.getKey();
			    	value = entry.getValue();
			    	String[]  array = value.split(";");
			    	runtimes = Integer.parseInt(array[3]);
			    	strStatus = array[4];
			    	try{
			    	
				    	Timestamp StarttimeStamp = getStartingTimestamp(array[0].trim()); //Store the Starting Time from application.html session start time
						//Convert start sec to  Test execution Starting time
				    	cal = Calendar.getInstance();
						cal.setTimeInMillis(StarttimeStamp.getTime());
						startingTime = new Timestamp( cal.getTime().getTime());
						
						//Convert Total sec to  Test execution Ending time
						Timestamp EndTimeStamp = getStartingTimestamp(array[1].trim()); //Store the Starting Time from application.html session start time
						cal = Calendar.getInstance();
						cal.setTimeInMillis(EndTimeStamp.getTime());
						EndingTime = new Timestamp( cal.getTime().getTime());	
		
			    	}catch(Exception eCalendar)
			    	{
			    		ReportEvents.Error("Adding the TimeStamp Error::", eCalendar);
			    	}

					try 
					{
						//update to Database
						UpdateEecutionMetrics(key,runtimes,startingTime,EndingTime,Computername,UserName,array[2].trim(),strStatus);
				
					} 
					catch (SQLException eUpdateExecutionMetrices)
					{
						ReportEvents.Error("Error in Updating the Metrices::", eUpdateExecutionMetrices);
					}	
				}
			}
			else
				System.out.println("No update to Database");
		}catch(Exception eUpdateDB)
		{
			ReportEvents.Error("Error in Updating the Database::", eUpdateDB);
		}
		
		  
	}

	/***
	 * Update the DB with Test Results
	 * @param testCaseName
	 * @param runTimes
	 * @param startTime
	 * @param endTime
	 * @param Computername
	 * @param UserName
	 * @param status
	 * @throws SQLException
	 * @throws IOException
	 */

	public static boolean UpdateEecutionMetrics(String testCaseName, int runTimes, Date startTime, Date endTime,String Computername, String UserName, String status,String strIterationStatus) throws SQLException, IOException
	{
		boolean updatecheck = false;
		String ClassName = new Exception().getStackTrace()[2].getClassName();
		try
		{
		if(conn != null)
		{
		String sqlQuery = "INSERT INTO [TCoE_MetricsHistory].[dbo].[ASTestExecutionData](LOB,LoginID,ALMID,ALMDomain,ALMProject,SourceMachineName,ExecutionMachineName,ScriptName,Status,Iterations,DTStarTime,DTEndTime,TestStatus,IterationStatus,TestType) "
				+ "VALUES('"+LOB_Type+"','"+UserName+"',null,null,null,'"+Computername+"',null,'"+testCaseName+"','End',"+runTimes+",'"+startTime+"','"+endTime+"','"+status+"','"+strIterationStatus+"','"+TestType+"');";
				
				//String sqlQuery = "Select * From [TCoE_MetricsHistory].[dbo].[ASTestExecutionData]" ;
		//System.out.println(sqlQuery);
		stmt = conn.createStatement();
       // rs = stmt.executeUpdate(sqlQuery); 
		stmt.execute(sqlQuery);
        ReportEvents.Done(ClassName,"Data Base updated with execution Results");
		}
		else
		{
			ReportEvents.Done(ClassName,"No Database connection");
		}
		}
		catch (Exception e)
		{
			updatecheck = false;
	        			
		}
		return updatecheck;        
			
	}

	
	/**
	 * Convert the String Date into TimeStamp
	 * @param CurrDate
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp getStartingTimestamp(String CurrDate) throws ParseException
	{
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date currentdate=sdf1.parse(CurrDate);
        Timestamp StarttimeStamp = new Timestamp(currentdate.getTime());
        System.out.println("Starting Time = "+StarttimeStamp );
		return StarttimeStamp;
        
	}
}
