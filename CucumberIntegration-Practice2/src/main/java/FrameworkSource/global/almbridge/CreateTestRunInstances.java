package FrameworkSource.global.almbridge;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import FrameworkSource.global.restutilities.*;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.junit.Assert;

/**
 * This class creates Test Run entity corresponding to Test Instance
 */
public class CreateTestRunInstances {

	/**
	* The function creates Test Run instance in ALM
	* @param value takes few string value like serverURL, domain, project, username, password, status and int values like id, testid
	*/
    public static void createRunInstance(final String serverUrl,
                                    final String domain, final String project, String username,
                                    String password, int id, int testid, String status) throws Exception {

        RestConnector con =
                RestConnector.getInstance().init(
                        new HashMap<String, String>(),
                        serverUrl,
                        domain,
                        project);

        Authentication login =
                new Authentication();

        boolean loginResult = login.login(username, password);
        Assert.assertTrue("failed to login", loginResult);
        
        
        Map<String, String> requestHeaders2 = new HashMap<String, String>();
        String qcsession = con.buildUrl("rest/site-session");
        con.httpPost(qcsession, null, requestHeaders2);

        String requirementsUrl = con.buildEntityCollectionUrl("run");
        
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + 
        		"<Entity Type=\"run\">\n" + 
        		"  <Fields>\n" + 
        		"    <Field Name=\"status\">\n" + 
        		"      <Value>Not Completed</Value>\n" + 
        		"    </Field>\n" + 
        		"    <Field Name=\"name\">\n" + 
        		"      <Value>TestRun</Value>\n" + 
        		"    </Field>\n" + 
        		"    <Field Name=\"owner\">\n" + 
        		"      <Value>"+username+"</Value>\n" + 
        		"    </Field>\n" + 
        		"    <Field Name=\"testcycl-id\">\n" + 
        		"      <Value>"+id+"</Value>\n" + 
        		"    </Field>\n" + 
        		"    <Field Name=\"test-id\">\n" + 
        		"      <Value>"+testid+"</Value>\n" +     
        		"    </Field>\n" + 
        		"    <Field Name=\"subtype-id\">\n" + 
        		"      <Value>hp.qc.run.MANUAL</Value>\n" +     
        		"    </Field>\n" + 
        		"  </Fields>\n" + 
        		"</Entity>";

     
        /* Now do the same only this time using an object, and not string xml.
        (Though we do build the object from the xml, we could have instantiated
        it differently, theoretically.)
        The reason we build it with post and not createEntity, is that when
        posting, the returned value is an xml representation */
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/xml");
        requestHeaders.put("Accept", "application/xml");
        Response postedEntityResponse =
                con.httpPost
                        (requirementsUrl, xml.getBytes(),
                                requestHeaders);
        
        //Now here we have to get the ID of previously testrun
        int runid =XMLParseID(postedEntityResponse.toString());
       // System.out.println(postedEntityResponse.toString());
        
       // System.out.println(postedEntityResponse.getStatusCode());
        
//        System.out.println(runid);


        
        Exception failure = postedEntityResponse.getFailure();
        if (failure != null) {
            System.out.println(failure);
        }

        
        //Now put request to update the status
        String requirementsUrl1 = con.buildEntityCollectionUrl("run");
        requirementsUrl1 = requirementsUrl1 + "/"+runid;
        
        String xml1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + 
        		"<Entity Type=\"run\">\n" + 
        		"  <Fields>\n" + 
        		"    <Field Name=\"status\">\n" + 
        		"      <Value>"+status+"</Value>\n" + 
        		"    </Field>\n" + 
        		"    <Field Name=\"draft\">\n" + 
        		"      <Value>N</Value>\n" +     
        		"    </Field>\n" + 
        		"  </Fields>\n" + 
        		"</Entity>";
        
        Map<String, String> requestHeaders1 = new HashMap<String, String>();
        requestHeaders1.put("Content-Type", "application/xml");
        requestHeaders1.put("Accept", "application/xml");
        Response postedEntityResponse1 =
                con.httpPut
                        (requirementsUrl1, xml1.getBytes(),
                                requestHeaders1);

        login.logout();
    }

    private RestConnector con;

    public CreateTestRunInstances() {
        con = RestConnector.getInstance();
    }

	/**
	* The function fetches id of a test run
	* @param value takes XML as string data type
	*/    
    private static int XMLParseID(String data)
	{
		// Create an instance of SAXBuilder
        SAXBuilder builder = new SAXBuilder();
        int runid = 0;
        int flag = 0;

        
        try {
        	// Tell the SAXBuilder to build the Document object from the
            // InputStream supplied.
			Document document = builder.build(
			        new ByteArrayInputStream(data.getBytes()));
			
			// Get our xml document root element which equals to the
            // <root> tag in the xml document.
            Element classElement = document.getRootElement();
            List<Element> list1 = classElement.getChildren();
            
            //System.out.println(list.size());
            

                    for(int i=0; i<list1.size(); i++)
                    {
                    	Element element1 = list1.get(i); 
                        if(element1.getName().equalsIgnoreCase("Fields")) //If it is field
                        {
                        	List<Element> list2 = element1.getChildren(); //children of fields
                            for(int j=0; j<list2.size(); j++)
                            {
                           	 Element element2 = list2.get(j); //Now working on Field
                             Attribute attribute2 =  element2.getAttribute("Name"); //Checking the name attribute of Field
                             if(element2.getName().equalsIgnoreCase("Field"))
                             {
                            	 if(attribute2.getValue().equalsIgnoreCase("id"))
                             	{
                      //      		 System.out.println(element2.getChild("Value").getText()); //Now fetch the test-id
                            		// System.out.println(element2.getChild("Value").getText());
                            		 runid = Integer.parseInt(element2.getChild("Value").getText());
                            		 //System.out.println(element2.getChild("Value").getText());
                            		 flag=1;
                            		 break;
                            		 
                            		// System.out.println(testid);
                             	}
                            	 
                            	 
	 
                             	} //End of Field operations 
                           	 
                            }// End of Fields children i.e Field
                        } //End of Fields

             //System.out.println("Value of flag "+flag);
             if(flag==1)
             {
            	 break;
             }
             else
             flag= 0;
            } //End of root element

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
        return runid;
	}

}