package FrameworkSource.global.almbridge;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import FrameworkSource.global.reporter.ReportGenerator;
/**
 * ALMBridge class consists of functions related to ALM(Application Lifecycle Management)
 */
public class ALMBridge {
	
	/**
	* The function sends the test results to ALM
	* @param value takes a string value named status. It can be either set to true or false
	*/
	public static void SendStatus(String status)
	{
			if(status.equalsIgnoreCase("true"))
			{
			//Retrieve serverUrl,domain,project,username and password
			String serverURL = "http://qcalm.sunlifecorp.com/qcbin";
			String domain = "";
			String project = "";
			String username = "";
			String password = "";
			
			String almstatus="";
			
			
			//Retrieve the Test-Set ID from a centralized location
			
			//Steps:
			//Read the ExecutionConfig entire data in 2-D array
			try {
			String DataTable = System.getProperty("user.dir") + "\\src\\test\\resources\\Data\\RunClassData.xlsx";
			File file = new File(DataTable);
			
			FileInputStream inputStream = new FileInputStream(file);
		    Workbook workbook = null;
		    workbook = WorkbookFactory.create(inputStream);
		    Sheet ExecutionSheet = workbook.getSheet("ExecutionConfig");
		    DataFormatter formatter = new DataFormatter();
		    int rowCount = ExecutionSheet.getLastRowNum()-ExecutionSheet.getFirstRowNum();
		    System.out.println("No. of rows in ExecutionConfig sheet: "+(rowCount+1));
		    
		    //Fetching details like username, password etc.
	    	Row details = ExecutionSheet.getRow(1);
	    	domain = details.getCell(3).getStringCellValue();
	    	System.out.println("Domain: "+domain);
	    	project = details.getCell(4).getStringCellValue();
	    	System.out.println("Project: "+project);
	    	username = details.getCell(5).getStringCellValue();
	    	System.out.println("Username: "+username);
	    	password = details.getCell(6).getStringCellValue();
  
		    //System.out.println(rowCount);
		    //int colCount = ExecutionSheet.getRow(0).getLastCellNum();
		    int colCount = 3; //Only interested in only two columns
		    //System.out.println(colCount);
		    String[][] AllValues = new String[rowCount][colCount]; //Entire sheet as 2-D array
		    for (int i = 0; i < rowCount; i++) 
		    {
		    	//Row row = ExecutionSheet.getRow(i+1);
		        for (int j = 0; j < 3; j++) 
		        {
		        	  Cell cell = ExecutionSheet.getRow(i+1).getCell(j);
		        	 AllValues[i][j] =formatter.formatCellValue(cell);
		        	 //System.out.println(AllValues[i][j]);
		      //  	 System.out.println(row.getCell(j).getStringCellValue());
		        }
		    }
		    //System.out.println(AllValues.length);
			
			//Check for which field the symbol is Y and fetch the Suite and SetID
		    //String Suite ="";
		    List<String> TestSuite = new ArrayList<String>();
		    List<Integer> TestSetID = new ArrayList<Integer>();
		    //int TestSetId = 0;
		    for(int i=0;i<AllValues.length;i++)
		    {
		    	if(AllValues[i][2].equalsIgnoreCase("Y"))
		    	{
		    		TestSuite.add(AllValues[i][0]); //TestSuite List
		    		//Suite = AllValues[i][0];
		    		TestSetID.add(Integer.parseInt(AllValues[i][1])); //TestSetID List
		    		//TestSetId = Integer.parseInt(AllValues[i][1]); 
		    	}
		    }
		    
		    //Now start parsing the TestSuite list
		    
		    for(int k=0;k<TestSuite.size();k++)
	        {
		    
		    System.out.println("Test Set Name: "+TestSuite.get(k));
		    System.out.println("Test Set ID: "+TestSetID.get(k));

			//Then from the 2-D array returned from Report Generator
			//Insert a loop for classes that ran
			String testruns[][] = ReportGenerator.Generate1();
			String teststatus = "";
			String testname = "";
			
			int id=0;
			int testid = 0;
			
			String almtestcase ="";
			
		    Sheet MappingSheet = workbook.getSheet("ALMMapping");
		    DataFormatter formatter2 = new DataFormatter();
		    int rowCount1 = MappingSheet.getLastRowNum()-MappingSheet.getFirstRowNum();
		    System.out.println("No. of rows in ALMMapping sheet: "+(rowCount1+1));
		    int flag = 0;

			
				
				//Fetch the mapping test case name of ALM based on test script
	/*	    
		  //start reading the test runs one by one
			for(int i=0; i<testruns[0].length;i++)
			{
				String testname = testruns[0][i];  //Test case name
			//	System.out.println("Test Script Name: "+testruns[0][i]);
				String teststatus = testruns[1][i];  //Test case name
*/
			    
			    //start parsing row by row
			    for (int j = 0; j < rowCount1; j++) {

			           Row row = MappingSheet.getRow(j+1);
			           String thirdRow = formatter2.formatCellValue(row.getCell(2));
			           String firstRow = formatter2.formatCellValue(row.getCell(0));
			           teststatus="";
			           //System.out.println(firstRow);
			           if(thirdRow.contains(TestSuite.get(k)))
			           {
			        	   
			        	   //Now find the status of test script
			        	   for(int m=0; m<testruns[0].length;m++)
			   			{
			        		  if(testruns[m][0].equals(firstRow)) 
			        		  {
			        			teststatus =  testruns[m][1];
			        			break;
			        		  }
			   			}
			        	   
			        	   
			        	   //firstRow.trim().equals(testname)
			        	   flag = 1;
				          // System.out.println(firstRow+" Data from excel");
			        	   //fetch the test case name of alm
			        	 //  testname = formatter2.formatCellValue(row.getCell(0));
			        	   testname = firstRow;
			        	   System.out.println("TestScript Name: "+testname);
			        	   almtestcase = formatter2.formatCellValue(row.getCell(1));
			        	   System.out.println("Mapped TestCase Name in ALM: "+almtestcase);
			        	   almtestcase = almtestcase.trim() + " [1]";
						    //Now we have received the test case name of alm
						    //Then pass to TestInstyance class and fetch the id
							//Pass the test case name to the function and retrieve the corresponding id
							String idcombined = ReadTestInstances.readExample(serverURL, domain, project, username, password,TestSetID.get(k),almtestcase);
							//System.out.println(idcombined);
							if(idcombined.equals(""))
							{
								System.out.println("Details not found. Please check details once again.");
								System.exit(0);
							}
							else
							{
								//Break id and testid based on comma
								id =  Integer.parseInt(idcombined.split(",")[0]);
								System.out.println("ID of TestCase name in ALM: "+id);
								testid =  Integer.parseInt(idcombined.split(",")[1]);	
							}
							
							if(teststatus.equalsIgnoreCase("Pass"))
							{
								almstatus="Passed";
								System.out.println("TestCase status is: "+almstatus);
							}
							
							else if(teststatus.equalsIgnoreCase("Fail"))
							{
								almstatus="Failed";
								System.out.println("TestCase status is: "+almstatus);
							}
							//Now create a test run on basis of pass and fail
							CreateTestRunInstances.createRunInstance(serverURL, domain, project, username, password,id,testid,almstatus);	

							
							//break;
			           } //If suite is found
			           
			    	} //End of for loop for parsing row by row
			    
			    if(flag==0)
			    {
			    	System.out.println("No tests found with SuiteParticipation as "+TestSuite.get(k));
			    }
			        
			    flag = 0;

			   // } //End of all test runs with status 
			    
			}//End of TestSuite List	
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.toString());
			}
			
			}
		
	}
	


}
