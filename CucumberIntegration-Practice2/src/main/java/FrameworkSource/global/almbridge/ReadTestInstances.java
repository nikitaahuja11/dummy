package FrameworkSource.global.almbridge;

import java.io.ByteArrayInputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.junit.Assert;
import FrameworkSource.global.restutilities.*;

/**
 * This class shows how to read test entities as XML or into objects.
 */
public class ReadTestInstances {

	/**
	* The function fetches id and test-id of a test instance
	* @param value takes few string value like serverURL, domain, project, username, password, almtestcase and int values like testsetid
	*/
    public static String readExample(final String serverUrl, final String domain,
                            final String project, String username, String password,int testsetid,String almtestcase)
            throws Exception {

        RestConnector con =
                RestConnector.getInstance().init(
                        new HashMap<String, String>(),
                        serverUrl,
                        domain,
                        project);

        Authentication login =
                new Authentication();



        boolean loginState = login.login(username, password);
        Assert.assertTrue("login failed.", loginState);
        
        Map<String, String> requestHeaders2 = new HashMap<String, String>();
        String qcsession = con.buildUrl("rest/site-session");
        con.httpPost(qcsession, null, requestHeaders2);        

        String requirementsUrl = con.buildEntityCollectionUrl("test-instance");
     // String requirementsUrl = con.buildEntityCollectionUrl("run");
       // System.out.println(requirementsUrl);
       // String requirementsUrl = "http://qcalm.sunlifecorp.com/qcbin/rest/domains/DEFAULT/projects/MC_POC/customization/entities/defect/fields?required=true";
       // String requirementsUrl = "http://qcalm.sunlifecorp.com/qcbin/rest/domains/DEFAULT/projects/MC_POC/customization/entities/run/fields?required=true";

        // Use the writing example to generate an entity so that we can read it.
        // Go over this code to learn how to create new entities.
        //String requirementsUrl =       writeExample.createEntity(requirementsUrl,                        Constants.entityToPostXml);

        //System.out.println(b.toString());
 
        StringBuilder query = new StringBuilder();
       // query.append("query={cycle-id[503];test-id[193]}");  //193 is test-id    365 is id
       query.append("query={contains-test-set.id["+testsetid+"]}");
       query.append("&fields=test-id,name");
        //or test-instances?query={cycle-id[42666]}
       //There are total of 6 test runs and their id start with 909 with last one having id 914
       Map<String, String> requestHeaders = new HashMap<String, String>();
       requestHeaders.put("Accept", "application/xml");
       
        Response serverResponse =
                con.httpGet(requirementsUrl, query.toString(), requestHeaders);
        Assert.assertEquals(
                "failed obtaining response"
                        + requirementsUrl,
                HttpURLConnection.HTTP_OK,
                serverResponse.getStatusCode());

        String listFromCollectionAsXml = serverResponse.toString();

       // System.out.print("ListFromCollectionXML: " + "\n");
      //  System.out.println(listFromCollectionAsXml);
        
        //Now parse the xml and return test-id
        String testid = XMLParse(listFromCollectionAsXml,almtestcase);
    //    System.out.println(testid);

     
        //cleanup
       // writeExample.deleteEntity(requirementsUrl);
        login.logout();
		//return testid;
		return testid;
    }
    
	/**
	* The function fetches id and testid of XML passed.
	* @param value takes XML and test case name as string data type
	*/  
    private static String XMLParse(String data, String almtestcase)
	{
		// Create an instance of SAXBuilder
        SAXBuilder builder = new SAXBuilder();
        int testid = 0;
        int flag = 0;
        String idString="";

        
        try {
        	// Tell the SAXBuilder to build the Document object from the
            // InputStream supplied.
			Document document = builder.build(
			        new ByteArrayInputStream(data.getBytes()));
			
			// Get our xml document root element which equals to the
            // <root> tag in the xml document.
            Element classElement = document.getRootElement();
            List<Element> list = classElement.getChildren();
            
            //System.out.println(list.size());
            
            
            for (int temp = 0; temp < list.size(); temp++) {    
                Element element = list.get(temp); 
                
               // Attribute attribute =  element.getAttribute("Type");
                
                if(element.getName().equalsIgnoreCase("Entity"))
                {
                    //System.out.println("Parent is "+element.getName()); //Entity
                    
                    List<Element> list1 = element.getChildren(); //children of Entity
                    for(int i=0; i<list1.size(); i++)
                    {
                    	Element element1 = list1.get(i); 
                        if(element1.getName().equalsIgnoreCase("Fields")) //If it is field
                        {
                        	List<Element> list2 = element1.getChildren(); //children of fields
                            for(int j=0; j<list2.size(); j++)
                            {
                           	 Element element2 = list2.get(j); //Now working on Field
                             Attribute attribute2 =  element2.getAttribute("Name"); //Checking the name attribute of Field
                             if(element2.getName().equalsIgnoreCase("Field"))
                             {
                            	 if(attribute2.getValue().equalsIgnoreCase("id"))
                             	{
                      //      		 System.out.println(element2.getChild("Value").getText()); //Now fetch the test-id
                            		// System.out.println(element2.getChild("Value").getText());
                            		 //testid = Integer.parseInt(element2.getChild("Value").getText());
                            		 
                            		 idString = element2.getChild("Value").getText();
                            		// System.out.println(testid);
                             	}
                            	 
                            	 else if(attribute2.getValue().equalsIgnoreCase("test-id"))
                              	{
                       //      		 System.out.println(element2.getChild("Value").getText()); //Now fetch the test-id
                             		// System.out.println(element2.getChild("Value").getText());
                             		 //testid = Integer.parseInt(element2.getChild("Value").getText());
                             		 if(flag==1)
                             		 {
                             		 idString = idString + ","+ element2.getChild("Value").getText();
                             		 break;
                             		 }
                             		// System.out.println(testid);
                              	}
                            	
                            	 else if(attribute2.getValue().equalsIgnoreCase("name"))
                             	{
                      //      		 System.out.println(element2.getChild("Value").getText()); 
                            		 //Now fetch the test case name and match it with expected
                            		 
                            		 if(element2.getChild("Value").getText().equals(almtestcase))
                            		 {
                            			// System.out.println(element2.getChild("Value").getText());
                            			flag=1; //We go the required value 
                               		 //break;
                            		 }
                            		                             		 
                             	}
	 
                             	} //End of Field operations 
                           	 
                            }// End of Fields children i.e Field
                        } //End of Fields
                    } // End of Entity children

                }// End of entity
             //System.out.println("Value of flag "+flag);
             if(flag==1)
             {
            	 break;
             }
             else
             flag= 0;
            } //End of root element

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
        if(!idString.contains(",")) {
        	idString = "";
        }
        return idString;
	}
    
}