package FrameworkSource.global.Intialize;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.steadystate.css.parser.ParseException;

import FrameworkSource.UI.Mobile.App;
import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.global.reporter.ReportEvents;
import FrameworkSource.global.restutilities.APIClient;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class ClsIntializeMobile implements CommonImplementation{

	public static AppiumDriver<WebElement> automationMobileDriver=null;
	public static AppiumDriver<WebElement> driver_old;
	static String capabilities_old;
	public static Device deviceTest = null;
	public static App appTest = null;
	private static String appType = null;
	public static WebDriver automationTempWebDriver;
	static String  previous_device = null;
	static APIClient mc = new APIClient();
	
	ClsIntializeMobile() throws IOException
	{
		appType =  ClsInitialize.func_GetDevice() ;
	}
	
	public static AppiumDriver fnGetAutomationMobileDriver() throws IOException, InterruptedException
	{
		if(automationMobileDriver == null)
		{
			deviceTest = fnInitializeDevice();
			automationMobileDriver = fnInitiateMobileDriver();
		}
		/*else
		{
			appTest.ResetApp();
		}*/
		return automationMobileDriver;
	}
	
	public static AppiumDriver fnInitiateMobileDriver() throws IOException, InterruptedException
	{
		
		if(automationMobileDriver == null)
		{
			//InitiateApp(clsInitalize.mapCommonDataMobile.get("Device_ID").toString(), appType, clsInitalize.mapCommonDataMobile.get("Browser").toString(), clsInitalize.mapCommonDataMobile.get("URL").toString());
			InitiateApp(ClsInitialize.mapCommonDataMobile.get("Device_ID").toString(), appType, ClsInitialize.mapCommonDataMobile.get("AppType").toString());
		}
		return automationMobileDriver;
	}
	
	public static Device fnInitializeDevice() throws IOException, InterruptedException
	{
		if (deviceTest == null)
		{
			deviceTest = new Device();
			appTest = new App(deviceTest);
			//appTest.ResetApp();
			automationMobileDriver = fnInitiateMobileDriver();
			deviceTest.driver = automationMobileDriver;
			deviceTest.OS = appType;
			System.out.println("Completed Intialization");
		}
		return deviceTest;
	}
	
	//protected static void InitiateApp(String deviceudid,String AppType, String browserType, String url) throws InterruptedException, IOException
	protected static void InitiateApp(String deviceudid,String AppType, String AppCast) throws InterruptedException, IOException
	{	
	
		long pageLoadTimeout = 40;
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		capabilities_old = ClsInitialize.capabilities.toString();
		try{
			
			ClsInitialize.properties = ClsInitialize.fngetProperties();
			ClsInitialize.mobile_properties = ClsInitialize.fngetMobileProperties();
			ClsInitialize.capabilities = ClsInitialize.func_GetCapabilities();
			// for Native App and Hybrid App case
			if(AppCast.equalsIgnoreCase("App") )
			{	  
				ClsInitialize.capabilities.setCapability("udid", deviceudid);
				switch(AppType.toLowerCase())
				{
					case "android":
						ClsInitialize.capabilities.setCapability("platformName", "Android");
						ClsInitialize.capabilities.setCapability("appPackage", ClsInitialize.mobile_properties.getProperty("PackageID_Android").trim());
						ClsInitialize.capabilities.setCapability("appActivity",ClsInitialize.mobile_properties.getProperty("LaunchActivity_Android").trim());
						if(!ClsInitialize.capabilities.toString().equals(capabilities_old) || automationMobileDriver == null)
						{
							if(ClsInitialize.mobile_properties.getProperty("InstallApplication").equalsIgnoreCase("True") && !deviceudid.equals(previous_device))
							{
								InstallApp(deviceudid);
							}
							try{
								automationMobileDriver = new AndroidDriver(new URL(host+"/wd/hub"), ClsInitialize.capabilities); 
								driver_old = automationMobileDriver;
								ReportEvents.Done(callerClassName+":Device", "Android Device is Launched : "+ deviceudid);	
								
							   }
							catch(Exception e)
							{
								System.out.println("Initialization Error:: Device id: "+deviceudid + e.getMessage());
							}
						}		
						else
						{
							System.out.println("capabilites are already same");
							automationMobileDriver = driver_old;
						}
						break;
						
					case "ios":
						ClsInitialize.capabilities.setCapability("platformName", "iOS");
						ClsInitialize.capabilities.setCapability("bundleId", ClsInitialize.mobile_properties.getProperty("BundleID_IOS").trim());
						ClsInitialize.capabilities.setCapability("simpleIsVisibleCheck", false);
						if(!ClsInitialize.capabilities.toString().equals(capabilities_old) || automationMobileDriver == null)
						{
							if(ClsInitialize.mobile_properties.getProperty("InstallApplication").equalsIgnoreCase("True") && !deviceudid.equals(previous_device))
							{
								InstallApp(deviceudid);
							}
							try{
								automationMobileDriver = new IOSDriver(new URL(host+"/wd/hub"), ClsInitialize.capabilities);	
								driver_old = automationMobileDriver;
								System.out.println(callerClassName+":Device " + " iOS Device is Launched : "+ deviceudid);	
							   }
							catch(Exception e)
								{
								e.printStackTrace();
								}
						}		
						else
						{
							System.out.println("capabilites are already same");
							automationMobileDriver = driver_old;
						}
						break;
					
					
					default: 
						System.out.println(callerClassName+":Device " + " Invalid device type : "+ AppType);
						System.out.println("Platform Name is not valid.");
						System.exit(0);
				}
			
				automationMobileDriver.manage().timeouts().implicitlyWait(55, TimeUnit.SECONDS); 
				//fnLaunchDevice();
				automationTempWebDriver = mc.launchDevice(deviceudid);
				new WebDriverWait(automationTempWebDriver, pageLoadTimeout).until(
				          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		
			}
			// For Web App Case
			else if(AppCast.equalsIgnoreCase("Web") )
			{
				ClsInitialize.capabilities.setCapability("browserName", ClsInitialize.mapCommonDataMobile.get("Browser").toString());
				ClsInitialize.capabilities.setCapability("udid", deviceudid);

				switch(AppType.toLowerCase())
				{
					case "android":
						if(!ClsInitialize.capabilities.toString().equals(capabilities_old) || automationMobileDriver == null)
						{
							try{
								automationMobileDriver = new AndroidDriver(new URL(host+"/wd/hub"), ClsInitialize.capabilities);
								driver_old = automationMobileDriver;
								System.out.println(callerClassName+":Device " +  " Android Device is Launched : "+deviceudid);	
							   }
							catch(Exception e)
								{
								e.printStackTrace();
								}
						}		
						else
						{
							System.out.println("capabilites are already same");
							automationMobileDriver = driver_old;
						}
						break;
					
					
					case "ios":
						if(!ClsInitialize.capabilities.toString().equals(capabilities_old) || automationMobileDriver == null )
						{
							try{
								automationMobileDriver = new IOSDriver(new URL(host+"/wd/hub"), ClsInitialize.capabilities);	
								driver_old = automationMobileDriver;
								System.out.println(callerClassName+":Device" + "iOS Device is Launched : " + deviceudid);	
							 	}
							catch(Exception e)
								{
								e.printStackTrace();
								}
						}		
						else
						{
							System.out.println("Initiate Mobile Device" +  "capabilites are already same");
							automationMobileDriver = driver_old;
						}
						break;
					
					default: 
						System.out.println(callerClassName+":Device " + "Invalid device type : "+ AppType);
						System.out.println("Platform Name is not valid.");
						System.exit(0);
				}
				automationMobileDriver.manage().timeouts().implicitlyWait(55, TimeUnit.SECONDS); 
				automationTempWebDriver = mc.launchDevice(deviceudid);
				new WebDriverWait(automationTempWebDriver, pageLoadTimeout).until(
				          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
				automationMobileDriver.get(ClsInitialize.mapMasterURL.get(ClsInitialize.mapCommonDataMobile.get("Environment").toString()).toString());
				
			}
			
			else
			{
				System.out.println("Either Browser or URL value is missing in Environment sheet");
				Assert.fail();
			}	
			previous_device = deviceudid;
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Device", e);
			//System.out.println(e.toString());
			Assert.fail();
		}
	}
	
	
	public static void fnLaunchDevice() throws InterruptedException, IOException
	{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\browserdrivers\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
		    options.addArguments("--app=http://sv67299.sunlifecorp.com:8081"); 
		    options.addArguments("disable-infobars");
		    automationTempWebDriver = new ChromeDriver(options); 
		    automationTempWebDriver.manage().window().setSize(new Dimension(700,1000));
		    automationTempWebDriver.navigate().to("http://sv67299.sunlifecorp.com:8081/integration/#/login");
		    automationTempWebDriver.findElement(By.xpath(".//input[@test-id='login-emailField']")).sendKeys(ClsInitialize.mobile_properties.getProperty("MC_Username").trim());
			//driver1.findElement(By.xpath("//*[@id=\'workspace\']/div/form/div/div/input[2]")).sendKeys(properties.getProperty("MC_Password").trim());
		    automationTempWebDriver.findElement(By.xpath(".//input[@test-id='login-passwordField']")).sendKeys(ClsInitialize.mobile_properties.getProperty("MC_Password").trim());
		    automationTempWebDriver.findElement(By.xpath(".//button[@test-id='login-button']/span")).submit();
			Thread.sleep(2000);
			automationTempWebDriver.navigate().to("http://sv67299.sunlifecorp.com:8081/integration/?locale=en#/remote?deviceId="+ClsInitialize.mapCommonDataMobile.get("Device_ID").toString());
			Thread.sleep(1000);
			//driver1.findElement(By.xpath("//*[@id=\"workspace\"]/div/div[2]/div[1]/div[2]")).click();

}
  
	
	public static boolean fnCloseMobile()
	{
		boolean blnResult = false;
		try{
			
			AppiumDriver<WebElement> automationMobileDriver=null;
			Device deviceTest = null;
			App appTest = null;
			String appType = null;
			WebDriver automationTempWebDriver;
			blnResult = true;
		}catch(Exception objExceptionClose)
		{
			blnResult = false;
		}
		return blnResult;
	}
	
	public static void InstallApp(String deviceUdid) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		JSONParser parser = new JSONParser();
		Object obj;
		String OS = null;
		
		 Properties properties = ClsInitialize.fngetMobileProperties();
		
		String username =  properties.getProperty("MC_Username");
		String password = properties.getProperty("MC_Password");
		String app_android = properties.getProperty("Application_Android");
		String build_android =  properties.getProperty("Build_Android");
		String version_android =  properties.getProperty("Version_Android");
		String app_iOS = properties.getProperty("Application_iOS");
		String build_iOS = properties.getProperty("Build_iOS");
		String version_iOS =  properties.getProperty("Version_iOS");
		
		APIClient client = new APIClient(username, password);
		
		//Finding the OS of the device
		String device = client.deviceContent();
		try {
			 obj = parser.parse(device);
			 JSONArray jsonArray =  (JSONArray) obj;
	         JSONObject object;
	         int flag=0;
	            for(int i=0; i<jsonArray.size(); i++){
	                object = (JSONObject)jsonArray.get(i);
	                if((boolean)object.get("udid").toString().equalsIgnoreCase(deviceUdid))
               	 	{
	                flag=1;
               		 OS = (String)object.get("platformName");
               		 System.out.println("OS for the device UDID "+deviceUdid+" is "+OS);
 	        		ReportEvents.Done(callerClassName+":Browser","OS for the device UDID "+deviceUdid+" is "+OS);
               	 	}
	            }
	            
	            if(flag==0)
	            {
	            	System.out.println("Incorrect device UDID "+deviceUdid);
	        		ReportEvents.Done(callerClassName+":Browser","Incorrect device UDID "+deviceUdid);
	            }
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			ReportEvents.Error(callerClassName+":Browser", e);
		}
		
		String apps = client.apps();
		String fileName = null,appPackage=null;
		Long counter = null;
	    LinkedHashMap<Long,String> appwithCounter = new LinkedHashMap<>();
	    LinkedHashMap<Long,String> packagewithCounter = new LinkedHashMap<>();
        try {         
            obj = parser.parse(apps);
            JSONObject jsonObject =  (JSONObject) obj;
            JSONObject object;            
            JSONArray jsonArray = (JSONArray) jsonObject.get("data");
            
            for(int i=0; i<jsonArray.size(); i++){            	
                object = (JSONObject)jsonArray.get(i);
                if(OS.equalsIgnoreCase("Android"))
                {
                	 if((boolean)object.get("name").toString().equalsIgnoreCase(app_android) && (boolean)object.get("appVersion").toString().equalsIgnoreCase(build_android) && (boolean)object.get("appBuildVersion").toString().equalsIgnoreCase(version_android) && (boolean)object.get("type").toString().equalsIgnoreCase("Android"))
                	 {
                		 counter = (Long)object.get("counter");
                	//	 System.out.println(counter);
                		 fileName = (String)object.get("fileName");
                	//	 System.out.println(fileName);
                		 appPackage = (String)object.get("appPackage");
                	//	 System.out.println(appPackage);
                		 appwithCounter.put(counter,fileName);  
                		 packagewithCounter.put(counter,appPackage);    
                	 }
                	 
                }
                else if(OS.equalsIgnoreCase("iOS"))
                {
                	 if((boolean)object.get("name").toString().equalsIgnoreCase(app_iOS) && (boolean)object.get("appVersion").toString().equalsIgnoreCase(build_iOS) && (boolean)object.get("appBuildVersion").toString().equalsIgnoreCase(version_iOS) &&  (boolean)object.get("type").toString().equalsIgnoreCase("iOS"))
                	 {
                  		 counter = (Long)object.get("counter");
                     	//	 System.out.println(counter);
                     		 fileName = (String)object.get("fileName");
                     	//	 System.out.println(fileName);
                     		 appPackage = (String)object.get("identifier");
                     	//	 System.out.println(appPackage);
                     		 appwithCounter.put(counter,fileName);  
                     		 packagewithCounter.put(counter,appPackage);  
                		 }
                	}
                }
            
            if(appwithCounter.size()==0)
            {
            	System.out.println("Couldn't find the build with the mentioned configurations.");
            	ReportEvents.Done(callerClassName+":Browser","Couldn't find the build with the mentioned configurations.");
            }
            
            else if(appwithCounter.size()==1)
            {
            	System.out.println("Installing build "+fileName+" on device UDID "+deviceUdid);
            	ReportEvents.Done(callerClassName+":Browser","Installing build "+fileName+" on device UDID "+deviceUdid);
            	//Install Application
            	client.installApp(counter,appPackage,deviceUdid);
            	System.out.println("Installation success");
            	ReportEvents.Done(callerClassName+":Browser","Installation Success");
            	client.logout();
            }
            
            else if(appwithCounter.size()>1)
            {
            	System.out.println("Found multiple builds with similar configurations:");
            	ReportEvents.Done(callerClassName+":Browser","Found multiple builds with similar configurations: "+appwithCounter.values());
            	System.out.println(appwithCounter.values());
            	Long counterBig=(long) 0;
            	for(Map.Entry m:appwithCounter.entrySet()){  
            		   if(Long.parseLong(m.getKey().toString())>counterBig)
            		   {
            			   counterBig=Long.parseLong(m.getKey().toString()); 
            		   }
            		  }  
            	//System.out.println(counterBig);
            	//System.out.println("Installing the latest one i.e "+appwithCounter.get(counter));
            	System.out.println("Installing build "+appwithCounter.get(counterBig)+" on device UDID "+deviceUdid);
            	ReportEvents.Done(callerClassName+":Browser","Installing build "+packagewithCounter.get(counterBig)+" on device UDID "+deviceUdid);
               	//Install Application
            	client.installApp(counterBig, packagewithCounter.get(counterBig), deviceUdid);
            	System.out.println("Installation success");
            	ReportEvents.Done(callerClassName+":Browser","Installation Success");
            	client.logout();
            }
 
	}
        
        catch(Exception e)
        {
        	System.out.println(e.toString());
			ReportEvents.Error(callerClassName+":Browser", e);
        }

		
	}
}
