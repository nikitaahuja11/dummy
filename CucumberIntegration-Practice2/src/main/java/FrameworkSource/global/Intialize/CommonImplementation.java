package FrameworkSource.global.Intialize;

import java.io.IOException;

public interface CommonImplementation {
	
	final static String propertyFilePath= System.getProperty("user.dir") + "\\src\\test\\resources\\configuration\\Configuration.properties";
	final static String MobilePropertiesPath= System.getProperty("user.dir") + "\\src\\test\\resources\\configuration\\MobileConfig.properties";
	final static String strChromeDriverPath = System.getProperty("user.dir") + "\\src\\test\\resources\\browserdrivers\\chromedriver.exe";
	final static String strIEDriverPath = System.getProperty("user.dir")+"\\src\\test\\resources\\browserdrivers\\IEDriverServer.exe";
	final static String strFFDriverPath = System.getProperty("user.dir")+"\\src\\test\\resources\\browserdrivers\\geckodriver.exe";
	final static String host = "http://sv67299.sunlifecorp.com:8080";
	static String strFilePathORLocation = System.getProperty("user.dir")+"\\src\\test\\resources\\OR";
	public static final String DB_URL = "SQC8RG1D.ca.sunlife\\V4D811";  
    public static final String DB_USER = "tcoe";
    public static final String DB_PASSWORD = "Te$TCoE1";
    public static String strPath = System.getProperty("user.dir") + "\\src\\test\\resources\\";
    //public static String strVBSORPath =  "\"" + System.getProperty("user.dir") + "\\src\\main\\java\\FrameworkSource\\global\\ORRead\\CreateJSONOR.vbs" + "\" ";
    public static String strPath_JSONOR = "\"" + strPath + "OR\\properties.json" + "\"";
    public static String strPath_JSONData ="\"" + strPath + "Data\\CommonData.json" + "\"";
    public static String strVBSCommonDataPath =  "\"" + System.getProperty("user.dir") + "\\src\\main\\java\\FrameworkSource\\global\\DataRead\\CreateJSONCommonData.vbs" + "\" ";
    public static String reportConfigPath =  System.getProperty("user.dir")+"\\src\\test\\resources\\configuration\\extent-config.xml";
    public static String strReportSnapshotsPath = "\"\\\\sp.sunlifecorp.com\\sites\\tcoe\\TCoE\\Regression\\ALL LOB\\Execution Reports\"";
    
	
	static void func_SetPlatform(String strPlatform) throws IOException
	{
			ClsInitialize.strPlatform = strPlatform;	
	}
	
	static void func_SetDevice(String strDevice) throws IOException
	{
		
			ClsInitialize.strDevice = strDevice;
		
	}
	
	
	
	
}
