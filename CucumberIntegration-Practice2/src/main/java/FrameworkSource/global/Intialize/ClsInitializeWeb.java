package FrameworkSource.global.Intialize;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import FrameworkSource.UI.Web.Browser;
import FrameworkSource.UI.Web.Page;
import FrameworkSource.global.reporter.ReportEvents;

public class ClsInitializeWeb implements CommonImplementation{

	public static WebDriver automationWebDriver = null;
	protected static Browser objBrowser = null;
	protected static Page objPage = null;
	
	/*
	 * Initiating the Automation Driver if no object already exists
	 * and returning the already created object
	 */
	public static WebDriver fnGetAutomationWebDriver() throws IOException, InterruptedException
	{
		if(automationWebDriver == null)
		{
			fnIntiateWebDriver(ClsInitialize.func_GetDevice());
		}
		
		return automationWebDriver;
	}
	
	/*
	 * Initiating the Driver for multiple Browsers based on the common_data
	 */
	public static boolean fnIntiateWebDriver(String strDevice) throws IOException
	{
		boolean blnResult = false;
		long pageLoadTimeout = 40;
			String callerClassName = new Exception().getStackTrace()[1].getMethodName();
			try{
				
				
				switch(strDevice.toLowerCase())
				{
					case "chrome":
								System.setProperty("webdriver.chrome.driver", strChromeDriverPath);
								automationWebDriver = new ChromeDriver(); 
								automationWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								new WebDriverWait(automationWebDriver, pageLoadTimeout).until(
								          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
								ReportEvents.Done(callerClassName+":Browser", "Google Chrome is Launched : "+ automationWebDriver);
								break;
					
					case "ie":
								
								System.setProperty("webdriver.ie.driver", new File(strIEDriverPath).getAbsolutePath());
								DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
								ieCapabilities.setCapability("ignoreZoomSetting", true);
								ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
								ieCapabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, " ");
								automationWebDriver = new InternetExplorerDriver(ieCapabilities);
								new WebDriverWait(automationWebDriver, pageLoadTimeout).until(
								          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
								automationWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								ReportEvents.Done(callerClassName+":Browser", "IE is Launched : "+ automationWebDriver);
								break;
					
					case "firefox":
								System.setProperty("webdriver.gecko.driver",strFFDriverPath );
								automationWebDriver = new FirefoxDriver(); 
								new WebDriverWait(automationWebDriver, pageLoadTimeout).until(
								          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
								automationWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								ReportEvents.Done(callerClassName+":Browser", "Firefox is Launched : "+ automationWebDriver);
								break;
					
					default: 
								System.setProperty("webdriver.chrome.driver", strChromeDriverPath);
								automationWebDriver = new ChromeDriver(); 
								new WebDriverWait(automationWebDriver, pageLoadTimeout).until(
								          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
								automationWebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
								ReportEvents.Done(callerClassName+":Browser", "Google Chrome is Launched : "+ automationWebDriver);
								break;
				}
				blnResult = true;
				automationWebDriver.get(ClsInitialize.mapMasterURL.get(ClsInitialize.mapCommonDataWeb.get("Environment").toString()).toString());
			}
			
			catch(Exception e)
			{
				ReportEvents.Error(callerClassName+":Browser", e);
				blnResult = true;
			}
		
		return blnResult;
		
	}
	
	/*
	 * Returning the Browser Class Object
	 */
	
	public static Browser func_getBrowser() throws IOException, InterruptedException
	{
		if (objBrowser == null)
		{
			objBrowser = new Browser();
			
		}
		
		return objBrowser;
		
	}
	
	/*
	 * Returning the Page Class Object
	 */
	
	public Page func_getPage() throws IOException, InterruptedException
	{
		if (objPage == null)
		{
			objPage = new Page(func_getBrowser());
		}
		
		return objPage;
		
	}
	
	/*
	 * Closing extra windows opened during execution
	 */
	
	public static boolean fnQuitBrowser() throws IOException, InterruptedException
	{
		boolean blnResult = false;
		try
		{
			if (automationWebDriver.getWindowHandles().size() > 1)
			{
				int iCount = 0;
				 for(String window : automationWebDriver.getWindowHandles())
				 {
					 if (iCount != 0)
					 automationWebDriver.switchTo().window(window).close();
					 iCount++;
					 if (automationWebDriver.getWindowHandles().size() > 1)
						 continue;
					 else
						 break;
				 }
				 automationWebDriver.switchTo().window(new ArrayList<String>(automationWebDriver.getWindowHandles()).get(0));
			}
			objBrowser = null;
			objPage = null;
			automationWebDriver.close();
			automationWebDriver = null;
		
			//fnGetAutomationWebDriver().close();
			//func_getBrowser().Quit();
			/*WebDriver automationWebDriver = null;
			Browser objBrowser = null;
			Page objPage = null;*/
			blnResult = true;
		}catch(Exception objException)
		{
			blnResult = false;
		}
		return blnResult;
	}
}
