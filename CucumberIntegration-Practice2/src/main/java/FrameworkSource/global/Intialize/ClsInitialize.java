package FrameworkSource.global.Intialize;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.github.mkolisnyk.cucumber.reporting.CucumberResultsOverview;
//import com.steadystate.css.parser.ParseException;

import FrameworkSource.global.DBMetrices.DB_UpdateMetrics;
import FrameworkSource.global.Read_OR_Data.ReadCommonData;
import FrameworkSource.global.reporter.ReportEvents;
import FrameworkSource.global.reporter.ReportGenerator;
import FrameworkSource.UI.Mobile.App;
import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.UI.Web.Page;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.AppiumDriver;


public class ClsInitialize implements CommonImplementation {

	
	/*
	 *Declaration of Variables for the Reading Properties files and capabilities  
	 * 
	 */
	public static BufferedReader reader = null;
	public static Properties properties = null;
	public static Properties mobile_properties = null;
	public static DesiredCapabilities capabilities = new DesiredCapabilities();
	
	/*
	 *Declaration of Variables for the Capturing 
	 *Test Case Name
	 *File Path of the OR for both Web and Mobile
	 *Platform - either web or mobile
	 *Device - either android or iOS
	 *Language - Either french or English. This is captured from the tags 
	 */
	
	public static String strTestCaseName = null;
	public static String strFilePathOR = null;
	public static String strPlatform = null;
	public static String strDevice= null;
	public static String strLanguage = null;
	
	/*
	 *Declaration of Objects for Web and Mobile classes
	 *Declaration of Objects for Browser and Page classes
	 *Declaration of Objects for Device and App classes  
	 * 
	 */
	public static ClsInitializeWeb objWeb;
	public static ClsIntializeMobile objMobile;
	public static Page iobjPage;
	public static Browser iobjBrowser;
	public static Device iobjDevice;
	public static App iobjApp;
	
	/*
	 *Declaration of HashMap for the Environment data
	 *Declaration of Hashmap for Master URL    
	 *Declaration of Hashmap for capturing Iteration with respect to Test Data 
	 *Declaration of HashMap for capturing the DB Metrices data 
	 */
	
	public static LinkedHashMap<String, Object> mapCommonDataWeb = null;
	public static LinkedHashMap<String, Object> mapMasterURL = null;
	public static LinkedHashMap<String, Object> mapCommonDataMobile = null;
	public static LinkedHashMap<String, Integer> mapIteration = new LinkedHashMap<String, Integer>();;
	public static HashMap<String, String> mapDBMetricesData = null;
	public static LinkedHashMap<String, String> mapCommonData = null;
	
	public static String strStartDate;
	public static String strEndDate;
	
	/*
	 *Declaration of Variables for the Capturing the Test Case Count and Iterations executed  
	 * 
	 */
	
	public static int iIterations = 0;
	public static int iTestCaseCount = 0;
	private static int defaultSleep = 2000;
	private static int intMillis = 1000;
	/*
	 *Declaration of Variables for the Capturing the Screenshots  
	 * 
	 */
	
	public static String strCaptureScreenShotFlag;
	public static String strStepCaptureScreenShotFlag;
	
	/*
	 *Declaration of Variables for the OR File Names from Config File  
	 * 
	 */
	
	static String strPropName_Web;
	static String strPropName_Mobile;
	static String strFlagJSON;
	
	static String strFeatureName = "";
	
	public ClsInitialize()
	{
		
	}
	
	/*
	 *Constructor of Initilize class.
	 *This constructor is the initial point of the execution and initiates the Browser/Device
	 *Reads Properties files at java/resources
	 *Starts the Reporting 
	 *Converts the CommonData.xlsx  
	 * Captures Iterations and Test Case Count Data.
	 * It Takes the Feature File name and Test case Name and returns null
	 */
	
	@SuppressWarnings("unchecked")
	public ClsInitialize(String strTestCaseName, String strObtFeatureName) throws IOException, InterruptedException, ClassNotFoundException, SQLException
	{
		Process processJSONCreationOR = null;
		Process processJSONCreationData = null;
		
		ReportEvents.FlagScreenShot = "false";
		ReportEvents.FlagStepScreenShot = "false";
		
		try{
			ClsInitialize.strTestCaseName = strTestCaseName;
			ClsInitialize.properties = fngetProperties();
			ClsInitialize.mobile_properties = fngetMobileProperties();
			strPropName_Web = properties.getProperty("Properties_FileName_Web").toString();
			strPropName_Mobile = properties.getProperty("Properties_FileName_Mobile").toString();
			strFlagJSON = properties.getProperty("Use_JSON_As_OR").toString();
			if(strPropName_Mobile.trim().contains(";")||strPropName_Web.trim().contains(";"))	
				func_GetORMulFilePath();
			else
				func_GetORFilePath();
			
			DateFormat dtStartDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date dtStartDate = new Date();
			strFeatureName = strObtFeatureName.replace("-", " ");
			
			
			
					
			if(mapIteration.containsKey(strTestCaseName))
			{
				
				iIterations++;
				mapIteration.put(strTestCaseName, mapIteration.get(strTestCaseName) + 1);
				//ReportEvents.Done("Pool:" + iTestCaseCount + "::Thread Count: 1::Iterations" + iIterations + "Feature Name: "+ strFeatureName + "Tests."+ strTestCaseName +"$",  "Execution Started");
			}
			else
			{
				iIterations = 0;
				iIterations++;
				iTestCaseCount++;
				strStartDate = dtStartDateFormat.format(dtStartDate);
				mapIteration.put(strTestCaseName, iIterations);
			}
			
			//ReportEvents.Done("Pool:" + iTestCaseCount + "::Thread Count: 1::Iterations:" + iIterations + "::Feature Name: "+ strFeatureName + "::Tests:$"+ strTestCaseName +"$",  "Execution Started");

			//mapCommonData contains all the values of common data sheet
			mapCommonData = new ReadCommonData().fnReadCommonData("", "CommonData");
			if (strPlatform.equalsIgnoreCase("web"))
			{
				mapCommonDataWeb= new ReadCommonData().fnReadCommonData(strTestCaseName,"Environment_Web");
				if(mapCommonDataWeb.get("WebApp").toString().length() > 0)
				{
					mapMasterURL= new ReadCommonData().fnReadCommonData(mapCommonDataWeb.get("WebApp").toString(),"MasterURLSheet");
				}
				else
				{
					ReportEvents.Fatal("Initialization Error", "WebApp cannot be left empty in Environment sheet");
					Assert.fail();
				}
					objWeb = new ClsInitializeWeb();
					iobjPage = objWeb.func_getPage();
					iobjBrowser = objWeb.objBrowser;
			}
			else
			{
				mapCommonDataMobile= new ReadCommonData().fnReadCommonData(strTestCaseName,"Environment_Mobile");
				objMobile = new ClsIntializeMobile();
				//ClsInitialize.strDevice = (String) mapCommonDataMobile.get("Device");
				iobjDevice = objMobile.fnInitializeDevice();
				iobjApp = objMobile.appTest;
				if(mapCommonDataMobile.get("WebApp").toString().length() > 0)
					mapMasterURL= new ReadCommonData().fnReadCommonData(mapCommonDataMobile.get("WebApp").toString(),"MasterURLSheet");
			}
			
			
			
			/*
			 * Update the Properties for the Snapshots. These flags are taken from configuration.properties 
			 * and will be updated as part of ReportEvents.
			 * The strCaptureScreenShotFlag is for the Pass and Fail Logging of the Test cases
			 */
			if (properties.getProperty("CaptureSnapshot").toString().equalsIgnoreCase("yes"))
			{
				strCaptureScreenShotFlag = "true";
			}
			else
			{
				strCaptureScreenShotFlag = "false";
			}
			
			/*
			 * Update the Properties for the Snapshots. These flags are taken from configuration.properties 
			 * and will be updated as part of ReportEvents.
			 * The strStepCaptureScreenShotFlag is for the Each and Every level of Reporting
			 */
			
			if (properties.getProperty("CaptureStepSnapshot").toString().equalsIgnoreCase("yes"))
			{
				strStepCaptureScreenShotFlag = "true";
			}
			else
			{
				strStepCaptureScreenShotFlag = "false";
			}
			
			ReportEvents.CaptureScreenShots(strCaptureScreenShotFlag,strStepCaptureScreenShotFlag);
			try{
				Runtime.getRuntime().exec("net use f: " + strReportSnapshotsPath);
				ReportEvents.strCaptureSnapshotPath = "F:\\";
			}catch(Exception e)
			{
				ReportEvents.strCaptureSnapshotPath = strReportSnapshotsPath + "\\";
			}
		}catch(Exception exobjInit)
		{
			ReportEvents.FlagScreenShot = "false";
			ReportEvents.FlagStepScreenShot = "false";
			ReportEvents.Error("Error in Intializing the Objects", exobjInit);
		}
	}
	
	/*
	 *Read the Properties from the config file kept at java/resources folder  
	 * 
	 */
	
	protected static Properties fngetProperties() throws IOException
	{
		if (ClsInitialize.properties == null)
		{
			ClsInitialize.reader = new BufferedReader(new FileReader(propertyFilePath));
			ClsInitialize.properties = new Properties();
			ClsInitialize.properties.load(ClsInitialize.reader);
			ClsInitialize.reader.close();
		}
		
		return ClsInitialize.properties;
	}
	
	/*
	 *Read the Properties from the mobile config file kept at java/resources folder  
	 * 
	 */
	public static Properties fngetMobileProperties() throws IOException
	{
		if (ClsInitialize.mobile_properties == null)
		{
			ClsInitialize.reader = new BufferedReader(new FileReader(MobilePropertiesPath));
			ClsInitialize.mobile_properties = new Properties();
			ClsInitialize.mobile_properties.load(ClsInitialize.reader);
			ClsInitialize.reader.close();
		}
		
		return ClsInitialize.mobile_properties;
	}
	
	/*
	 *Function for getting the device currently active
	 * 
	 */
	
	protected static String func_GetDevice() throws IOException
	{
		if (strDevice ==null)
		{
			try{
				if(ClsInitialize.strPlatform.trim().equalsIgnoreCase("web"))
					strDevice = (String) mapCommonDataWeb.get("Browser");
				else
					strDevice = (String) mapCommonDataMobile.get("Device");
			}catch(Exception exobjGetDevice)
			{
				ReportEvents.Error("Error in Common Data, Please check if the Test cases are added in Common Data File", exobjGetDevice);
			}
		}
		return strDevice;
	}
	
	/*
	 *Function for reading the OR File
	 * 
	 */
	
	protected static String func_GetORFilePath() throws IOException
	{
		if (ClsInitialize.strFilePathOR == null)
		{
			switch (strFlagJSON.toUpperCase().trim()) {
            case "YES":
            	if(ClsInitialize.strPlatform.trim().equalsIgnoreCase("web"))
    			{
    				ClsInitialize.strFilePathOR = strFilePathORLocation +"\\" + strPropName_Web + ".json";
    			}
    			else if(ClsInitialize.strPlatform.equalsIgnoreCase("mobile"))
    			{
    				ClsInitialize.strFilePathOR = strFilePathORLocation +"\\"+ strPropName_Mobile + ".json";
    			}
            	break;
            case "NO":
            	if(ClsInitialize.strPlatform.trim().equalsIgnoreCase("web"))
    			{
    				ClsInitialize.strFilePathOR = strFilePathORLocation +"\\" + strPropName_Web + ".xlsx";
    			}
    			else if(ClsInitialize.strPlatform.equalsIgnoreCase("mobile"))
    			{
    				ClsInitialize.strFilePathOR = strFilePathORLocation +"\\"+ strPropName_Mobile + ".xlsx";
    			}
            	break;
            	default:
            		break;
			}
			
		}
		
		return ClsInitialize.strFilePathOR;
	}
	
	protected static String func_GetORMulFilePath() throws IOException
	{
		
		if (ClsInitialize.strFilePathOR == null)
		{
			ClsInitialize.strFilePathOR="";
			String[] arrMulORPath = null;
			switch (strFlagJSON.toUpperCase().trim()) {
            case "YES":
            	if(ClsInitialize.strPlatform.trim().equalsIgnoreCase("web"))
    			{
            		arrMulORPath = ClsInitialize.properties.getProperty("Properties_FileName_Web").toString().split(";");
            		for(int iCount = 0;iCount<arrMulORPath.length;iCount++)
    				ClsInitialize.strFilePathOR = ClsInitialize.strFilePathOR + strFilePathORLocation +"\\" + arrMulORPath[iCount] + ".json" +";";
    			}
    			else if(ClsInitialize.strPlatform.equalsIgnoreCase("mobile"))
    			{
    				arrMulORPath = ClsInitialize.properties.getProperty("Properties_FileName_Mobile").toString().split(";");
            		for(int iCount = 0;iCount<arrMulORPath.length;iCount++)
    				ClsInitialize.strFilePathOR = ClsInitialize.strFilePathOR + strFilePathORLocation +"\\" + arrMulORPath[iCount] + ".json" +";";
    			}
            	break;
            case "NO":
            	if(ClsInitialize.strPlatform.trim().equalsIgnoreCase("web"))
    			{
            		arrMulORPath = ClsInitialize.properties.getProperty("Properties_FileName_Web").toString().split(";");
            		for(int iCount = 0;iCount<arrMulORPath.length;iCount++)
    				ClsInitialize.strFilePathOR = ClsInitialize.strFilePathOR + strFilePathORLocation +"\\" + arrMulORPath[iCount] + ".xlsx" +";";
    			}
    			else if(ClsInitialize.strPlatform.equalsIgnoreCase("mobile"))
    			{
    				arrMulORPath = ClsInitialize.properties.getProperty("Properties_FileName_Mobile").toString().split(";");
            		for(int iCount = 0;iCount<arrMulORPath.length;iCount++)
    				ClsInitialize.strFilePathOR = ClsInitialize.strFilePathOR + strFilePathORLocation +"\\" + arrMulORPath[iCount] + ".xlsx" +";";
    			}
            	break;
            	default:
            		break;
			}
			
		}
		
		return ClsInitialize.strFilePathOR;
	}

	
	public static DesiredCapabilities func_GetCapabilities()
	{
			capabilities = new DesiredCapabilities();	
			capabilities.setCapability("userName", mobile_properties.getProperty("MC_Username").trim());
			capabilities.setCapability("password", mobile_properties.getProperty("MC_Password").trim());
		
		return capabilities;
	}
	
	public static boolean fnCloseObjects(String status)
	{
		boolean blnResult = false;
		
		try
		{
			//ReportEvents.Done("Pool:" + iTestCaseCount + "::Thread Count: 1::Iterations:" + iIterations + "::Feature Name: "+ strFeatureName + "::Tests:$"+ strTestCaseName + "::Execution Status: " + status,  status);
			fnUpdateDBMap(status);
			strDevice = null;
			if (strPlatform.equalsIgnoreCase("web"))
			{
				blnResult = objWeb.fnQuitBrowser();
			}
			else
			{
				
				ClsIntializeMobile.automationMobileDriver.close();
				//ClsIntializeMobile.automationMobileDriver.quit();
				ClsIntializeMobile.automationMobileDriver = null;
				ClsIntializeMobile.deviceTest = null;
				ClsIntializeMobile.automationTempWebDriver.close();
				ClsIntializeMobile.automationTempWebDriver.quit();
				ClsIntializeMobile.automationTempWebDriver=null;
				/*
				iobjDevice.Quit();
				objMobile.automationTempWebDriver.close();
				//objMobile.automationTempWebDriver.quit();
				blnResult = objMobile.fnCloseMobile();*/
			}
			
			
			
			
			blnResult = true;
		}catch(Exception e)
		{
			blnResult = false;
		}
		return blnResult;
	}
	
	public static void fnUpdateDBMap(String status)
	{
		String strValue;
		String strIterationStatus;
		DateFormat dtEndTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date dateEndTime = new Date();
		strEndDate = dtEndTimeFormat.format(dateEndTime);
		// Store the Data(Tests, CurentDate,StartTime,TotalExetime and Test Execution Status) in Hashmap
		
		if(status.equalsIgnoreCase("failed"))
			strIterationStatus = "0";
		else
			strIterationStatus = "1";
		
		if(mapDBMetricesData.containsKey(strTestCaseName))
		{
			strValue = mapDBMetricesData.get(strTestCaseName);
			strIterationStatus = strIterationStatus + "," + strValue.split(";")[4];
			//strValue.split(";")[3] = status;
			strValue = strValue.split(";")[0] + ";" + strValue.split(";")[1]  + ";" + status+";" + iIterations +";"+ strIterationStatus;
			mapDBMetricesData.put(strTestCaseName,strValue);
		}
		else
		{
			strValue = strStartDate + ";" + strEndDate+ ";" + status+";" + iIterations + ";" + strIterationStatus;
			mapDBMetricesData.put(strTestCaseName,strValue);
		}
	}
	private static long getCurrentTime() {
        return System.currentTimeMillis();
    }
	
	public static boolean waitForElementToAppear(WebElement UIElement , int seconds,String strElmName) throws IOException {
        boolean flag = false;
        long end = getCurrentTime() + (seconds * intMillis);
        while (getCurrentTime() < end) {
            try {
                if (UIElement !=null ) {
                    Thread.sleep(defaultSleep);
                    ReportEvents.Done("Dynamic Wait", "Waiting for "+ strElmName + "Element to Appear");
                } else {
                    flag = true;
                    break;
                }
            } catch (Exception e) {
                try {
                    Thread.sleep(defaultSleep);
                    ReportEvents.Done("Dynamic Wait", "Waiting for "+ strElmName + "Element to Appear with default Sleep");
                } catch (Exception f) {
                	ReportEvents.Done("Dynamic Wait", "Waiting for "+ strElmName + "Element to Appear with default Sleep");
                }
            }
        }
        return flag;
    }
	
	public static String getDriverType() {
        try {
            if (ClsIntializeMobile.automationMobileDriver.getPlatformName().equalsIgnoreCase("android")) {
                return "android";
            } else {
                return "ios";
            }
        } catch (Exception e) {
            return "";
        }
    }
}
