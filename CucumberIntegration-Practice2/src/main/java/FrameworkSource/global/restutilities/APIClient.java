package FrameworkSource.global.restutilities;
import okhttp3.*;
import okhttp3.Response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.Intialize.ClsIntializeMobile;
import FrameworkSource.UI.Mobile.Device;



public class APIClient {

    // *************************************
    // Initiate the constants with your data
    // *************************************
    private static String BASE_URL = "http://mobilecenter.sunlifecorp.com:8080/rest/";  
    private static String LWSSO_COOKIE_KEY;
    WebDriver driver;
    
    
    // ************************************
    // Mobile Center APIs end-points
    // ************************************

    private static final String ENDPOINT_CLIENT_LOGIN = "client/login";
    private static final String ENDPOINT_CLIENT_LOGOUT = "client/logout";
    private static final String ENDPOINT_CLIENT_DEVICES = "deviceContent";
    private static final String ENDPOINT_CLIENT_APPS = "apps";
    private static final String ENDPOINT_CLIENT_INSTALL_APPS = "apps/install";

    // ************************************
    // Initiate proxy configuration
    // ************************************

    private static final boolean USE_PROXY = false;
    private static final String PROXY = "<PROXY>";

    // ************************************
    // Path to app (IPA or APK) for upload
    // ************************************

    @SuppressWarnings("unused")
    private static String APP = "/PATH/TO/APP/FILE.ipa|apk";

    private OkHttpClient client;
    private String hp4msecret;
    private String jsessionid;
    private String responseBodyStr;

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final MediaType APK = MediaType.parse("application/vnd.android.package-archive");

    // ******************************************************
    // APIClient class constructor to store all info and call API methods
    // ******************************************************
    public APIClient() {};
    public APIClient(String username, String password) throws IOException {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .readTimeout(240, TimeUnit.SECONDS)
                .writeTimeout(240, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .cookieJar(new CookieJar() {
                    private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

                    @Override
                    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                        List<Cookie> storedCookies = cookieStore.get(url.host());
                        if (storedCookies == null) {
                            storedCookies = new ArrayList<>();
                            cookieStore.put(url.host(), storedCookies);
                        }
                        storedCookies.addAll(cookies);
                        for (Cookie cookie : cookies) {
                            if (cookie.name().equals("hp4msecret"))
                                hp4msecret = cookie.value();
                            if (cookie.name().equals("JSESSIONID"))
                                jsessionid = cookie.value();
                        }
                    }

                    @Override
                    public List<Cookie> loadForRequest(HttpUrl url) {
                        List<Cookie> cookies = cookieStore.get(url.host());
                        return cookies != null ? cookies : new ArrayList<>();
                    }
                });

        if (USE_PROXY) {
            int PROXY_PORT = 8080;
            clientBuilder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY, PROXY_PORT)));
        }

        client = clientBuilder.build();
        login(username, password);
    }

    // ***********************************************************
    // Login to Mobile Center for getting cookies to work with API
    // ***********************************************************

    private void login(String username, String password) throws IOException {
    	Properties properties = ClsInitialize.fngetMobileProperties();
        String strCredentials = "{\"name\":\"" + properties.getProperty("MC_Username") + "\",\"password\":\"" + properties.getProperty("MC_Password") + "\"}";
        RequestBody body = RequestBody.create(JSON, strCredentials);
        executeRestAPI(ENDPOINT_CLIENT_LOGIN, HttpMethod.POST, body);

    }

    // ************************************
    // List all apps from Mobile Center
    // ************************************

    public String apps() throws IOException {
       return executeRestAPI(ENDPOINT_CLIENT_APPS);
    }

    // ************************************
    // List all devices from Mobile Center
    // ************************************

    public String deviceContent() throws IOException {
        return executeRestAPI(ENDPOINT_CLIENT_DEVICES);
    }

    // ************************************
    // Install application
    // ************************************

    public void installApp(Long counter, String package_name, String device_id) throws IOException {
        //counter="2";
        //package_name= "ca.sunlife.enterprise.dev.slfmember";
//        package_name = "com.Advantage.aShopping";
        String str = "{\n" +
                "  \"app\": {\n" +
                "    \"counter\": " + counter + ",\n" +
                "    \"id\": \"" + package_name + "\",\n" +
                "    \"instrumented\": " + (true ? "true" : "false") + "\n" +
                "  },\n" +
                "  \"deviceCapabilities\": {\n" +
                "    \"udid\": \"" + device_id + "\"\n" +
                "  }\n" +
                "}";
        RequestBody body = RequestBody.create(JSON, str);
        executeRestAPI(ENDPOINT_CLIENT_INSTALL_APPS, HttpMethod.POST, body);
    }

    // ************************************
    // Logout from Mobile Center
    // ************************************

    public void logout() throws IOException {
        RequestBody body = RequestBody.create(JSON, "");
        executeRestAPI(ENDPOINT_CLIENT_LOGOUT, HttpMethod.POST, body);
    }

    private String executeRestAPI(String endpoint) throws IOException {
        return executeRestAPI(endpoint, HttpMethod.GET);
    }

    private String executeRestAPI(String endpoint, HttpMethod httpMethod) throws IOException {
        return executeRestAPI(endpoint, httpMethod, null);
    }

    private String executeRestAPI(String endpoint, HttpMethod httpMethod, RequestBody body) throws IOException {

        // build the request URL and headers
        Request.Builder builder = new Request.Builder()
                .url(BASE_URL + endpoint)
                .addHeader("Content-type", JSON.toString())
                .addHeader("Accept", JSON.toString());

        // add CRSF header
        if (hp4msecret != null) {
            builder.addHeader("x-hp4msecret", hp4msecret);
        }

        // build the http method
        if (HttpMethod.GET.equals(httpMethod)) {
            builder.get();
        } else if (HttpMethod.POST.equals(httpMethod)) {
            builder.post(body);
        }

        Request request = builder.build();
        //System.out.println("\n" + request);

        try (okhttp3.Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                //System.out.println(response.toString());
                final ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    responseBodyStr = responseBody.string();
                  //  System.out.println("Body: " + responseBodyStr);
                }
            }
            
            else if(response.code()==401)
            {
                throw new IOException("Bad Request. Check user credentials. Error code: " + response.code());
            }
            
            else if(response.code()==403)
            {
                throw new IOException("Forbidden error. Seems device is reserved. Error code: " + response.code());
            }
            
            else  {
                throw new IOException("Unexpected code:" + response.code());
            }
            response.close();
        }
		return responseBodyStr;
    }


    private enum HttpMethod {
        GET,
        POST
    }
    
    
    
   //MC Methods
    
    
    
   public String between(String value, String a, String b) {
	     
        int posA = value.indexOf(a);
        if (posA == -1) {
            return "";
        }
        int posB = value.lastIndexOf(b);
        if (posB == -1) {
            return "";
        }
        int adjustedPosA = posA + a.length();
        if (adjustedPosA >= posB) {
            return "";
        }
        return value.substring(adjustedPosA, posB);
    
}
    
    public void loginToMC() throws IOException
    {
    	OkHttpClient client = new OkHttpClient();
    	 Properties properties = ClsInitialize.fngetMobileProperties();
        Request.Builder builder = new Request.Builder()
                .url(BASE_URL + "client/login")
                .addHeader("Content-type", JSON.toString())
                .addHeader("Accept", JSON.toString());
        
        String strCredentials = "{\"name\":\"" + properties.getProperty("MC_Username") + "\",\"password\":\"" + properties.getProperty("MC_Password") + "\"}";
        RequestBody body = RequestBody.create(JSON, strCredentials);
        
        builder.post(body);
        
        Request request = builder.build();
        //System.out.println("\n" + request);
        
        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
            	//System.out.println("Response Headers:"+response.headers());
            	hp4msecret = response.headers("x-hp4msecret").get(0);
            	//System.out.println("x-hp4msecret: "+hp4msecret);
            	LWSSO_COOKIE_KEY= between(response.headers("Set-Cookie").toString(), "LWSSO_COOKIE_KEY=", ",");
            	//System.out.println("LWSSO_COOKIE_KEY: "+LWSSO_COOKIE_KEY);
            } else {
                throw new IOException("Unexpected code " + response);
            }
            response.close();
        }

    }
		// TODO Auto-generated method stub
    
    public WebDriver launchDevice(String device_udid) throws IOException, InterruptedException {
 
    	Device d = new Device();
    	if(hp4msecret==null && LWSSO_COOKIE_KEY==null)
    	{
    	loginToMC();
    		System.out.println("Call VNC selenium code using new headers");
    		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\browserdrivers\\chromedriver.exe"); 
    		ChromeOptions options = new ChromeOptions();
    	    options.addArguments("--app=http://sv67299:8080/integration/?locale=en#/remote?deviceId="+device_udid); 
    	    options.addArguments("disable-infobars");
    		driver = new ChromeDriver(options); 
		//driver.get("http://sv67299:8080/integration/?locale=en#/remote?deviceId=90aea2ce416efa8e2669e928a6cc90cbdff44730");
		//driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		//driver.get("http://mobilecenter.sunlifecorp.com:8080/integration/#/login");

		//Thread.sleep(3000);
		//driver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);
		  String jscode = modify_code(LWSSO_COOKIE_KEY, hp4msecret, device_udid);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript(jscode);	
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window((String) tabs.get(1));
    	}
    	
    	else
    	{
    		/*System.out.println("Call VNC selenium code using existing headers");
    		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\browserdrivers\\chromedriver.exe");
    		ChromeOptions options = new ChromeOptions();
    	    options.addArguments("--app=http://sv67299:8080/integration/?locale=en#/remote?deviceId=90aea2ce416efa8e2669e928a6cc90cbdff44730"); 
    	    options.addArguments("disable-infobars");
    		driver = new ChromeDriver(options); 
    		Thread.sleep(3000);*/
    		if( ClsIntializeMobile.automationTempWebDriver == null)
    		{
    			System.out.println("Calling empty VNC");
    			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\browserdrivers\\chromedriver.exe"); 
    			ChromeOptions options = new ChromeOptions();
    		    options.addArguments("--app=http://sv67299:8080/integration/?locale=en#/remote?deviceId=90aea2ce416efa8e2669e928a6cc90cbdff44730"); 
    		    options.addArguments("disable-infobars");
    			driver = new ChromeDriver(options); 
    		}
    		System.out.println("Call VNC selenium code using existing headers");	
  		  String jscode = modify_code(LWSSO_COOKIE_KEY, hp4msecret, device_udid);
  		JavascriptExecutor js = (JavascriptExecutor)driver;
  		//js.executeAsyncScript(jscode);	
  		js.executeScript(jscode);
  		
  		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window((String) tabs.get(1));
    		//driver = new ChromeDriver(); 
    		//driver.get("http://sv67299:8080/integration/?locale=en#/remote?deviceId=90aea2ce416efa8e2669e928a6cc90cbdff44730");
    		

    		/*
    		
    		driver.manage().window().maximize();
    		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

    		driver.get("http://mobilecenter.sunlifecorp.com:8080/integration/#/login");

    		Thread.sleep(3000);
    		*/
    	}
    	return driver;
    }
    public String modify_code(String LWSSO_COOKIE_KEY, String Hp4mSecret, String deviceudid)
	{
		String test = "document.cookie=\"LWSSO_COOKIE_KEY="+LWSSO_COOKIE_KEY+"\""+";\r\n" + 
				"\r\n" + 
				"var remoteWindow = window.open('http://sv67299:8080/integration/?locale=en#/remote?deviceId="+deviceudid+"');\r\n" + 
				"let setSecretAndConfigAfterLoaded = function() {\r\n" + 
				"	if (!!remoteWindow.incoming) {\r\n" + 
				"		remoteWindow.incoming.setHp4mSecret("+"\""+Hp4mSecret+"\""+");\r\n" + 
				"		// Set relevant client config on the remote window.\r\n" + 
				"		remoteWindow.incoming.setMcClientConfig({\r\n" + 
				"                sensorSim_authentication: true,\r\n" + 
				"                sensorSim_camera: true,\r\n" + 
				"                sensorSim_barcode: true,\r\n" + 
				"                showAppManagement: true,\r\n" + 
				"                showNv: true,\r\n" + 
				"                showNvHowto: true,\r\n" + 
				"                adhoc: true,\r\n" + 
				"                showCaptureDeviceAudio: true\r\n" + 
				"            });\r\n" + 
				"	}\r\n" + 
				"};\r\n" + 
				"\r\n" + 
				" let setSecretAndConfig = function() {\r\n" + 
				"	if (!!remoteWindow.incoming) {\r\n" + 
				"		setSecretAndConfigAfterLoaded();\r\n" + 
				"	}\r\n" + 
				"	else {\r\n" + 
				"		setTimeout(function(){\r\n" + 
				"			if (!!remoteWindow.incoming) {\r\n" + 
				"				setSecretAndConfigAfterLoaded();\r\n" + 
				"			}\r\n" + 
				"		}, 5000);\r\n" + 
				"	}\r\n" + 
				"};\r\n" + 
				"\r\n" + 
				" if(remoteWindow !== null && remoteWindow !== undefined) {\r\n" + 
				"	if (remoteWindow.addEventListener) {\r\n" + 
				"		remoteWindow.addEventListener(\"load\", setSecretAndConfig, false);\r\n" + 
				"	}\r\n" + 
				"	else if (remoteWindow.attachEvent) {\r\n" + 
				"		remoteWindow.attachEvent(\"onload\", setSecretAndConfig);\r\n" + 
				"	} else if (remoteWindow.onLoad) {\r\n" + 
				"		remoteWindow.onload = setSecretAndConfig;\r\n" + 
				"	}\r\n" + 
				"	//All the events -\"load\"/\"onload\" may fired before listeners set, for be sure secret set we call this function again.\r\n" + 
				"	if (remoteWindow) {\r\n" + 
				"		setSecretAndConfig();\r\n" + 
				"	}\r\n" + 
				"	\r\n" + 
				"}";
	
	return test;
	}
    
}