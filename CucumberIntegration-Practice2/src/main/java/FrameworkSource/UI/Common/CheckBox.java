package FrameworkSource.UI.Common;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.Keys;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.*;
/**
 * The class consists of all the functions relates to Checkbox of web page.
 */
public class CheckBox extends UIElement {
	/**
	 * CheckBox class constructor, directly invoked when an object of CheckBox class is created. Class Checkbox extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	/*public CheckBox(String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}*/
	
	public CheckBox(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public CheckBox( Device d, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	public CheckBox( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	/**
	 * The function checks if Check Box is selected or not. If it is selected then no operation is performed, else Check Box is selected.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void SelectCheckbox() throws IOException, InterruptedException
	{	
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try {
			if(ClsInitialize.objWeb.automationWebDriver !=null)
			{
				try
				{
					if(ClsInitialize.objWeb.func_getBrowser().element.isSelected())
				        {
							ReportEvents.Done(callerClassName+":CheckBox", "Checkbox "+ ClsInitialize.objWeb.func_getBrowser().element +" already checked");
							
				        }
				        else
				        {
				        	ClsInitialize.objWeb.func_getBrowser().element.sendKeys(Keys.SPACE); 
				        	ReportEvents.Done(callerClassName+":CheckBox","Checkbox "+ ClsInitialize.objWeb.func_getBrowser().element +" successfully checked");
				        }
				}catch(Exception e)
				{
					ReportEvents.Error(callerClassName+":CheckBox:: Exception in Selecting the checkbox", e);
				     Assert.fail();
				}
			}
			else if(ClsInitialize.objMobile.automationMobileDriver !=null)
			{
				try
				{
				if(ClsInitialize.objMobile.deviceTest.element.isSelected())
			        {
						ReportEvents.Done(callerClassName+":CheckBox", "Checkbox "+ ClsInitialize.objMobile.deviceTest.element +" already checked");
						//Logger.INFO("CheckBox", "Checkbox "+ b.element +" already checked");
			        }
			        else
			        {
			        	ClsInitialize.objMobile.deviceTest.element.sendKeys(Keys.SPACE); 
			        	ReportEvents.Done(callerClassName+":CheckBox","Checkbox "+ ClsInitialize.objMobile.deviceTest.element +" successfully checked");
			        
			        }
				}
				catch(Exception e)
				{
					ReportEvents.Error(callerClassName+":CheckBox:: Exception in Selecting the checkbox", e);
				    Assert.fail();
				}
			}
		}catch(Exception objCheckBox)
		{
			ReportEvents.Error(callerClassName+":CheckBox", objCheckBox);
		     Assert.fail();
		}
	}

	public void DeselectCheckbox() throws IOException, InterruptedException
	{	
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try {
			if(ClsInitialize.objWeb.automationWebDriver !=null)
			{
				try	{
					if(ClsInitialize.objWeb.func_getBrowser().element.isSelected())
				        {
							ClsInitialize.objWeb.func_getBrowser().element.sendKeys(Keys.SPACE); 
							ReportEvents.Done(callerClassName+":CheckBox", "Checkbox "+ ClsInitialize.objWeb.func_getBrowser().element +" successfully unchecked");
				        }
				        else
				        	ReportEvents.Done(callerClassName+":CheckBox","Checkbox "+ ClsInitialize.objWeb.func_getBrowser().element +" already unchecked");
				}
				catch(Exception e)
				{
					ReportEvents.Error(callerClassName+":CheckBox:: Exception in DeSelecting the checkbox", e);
				    Assert.fail();
				}
			}
			else if(ClsInitialize.objMobile.automationMobileDriver !=null)
			{
				try	{
					if(ClsInitialize.objMobile.deviceTest.element.isSelected())
				        {
							ClsInitialize.objMobile.deviceTest.element.sendKeys(Keys.SPACE); 
							ReportEvents.Done(callerClassName+":CheckBox", "Checkbox "+ ClsInitialize.objMobile.deviceTest.element +" successfully unchecked");
				        }
				        else
				        	ReportEvents.Done(callerClassName+":CheckBox","Checkbox "+ ClsInitialize.objMobile.deviceTest.element +" already unchecked");
					}
					catch(Exception e)
					{
						ReportEvents.Error(callerClassName+":CheckBox:: Exception in DeSelecting the checkbox", e);
					     Assert.fail();
					}
			}
		}catch(Exception objCheckBox)
		{
			ReportEvents.Error(callerClassName+":CheckBox:: Exception in DeSelecting the checkbox", objCheckBox);
		     Assert.fail();
		}
	}
	
}
