package FrameworkSource.UI.Common;

import java.io.IOException;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.*;
import org.junit.Assert;
/**
 * The class consists of all the functions related to RadioButton Class.
 */
public class RadioButton extends UIElement {
	/**
	 * Radio Button class constructor, directly invoked when an object of Radio button class is created. Class radio button extends WebUIElement class.
	 * @param b is a variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */	
	/*public RadioButton(String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}*/
	
	public RadioButton(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public RadioButton( Device d, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	public RadioButton( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	/**
	 * The function selects the radio button. If it is already selected then no operation is performed, else radio button will get selected.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void SelectRadiobutton() throws IOException, InterruptedException
	{	
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		
		if (ClsInitialize.objWeb.automationWebDriver != null)
		{
			try
			{		
				if(ClsInitialize.objWeb.func_getBrowser().element.isSelected())
			        {
					ReportEvents.Done(callerClassName+":Radiobutton","Radiobutton " + ClsInitialize.objWeb.func_getBrowser().element +" already deselected");
			        }
			        else
			        {
			        	ClsInitialize.objWeb.func_getBrowser().element.click(); 
			        	ReportEvents.Done(callerClassName+":Radiobutton","Radiobutton " + ClsInitialize.objWeb.func_getBrowser().element +" successfully selected");
			        }
			}
			catch(Exception e)
			{
				ReportEvents.Error(callerClassName+":Radiobutton::Exception in Selecting the RadioButton", e);
			    Assert.fail();
			}
		}
		else if(ClsInitialize.objMobile.automationMobileDriver != null)
		{
			try
			{		
				if(ClsInitialize.objMobile.deviceTest.element.isSelected())
			        {
						ReportEvents.Done(callerClassName+":Radiobutton","Radiobutton " + ClsInitialize.objMobile.deviceTest.element +" already deselected");
			        }
			        else
			        {
			        	ClsInitialize.objMobile.deviceTest.element.click(); 
			        	ReportEvents.Done(callerClassName+":Radiobutton","Radiobutton " + ClsInitialize.objMobile.deviceTest.element +" successfully selected");
			          //Logger.INFO("Radiobutton", "Radiobutton " + b.element +" successfully selected");
			        }
			}
			catch(Exception e)
			{
				ReportEvents.Error(callerClassName+":Radiobutton::Exception in Selecting the RadioButton", e);
			     Assert.fail();
				// Logger.ERROR("Radiobutton",e);
			}
		}
			
	}
	
}
