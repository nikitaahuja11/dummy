package FrameworkSource.UI.Common;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.*;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;
/**
 * The class consists of all the functions relates to List of web page.
 */
public class DropDown extends UIElement{
	/**
	 * List class constructor, directly invoked when an object of List class is created. Class List extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	/*public DropDown(String Elementvalue) throws IOException, InterruptedException
	{
		//<select> and <li> tag
		super(Elementvalue);
	}*/
	
	public DropDown(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public DropDown( Device d, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	public DropDown( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	/**
	 * The function selects an element from a List <li> tag.
	 * @param value takes string value which needs to be selected from a list.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void SelectItemByText(String value) throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
			
		if((value != null) && (ClsInitialize.objWeb.automationWebDriver !=null))
		{
			try
				{	List<WebElement> Language = ClsInitialize.objWeb.func_getBrowser().element.findElements(By.tagName("li"));
					boolean presence =false;
					for (WebElement li : Language) 
					{			
						if (li.getText().equals(value)) 
						{
						     li.click();
						     ReportEvents.Done(callerClassName+":DropDown",value+" is Clicked from the list.");
						     presence = true;
						     break;
						}			
					}
					if (presence==false)
					{
						ReportEvents.Done(callerClassName+":DropDown",value+" is not listed in the list.");
					}		 
				}		
				catch(Exception e)
					{
					ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting the Text", e);
				     Assert.fail();
					}
			}
		else if((value != null) && (ClsInitialize.objMobile.automationMobileDriver !=null))
		{
			try
			{	List<WebElement> Language = ClsInitialize.objMobile.deviceTest.element.findElements(By.tagName("li"));
				boolean presence =false;
				for (WebElement li : Language) 
				{			
					if (li.getText().equals(value)) 
					{
					     li.click();
					     ReportEvents.Done(callerClassName+":DropDown",value+" is Clicked from the list.");
					  
					     presence = true;
					     break;
					}			
				}
				if (presence==false)
				{
					ReportEvents.Done(callerClassName+":DropDown",value+" is not listed in the list.");
						//Logger.INFO("DropDown", value+" is not listed in the list.");
				}		 
			}		
			catch(Exception e)
				{
				ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting the Text", e);
				//Logger.ERROR("DropDown",e);
			     Assert.fail();
				}
		}
	}
	
	/**
	 * The function selects an element from a select tag <select>.
	 * @param value takes string value which needs to be selected from a list.
	 * Example: In <select id="SelectID"><option value="value1">Sunlife</option></select> Sunlife is the value passed as parameter
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void SelectByText(String value) throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		Select dropdown;
		
		if((value != null) && (ClsInitialize.objWeb.automationWebDriver !=null))
		{
			try
				{
					dropdown = new Select(ClsInitialize.objWeb.func_getBrowser().element);
					dropdown.selectByVisibleText(value);
					ReportEvents.Done(callerClassName+":DropDown",value+" is selected from the dropdown.");
				}
				catch(Exception e)
				{
					ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting the Text", e);
			    // Logger.ERROR("DropDown",e);
			     Assert.fail();
				}
		}
		else if((value != null) && (ClsInitialize.objMobile.automationMobileDriver !=null))
		{
			try
			{
				dropdown= new Select(ClsInitialize.objMobile.deviceTest.element);
				dropdown.selectByVisibleText(value);
				ReportEvents.Done(callerClassName+":DropDown",value+" is selected from the dropdown.");
				//Logger.INFO("DropDown", value+" is selected from the dropdown.");
			}
			catch(Exception e)
			{
				ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting the Text", e);
		    // Logger.ERROR("DropDown",e);
		     Assert.fail();
			}
		}
	}
	
	/**
	 * The function selects an element based on vaue parameter defined under <option> tag.
	 * @param value takes string value from value tag.
	 * Example: In <select id="SelectID"><option value="value1">Sunlife</option></select> value1 is the value passed as parameter
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void SelectByValue(String value) throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		Select dropdown;
		if((value != null) && (ClsInitialize.objWeb.automationWebDriver !=null))
		{
			try
			{
				dropdown = new Select(ClsInitialize.objWeb.func_getBrowser().element);
				dropdown.selectByValue(value);
				ReportEvents.Done(callerClassName+":DropDown",value+" is selected from the dropdown.");
			}
			catch(Exception e)
			{
				ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting the Value", e);
				Assert.fail();
			}
		}
		else if((value != null) && (ClsInitialize.objMobile.automationMobileDriver !=null))
		{
			try
			{
				dropdown= new Select(ClsInitialize.objMobile.deviceTest.element);
				dropdown.selectByValue(value);
				ReportEvents.Done(callerClassName+":DropDown",value+" is selected from the dropdown.");
				//Logger.INFO("DropDown", value+" is selected from the dropdown.");
			}
			catch(Exception e)
			{
				ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting the Value", e);
		     //Logger.ERROR("DropDown",e);
		     Assert.fail();
			}
		}
	
	}
	
/**
 * The function selects an element from a select tag depending upon the index value.
 * @param value takes an integer value.
 * @throws IOException 
 * @throws InterruptedException 
 */
	public void SelectByIndex(int value) throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		Select dropdown;
		if(ClsInitialize.objWeb.automationWebDriver !=null)
		{
			try{
				dropdown= new Select(ClsInitialize.objWeb.func_getBrowser().element);
				dropdown.selectByIndex(value);
				ReportEvents.Done(callerClassName+":DropDown", value+" is selected from the dropdown.");
				}catch(Exception e)
				{
					ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting by Index", e);
					Assert.fail();
				}
		}
		else if(ClsInitialize.objMobile.automationMobileDriver !=null)
		{
			try
			{
				dropdown= new Select(ClsInitialize.objMobile.deviceTest.element);
				dropdown.selectByIndex(value);
				ReportEvents.Done(callerClassName+":DropDown", value+" is selected from the dropdown.");
			}catch(Exception e)
			{
					ReportEvents.Error(callerClassName+":DropDown:: Exception in Selecting By Index", e);
					Assert.fail();
			}
		}
		
		
	}
	
	/**
	 * The function retrieves the value of current element which is selected.
	 * @param value takes an integer value.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public String GetSelectedText() throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		String SelectedText = null;
		Select dropdown = null;
		
		if(ClsInitialize.objWeb.fnGetAutomationWebDriver() != null)
		{
			dropdown = new Select(ClsInitialize.objWeb.func_getBrowser().element);
		}
		else if(ClsInitialize.objMobile.automationMobileDriver != null)
		{
			dropdown= new Select(ClsInitialize.objMobile.deviceTest.element);
		}
		
		try
		{
			
			WebElement dropdownoption = dropdown.getFirstSelectedOption();
			SelectedText =  dropdownoption.getText();
		}
		catch(Exception e)
		{
			ReportEvents.Error("DropDown:: Exception in Get Selecting text",e);
	     
		}
		return SelectedText;
	}
	
	/**
	 * The function verifies currently selected drop-down text of select tag
	 * @param value takes an String value to be verified.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public boolean VerifySelectedText(String Value) throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		Boolean check = false;
		Select dropdown = null;
		
		if(ClsInitialize.objWeb.fnGetAutomationWebDriver() != null)
		{
			dropdown = new Select(ClsInitialize.objWeb.func_getBrowser().element);
		}
		else if(ClsInitialize.objMobile.automationMobileDriver != null)
		{
			dropdown= new Select(ClsInitialize.objMobile.deviceTest.element);
		}
		try
		{
			WebElement dropdownoption = dropdown.getFirstSelectedOption();
			String SelectedText =  dropdownoption.getText();
			if (Value.equals(SelectedText))
			{
				check = true;
			}									
		}
		catch(Exception e)
		{
		    Logger.ERROR("DropDown:: Exception in Verifying Selected Text",e);
		    check = false;
		}
	return check;
	}
	
	/**
	 * The function returns total elements in a list.
	 * @return integer value.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public int GetItemCount() throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		Select dropdown = null;
		try{
			if(ClsInitialize.objWeb.fnGetAutomationWebDriver() != null)
			{
				dropdown = new Select(ClsInitialize.objWeb.func_getBrowser().element);
			}
			else if(ClsInitialize.objMobile.automationMobileDriver != null)
			{
				dropdown= new Select(ClsInitialize.objMobile.deviceTest.element);
			}
		}catch(Exception obj)
		{
			ReportEvents.Error("GetItemCount:: Exception in Get Item Count",obj);
			Assert.fail();
		}
		
		return  dropdown.getOptions().size(); 	
	}

	/**
	 * The function verifies whether an element is present in drop-down or not.
	 * @param value takes an String value to be verified.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */  
	public Boolean VerifyTextListedInDropDown(String value) throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		Boolean found = false;
		Select dropdown = null;
		
		if(ClsInitialize.objWeb.fnGetAutomationWebDriver() != null)
		{
			dropdown = new Select(ClsInitialize.objWeb.func_getBrowser().element);
		}
		else if(ClsInitialize.objMobile.automationMobileDriver != null)
		{
			dropdown= new Select(ClsInitialize.objMobile.deviceTest.element);
		}
		try
		{
			List<WebElement> dropdownoptions = dropdown.getOptions();
			for(WebElement we:dropdownoptions)  
	        {   if (we.getText().equals(value))
	               {
	            	   found = true;
	            	   break;
	                } 
	         }
		}		
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":DropDown", e);
		    found = false;
		}
		return found;
		
	}

}
