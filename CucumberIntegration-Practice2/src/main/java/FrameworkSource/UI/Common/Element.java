package FrameworkSource.UI.Common;

import java.io.IOException;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;

/**
 * The class consists of all the functions related to WebElement.
 */

public class Element extends UIElement
{
	/**
	 * WebElement class constructor, directly invoked when an object of WebElement class is created. Class List extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */

	/*public Element(String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}*/
	
	public Element(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public Element( Device d, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	public Element( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
}
