package FrameworkSource.UI.Mobile;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import FrameworkSource.UI.Common.UIElement;
import FrameworkSource.global.Read_OR_Data.ORReader;
import FrameworkSource.global.reporter.ReportEvents;


/**
 * The class consists of all the functions relates to App Class.
 */
public class App{
	
	Device d;
	
	/**
	 * App class constructor, directly invoked when an object of App class is created.
	 * @param d is a static global variable in Device class which maintains properties of Device session.
	 */

	public App(Device d)
	{
		this.d = d;
	}
	
	/**
	 * The function sets page value depending upon the string value provided. This function calls Properties function in ORReaded class.	
	 * @param Value takes string value.
	 * @throws IOException
	 * @throws InterruptedException 
	 * @throws ParseException 
	 */
	
	public void SetCurrentApp(String Value) 
	{	
		try{
			String callerClassName = new Exception().getStackTrace()[1].getMethodName();
			//Handled empty parameter passed in SetCurrentPage
			if(Value == "")
			{
				ReportEvents.Fatal(callerClassName+":Page", "SetCurrentApp() parameter cannot be blank");
				Assert.fail();
			}
			else
			d.PageValues= new ORReader().Properties(Value);
		}catch(Exception e)
		{
			
		}
	}
	
	/**
	 * The function sets page value depending upon the string value and excel name provided. This function calls Properties function in ORReaded class.	
	 * @param Value takes string value.
	 * @throws IOException
	 * @throws InterruptedException 
	 * @throws ParseException 
	 */
	public void SetProperty(String strLogicalName, String strPropertyID, String strPropValue) throws IOException
	{
		new ORReader().fnSetProperty(strLogicalName, strPropertyID, strPropValue);
	}
	public void SetCurrentApp(String Value , String excelname) throws IOException, InterruptedException
	{
		//d.PageValues = new ORReader().Properties(Value, excelname);
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		//Handled empty parameter passed in SetCurrentPage
		if(Value == "")
		{
			ReportEvents.Fatal(callerClassName+":Page", "SetCurrentApp() parameter cannot be blank");
			Assert.fail();
		}
		else
		d.PageValues= new ORReader().Properties(Value);
	}
	/**
	 * The function waits for specified number of seconds.	
	 * @param seconds takes value of type int.
	 * @throws InterruptedException
	 * @throws IOException 
	 */
	public void Wait(int seconds) throws InterruptedException, IOException
	{
		//int total = Integer.parseInt(seconds+"000");
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		Thread.sleep(seconds*1000);		
		ReportEvents.Done(callerClassName+":App","Waited for "+seconds+" seconds.");
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			 //Assert.fail();
			
		}
	}
	
	/**
	 * This function Scrolls vertically till top  of the Web
	 * @throws IOException
	 */
	
	public void ScrollTillTop_Web() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		
		((JavascriptExecutor)d.driver).executeScript("window.scrollTo(document.body.scrollHeight,0)");
		ReportEvents.Done(callerClassName+":App","Scrolled till Height of the Web");
		
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			 Assert.fail();
		}
		
	}
	
	/**
	 * This function Scrolls vertically till bottom  of the Web
	 * @throws IOException
	 */
	
	public void ScrollDownTillBottom_Web() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		((JavascriptExecutor)d.driver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			 Assert.fail();
		}
		
	}
	
	/**
	 *  This function Scrolls Vertically on the mobile Web according to given pixels 
	 * @param vertical_pos for giving pixels
	 * @throws IOException 
	 */
	public void ScrollVericallyByPixels_Web(String vertical_pos) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		((JavascriptExecutor)d.driver).executeScript("window.scrollBy(0,"+vertical_pos+")");
		ReportEvents.Done(callerClassName+":App","Scrolled Vertically the Web");
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			   Assert.fail();
		}
	}
	/**
	 * This function Scrolls Horizontally on the mobile Web according to given pixels
	 * @param horizontal_pos for pixels
	 * @throws IOException 
	 */
	public void ScrollHorizontallyByPixels_Web(String horizontal_pos) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		((JavascriptExecutor)d.driver).executeScript("window.scrollBy("+horizontal_pos+",0)");
		ReportEvents.Done(callerClassName+":App","Scrolled Vertically the Web");
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			 Assert.fail();
			
		}
	}
	
	/**
	 * This function navigates back on the Mobile-Web
	 * @throws IOException
	 */
	public void  NavigateBack_Web() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		d.driver.navigate().back();
		ReportEvents.Done(callerClassName+":App","Navigate back on the Web");
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			 Assert.fail();
			
		}
	}
	/**
	 * This function refresh page on the Mobile-Web
	 * @throws IOException
	 */
	public void Refresh_Web() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		d.driver.navigate().refresh();
		ReportEvents.Done(callerClassName+":App","Refreshed the Web");
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			 Assert.fail();
			   
		}
	}
	/**
	 * This function navigates forward on the Mobile-Web
	 * @throws IOException
	 */
	public void NavigateForward_Web() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		d.driver.navigate().forward();
		ReportEvents.Done(callerClassName+":App","Navigate forward on the Web");
		}
		catch(Exception e)
		{
			 ReportEvents.Error(callerClassName+":App", e);
			 Assert.fail();
			
		}
	}
	
	  /**
		 * The function for Android Device, Scroll  until the specified Instance of Text and Click that
		 * Arguments : expectedText, Instance to be clicked
	 * @throws IOException 
		 */
	    
	public void ScrollUntilTextExists_Android(String expectedText, int instance) throws IOException 
	{
	   String callerClassName = new Exception().getStackTrace()[1].getMethodName();
	   try
	   {
	   MobileElement element = (MobileElement) ((FindsByAndroidUIAutomator) d.driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+expectedText+"\").instance("+(instance-1)+"))");
	   ReportEvents.Done(callerClassName+":App","Screen Scrolled until: "+expectedText + " text in Android");
	   }
	   catch(Exception e)
	   {
		   ReportEvents.Error(callerClassName+":App", e);
		   Assert.fail();
	   }
	}
	    
    public MobileElement ScrollUntilTextExists_AndroidElement(String expectedText, int instance) throws IOException, InterruptedException 
    {
        
        
        MobileElement element = null;
        Thread.sleep(5000);
       System.out.println(expectedText);
       String callerClassName = new Exception().getStackTrace()[1].getMethodName();
       try
       {
       element = (MobileElement) ((FindsByAndroidUIAutomator) d.driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+expectedText+"\").instance("+(instance-1)+"))");
       ReportEvents.Done(callerClassName+":App","Screen Scrolled until: "+expectedText + " text in Android");
       }
       catch(Exception e)
       {
           element = null;
           e.printStackTrace();
           ReportEvents.Error(callerClassName+":App", e);
           Assert.fail();
       }
       return element;
    }
        
  

		/**
		 * The function for IOS, Scroll down until the specifies Text
		 * Argument : "Text" to be looked for to scroll
		 * @throws IOException 
		 */
	    public void ScrollUntilTextExists_IOS(String text) throws IOException 
	    {
	    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
	    	try
	    	{
	    	JavascriptExecutor js = (JavascriptExecutor) d.driver;
	        HashMap scrollObject = new HashMap<>();
	        scrollObject.put("predicateString", "value == '" + text + "'");
	        scrollObject.put("direction", "down");
	             js.executeScript("mobile: scroll", scrollObject);
	             ReportEvents.Done(callerClassName+":App","Screen Scrolled until: "+text + " text in iOS");
	    	}
	    	catch(Exception e)
	    	{
	    		ReportEvents.Error(callerClassName+":App", e);
	    		Assert.fail();
	    		
	    	}
	            
	    	}
	    
	    /**
	  	 * The function for IOS, Scroll Up Number of times Mentioned
	     * @throws IOException 
	  	 */
	        public void ScrollUpNoOfTimes_IOS(int times) throws IOException
	        {
	        String callerClassName = new Exception().getStackTrace()[1].getMethodName();
	        try
	        {
	      	JavascriptExecutor js = (JavascriptExecutor) d.driver;
	          HashMap scrollObject = new HashMap<>();
	          for(int i=0;i<times;i++)
	          {
	          scrollObject.put("direction", "up");
	               js.executeScript("mobile: scroll", scrollObject);
	          }
	          ReportEvents.Done(callerClassName+":App","Screen Scrolled for : "+times + " in iOS");
	        }
	        catch(Exception e)
	        {
	        	ReportEvents.Error(callerClassName+":App", e);
	        	Assert.fail();
	        }
	      	}
	        
	        /**
		  	 * The function for IOS, Scroll down Number of times Mentioned
		     * @throws IOException 
		  	 */
	        public void ScrollDownNoOfTimes_IOS(int times) throws IOException
	        {
	        String callerClassName = new Exception().getStackTrace()[1].getMethodName();
	      	JavascriptExecutor js = (JavascriptExecutor) d.driver;
	          HashMap scrollObject = new HashMap<>();
	          for(int i=0;i<times;i++)
	          {
	          scrollObject.put("direction", "down");
	               js.executeScript("mobile: scroll", scrollObject);
	          }
	          ReportEvents.Done(callerClassName+":App","Screen Scrolled for : "+times + " in iOS");
	      	}
	        

	/**
	* The function for IOS, Swipe the screen to  Right
	* Argument : "Text" to be looked for to scroll
	* @throws IOException 
	*/    
	    
    public void SwipeRight() throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	try
    	{
        Dimension size = d.driver.manage().window().getSize();
        int startx = (int) (size.width * 0.9);
        int endx = (int) (size.width * 0.20);
        int starty = size.height / 2;
        new TouchAction(d.driver).press(startx, starty)
                .waitAction(Duration.ofSeconds(2))
                .moveTo(endx,starty).release().perform();
        ReportEvents.Done(callerClassName+":App","Screen Swiped Right");
    	}
    	
    	catch(Exception e)
    	{
    		ReportEvents.Error(callerClassName+":App", e);
    		Assert.fail();
    	}
    }
    

	/**
	* The function for IOS, Swipe the screen to  Left
	* Argument : "Text" to be looked for to scroll
	* @throws IOException 
	*/ 
    public void SwipeLeft() throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	try
    	{
        Dimension size = d.driver.manage().window().getSize();
        int startx = (int) (size.width * 0.8);
        int endx = (int) (size.width * 0.20);
        int starty = size.height / 2;
        new TouchAction(d.driver).press(startx, starty)
                .waitAction(Duration.ofSeconds(3000))
                .moveTo(endx,starty).release();
        ReportEvents.Done(callerClassName+":App","Screen Swiped Left");
    	}
    	catch(Exception e)
    	{
    		ReportEvents.Error(callerClassName+":Page", e);
    		Assert.fail();
    	}
    }
    
  
    /**
	 * The function scrolls till the particular web element. This will work for Mobile-Web Elements
	 * @throws IOException 
	 */
    public void ScrollTillElement(WebElement element) throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	try
    	{
    	JavascriptExecutor js = (JavascriptExecutor) d.driver;
    	HashMap scrollObject = new HashMap<String, String>();
    	scrollObject.put("direction", "down");
    	 scrollObject.put("element", ((RemoteWebElement) element).getId());
         scrollObject.put("toVisible", "true");
    	js.executeScript("mobile: scroll", scrollObject);
        ReportEvents.Done(callerClassName+":App","Screen Scrolled until: "+ element + " (WebElement) in iOS");
    	}
    	
    	catch(Exception e)
    	{
    	 ReportEvents.Error(callerClassName+":App",e);
   	     Assert.fail();
    	}
    }
    
    /**
     * Scroll On Mobile Web by specifying  Number of Times
     * @param times
     * @throws IOException 
     */
    public void ScrollDownUsingTouchactions(int times) throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	try
    	{
    	for(int i = 0 ; i<=times ; i++)
    	{
    		TouchAction action1=new TouchAction(d.driver);
			  action1.press(300, 500).waitAction().moveTo(300, 200).release().perform();
    	}
    	ReportEvents.Done(callerClassName+":App","Scrolled" + times + "Times Down");
    	}
    	catch(Exception e)
    	{
    		ReportEvents.Error(callerClassName+":App", e);
    		Assert.fail();
    	}
    }
    
    /**
     *Scroll On Mobile Web by specifying  Number of Times
     * @param times
     * @throws IOException
     */
    public void ScrollUpUsingTouchactions(int times) throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	try
    	{
    	for(int i = 0 ; i<=times ; i++)
    	{
    		TouchAction action1=new TouchAction(d.driver);
			  action1.press(300, 200).waitAction().moveTo(300, 500).release().perform();
    	}
    	ReportEvents.Done(callerClassName+":App","Scrolled" + times + "Times Up");
    	}
    	catch(Exception e)
    	{
    		ReportEvents.Error(callerClassName+":App", e);
    		
    	}
    }
          
   
   /**
    *  Gives the session ID as a String of the opened application 
    * @return
    * @throws IOException
    */
    public String GetSessionID() throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	ReportEvents.Done(callerClassName+":App","Returning the Session ID");
    	return d.driver.getSessionId().toString();
    	
     }
    
    
    /**
     * Closes the opened application in both android and iOS
     * @throws IOException
     */
    public void CloseApp() throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	try
    	{
    	d.driver.closeApp();
    	ReportEvents.Done(callerClassName+":App","Application Closed");
    	}
    	catch(Exception e)
    	{
    		ReportEvents.Error(callerClassName+":App",e);
      	    Assert.fail();
    	}
    }

 /**
  *  Resets the opened application i.e. all the values/setting will be refreshed in the app
  * @throws IOException
  */
    public void ResetApp() throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	try
    	{
	    	d.driver.resetApp();
	    	ReportEvents.Done(callerClassName+":App","Application Reset");
    	}
    	catch(Exception e)
    	{
    		ReportEvents.Error(callerClassName+":App",e);
      	    //Assert.fail();
    	}
    }
  
    /**
     * This method selects the date in the iOS device.
     * @param driver2 is the IOS driver
     * @param list is the date in format: "Month", "Date","Year"
     */
    public void CalendarSet_iOS(IOSDriver driver2, String[] list) {
        System.out.println("Starting the process");
        for (int i = 0; i < list.length; i++) {
            MobileElement we = (MobileElement) d.driver.findElementByXPath("//UIAPickerWheel["+(i+1)+"]");
            we.sendKeys(list[i]);
        }
        System.out.println("Ending Process");
        
    }
    
    /**
     * This method SwitchContext - native to web window
     * @param number is window number
     */
    public void SwitchContext(int number)
    {
               Set<String> contexts = d.driver.getContextHandles();


                d.driver.context((String) contexts.toArray()[number-1]);
                
                System.out.println("execute context switch successfully!!!  "
                        + contexts.toArray()[number-1]);

           
        }

    /**
     * This method selects the date in the Android Device
     * @param strDate is the date given by the user
     * @throws IOException 
     */
    
	@SuppressWarnings("deprecation")
	public void CalendarSet_Android(String strDate) throws IOException
    {

		TouchAction action = new TouchAction((PerformsTouchActions)d.driver);  
		boolean blnDateExist = false;
		boolean blnSwipeUp = false;
		boolean blnYear = false;
		int iNoofSwipes = 0,iInstanceFrom,iInstanceTo;
		WebElement source;
		WebElement destination;
		LocalDate dtLocalCurrentDate,dtLocalExpDate;
		
		 Date dtCurrentDate = new Date();
		 Date dtExpectedDate = new Date(strDate);
		 
		 dtLocalCurrentDate = dtCurrentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 dtLocalExpDate = dtExpectedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		
		 iInstanceFrom = 1;
		 iInstanceTo = 30;
	    
		 
		 d.driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		 if (dtLocalCurrentDate.getYear() != dtLocalExpDate.getYear())
		 {
			 d.driver.findElementsById("com.sunlife.mobile.mysunlife:id/date_picker_year").get(0).click();
			 blnYear = d.driver.findElements(By.xpath("//android.widget.TextView[@content-desc='" + dtLocalExpDate.getYear() + "']")).isEmpty();
			 if (blnYear)
			 {
				 System.out.println("Calendar year not Found");
				 return;
			 }
			 else
			 {
				 d.driver.findElements(By.xpath("//android.widget.TextView[@content-desc='" + dtLocalExpDate.getYear() + "']")).get(0).click();
				 
			 }
			 
			
		 }		 	
		 
		 if (dtCurrentDate.getMonth() > dtExpectedDate.getMonth())
		 {
			 blnSwipeUp = true;
			 iNoofSwipes = iNoofSwipes + (dtCurrentDate.getMonth()-dtExpectedDate.getMonth());
		 }
		 else
		 {
			 blnSwipeUp = false;
			 iNoofSwipes = iNoofSwipes + (dtExpectedDate.getMonth()-dtCurrentDate.getMonth());
		 }

	    if (iNoofSwipes>0)
	    {
	    	blnDateExist = true;
	    }
	    else
	    {
	    	try{
	    		blnDateExist = d.driver.findElements(By.xpath("//android.view.View[@content-desc='" + strDate + "']")).isEmpty();
			}
			catch(Exception e1)
			{
				ReportEvents.Error("Error: Calendar Date Picker", e1);
			}
	    	
	    }
	    
	    
	    
		while(blnDateExist!=false)
		{
			 	
			    try{
				    for (int iCountSwipes = 1;iCountSwipes<=iNoofSwipes;iCountSwipes++)
				    {
				    	
				    	source = d.driver.findElement(By.xpath("//android.view.View[@instance='" + iInstanceFrom + "']"));    
					    destination = d.driver.findElement(By.xpath("//android.view.View[@instance='"+ iInstanceTo + "']"));
				    	if (blnSwipeUp)
					    {
				    		
					    	action.longPress(source).waitAction(Duration.ofMillis(100)).moveTo(destination).release().perform();
					    	
					    	
					    }
					    else
					    {
					    	action.longPress(destination).moveTo(source).release().perform();
					    }
				    }
				    blnDateExist = d.driver.findElements(By.xpath("//android.view.View[@content-desc='" + strDate + "']")).isEmpty();
				    iNoofSwipes = 1;
			    }catch(Exception exIdentification)
			    {
			    	System.out.println(exIdentification.getMessage());
			    }
		}
		d.driver.findElementsByXPath("//android.view.View[@content-desc='" + strDate + "']").get(0).click();
    }
    

	public void fnScrollUntilTextFound(String expectedText,int instance) throws IOException
	{
		
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean blnFlagSwipe = true;
		Dimension dmnSize;
		int iStartY, iEndY,iStartX,iCountSwipe = 0 ;
		String strPrevScrollContent, strNewScrollContent;
		try
		{
			if (d.driver.getPageSource().toLowerCase().contains(expectedText.toLowerCase()))
			  {
				blnFlagSwipe = false;
			  }
			
			while(blnFlagSwipe)
			{
				dmnSize = d.driver.manage().window().getSize();
				iStartY = (int) (dmnSize.height * 0.80);
				iEndY = (int) (dmnSize.height * 0.20);
				iStartX = dmnSize.width / 2;
				new TouchAction(d.driver).press(iStartX, iStartY).waitAction().moveTo(iStartX, iEndY).release().perform();
				strNewScrollContent = d.driver.getPageSource().toLowerCase();
				if (d.driver.getPageSource().toLowerCase().contains(expectedText.toLowerCase()))
				  {
					iCountSwipe++;
					if (iCountSwipe == instance)
					{
						blnFlagSwipe = false;
					}
					
				  }
				strPrevScrollContent = d.driver.getPageSource().toLowerCase();
				
				if (strNewScrollContent.equalsIgnoreCase(strPrevScrollContent))
				{
					//since the content after the scroll and before the scroll are same, hence we have reached the end of screen
					blnFlagSwipe = false;
				}
			}
		}catch(Exception eScrollFail)
		   {
			   ReportEvents.Error(callerClassName+":App", eScrollFail);
			   Assert.fail();
		   }
	}
	
	
	
	 
}