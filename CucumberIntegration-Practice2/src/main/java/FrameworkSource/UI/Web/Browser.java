package FrameworkSource.UI.Web;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.*;

/**
 * The class consists of all the customized functions related to browsers.
 */
public class Browser extends ClsInitialize{
	


	public  WebDriver driver = null;
	public WebElement element = null;
	public List<WebElement> listOfElements = null;
	public String[][] PageValues;
	
/**
* This function launches a browser version either Chrome, IE or Firefox.
* @param BrowserType is a string parameter which takes either values of Chrome, IE or Firefox.
* @throws InterruptedException
 * @throws IOException 
*/
	
	public Browser() throws IOException, InterruptedException
	{
		driver = ClsInitialize.objWeb.fnGetAutomationWebDriver();
	}
	
	

/**
 * The function opens a URL in the launched browser.
 * @param url takes a String value.
 * @throws IOException 
 */
	public void NavigateURL(String url) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
			ClsInitialize.objWeb.fnGetAutomationWebDriver().get(url);
			
		ReportEvents.Done(callerClassName+":Browser", "Opening URL "+ url);
		//Logger.INFO("Browser","Opening URL "+ url);
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
		     Assert.fail();
		}
		
	}
	/**
	 * The function closes the open browser.
	 * @throws IOException 
	 */
	public void Quit() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
			driver.quit();
			ReportEvents.Done(callerClassName+":Browser",  "Browser is Closed");
			//Logger.INFO("Browser", "Browser is Closed");	
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
		}
	}
	/**
	 * The function Maximizes the browser.
	 * @throws IOException 
	 */
	public void Maximize() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
		driver.manage().window().maximize();
		ReportEvents.Done(callerClassName+":Browser",  "Browser Maximize");
		//Logger.INFO("Browser", "Browser Maximize");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		     Assert.fail();
		}
		
	}
	/**
	 * The function switches to desired open windows of browser.
	 * @param windownumber takes integer value. Eg.1 for first window, 2 for second window
	 * @throws IOException 
	 */
	public void GetBrowser(int windownumber) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		ReportEvents.Done(callerClassName+":Browser",  "Total No. of windows opened" + tabs.size());
		//Logger.INFO("Browser", "Total No. of windows opened" + tabs.size());
		driver.switchTo().window((String) tabs.get(windownumber-1));
		ReportEvents.Done(callerClassName+":Browser",  "Windows switched successfully");
		//Logger.INFO("Browser", "Windows switched successfully");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		     Assert.fail();
		}

	}
	
	/**
	 * The function switches to desired open windows of browser.
	 * @param title takes the current page title.
	 * @throws IOException 
	 */
	public void GetBrowser(String title) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			ReportEvents.Done(callerClassName+":Browser",  "Total No. of windows opened" + tabs.size());
			//Logger.INFO("Browser", "Total No. of windows opened" + tabs.size());
			String currentWindow = driver.getWindowHandle();  //will keep current window to switch back
			for(String winHandle : driver.getWindowHandles()){
				System.out.println(driver.switchTo().window(winHandle).getTitle() + "--------" + title);
			   if (driver.switchTo().window(winHandle).getTitle().equalsIgnoreCase(title)) {
				   System.out.println("Windows switched successfully");
				   ReportEvents.Done(callerClassName+":Browser",  "Windows switched successfully");
					//Logger.INFO("Browser", "Windows switched successfully");
			     break;
			   } 
			   else {
			      driver.switchTo().window(currentWindow);
			   } 
			}
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		     Assert.fail();
		}

	}
	
	/**
	 * The function clicks Back button of opened browser.
	 * @throws IOException 
	 */
	public  void Back() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
		driver.navigate().back();
		ReportEvents.Done(callerClassName+":Browser",  "Browser Back button is pressed successfully. ");
		//Logger.INFO("Browser", "Browser Back button is pressed successfully. ");	
	}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		     Assert.fail();
		}
	}
	/**
	 * The function clicks on the Forward button of opened browser.
	 * @throws IOException 
	 */
	public  void Forward() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
		driver.navigate().forward();
		ReportEvents.Done(callerClassName+":Browser",  "Browser Forward button is pressed successfully. ");
		//Logger.INFO("Browser", "Browser Forward button is pressed successfully. ");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		     Assert.fail();
		}
		
	}
	/**
	 * The function clears all cookies of the browser.
	 * @throws IOException 
	 */
	public void ClearCookies() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
		driver.manage().deleteAllCookies();
		ReportEvents.Done(callerClassName+":Browser",  "All Cookies has been deleted successfully. ");
		//Logger.INFO("Browser", "All Cookies has been deleted successfully. ");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		    // Assert.fail();
		}
	}
	
	/**
	 * The function clears specific cookie by name.
	 * @throws IOException 
	 */
	public void ClearCookies(String name) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
		driver.manage().deleteCookieNamed(name);
		ReportEvents.Done(callerClassName+":Browser",  name+" cookie has been deleted successfully. ");
		//Logger.INFO("Browser", name+" cookie has been deleted successfully. ");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		    // Assert.fail();
		}
	}
	
	
	/**
	 * The function refresh the opened open session of browser.
	 * @throws IOException 
	 */
	public void Refresh() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try{
		driver.navigate().refresh();
		ReportEvents.Done(callerClassName+":Browser",   "Browser Page Refresh successfully.");
		//Logger.INFO("Browser", "Browser Page Refresh successfully.");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		    //Assert.fail();
		}
	}
	
	/**
	 * The function will kill any task.
	 * @throws IOException 
	 */	
	public void KillTask(String taskName) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		if(!taskName.contains(".exe"))
		{
			taskName = taskName + ".exe"; 
		}

		try{
		Runtime.getRuntime().exec("Taskkill /IM "+taskName+" /F");
		ReportEvents.Done(callerClassName+":Browser",   taskName+" killed successfully.");
		//Logger.INFO("Browser", "Browser Page Refresh successfully.");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		     //Assert.fail();
		}
	}
	
	/**
	 * The function inputs various keys from keyboard.
	 * @param command takes string value which needs to be input from keyboard like Down, Left, Up, Right, Backspace, Alt, Ctrl, Delete, Enter and Spacebar
	 * @param times takes a integer value of number of times the command is to be executed
	 * @throws IOException 
	 */
	public void PressKeyonPage(String command, int times) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		String command1 = command.substring(0,1).toUpperCase() + command.substring(1).toLowerCase();
		Actions actions = new Actions(driver);
		try
		{			
			switch(command1)
			{
				case "Enter":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.ENTER).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","Enter Key is pressed successfully.");
				break;
							
				case "Down":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.DOWN).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","DownArrow Key is pressed successfully.");
				break;
				
				case "Up":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.UP).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","UpArrow Key is pressed successfully.");
				break;		
				
				case "Left":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.LEFT).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","UpArrow Key is pressed successfully.");
				break;	
				
				case "Right":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.RIGHT).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","UpArrow Key is pressed successfully.");
				break;	
					
				case "Backspace":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.BACK_SPACE).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","Backspace Key is pressed successfully.");
				break;
						
				case "Spacebar":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.SPACE).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","Spacebar Key is pressed successfully.");
				break;
				
				case "Tab":
				for(int i=1;i<=times;i++)
				{
					actions.sendKeys(Keys.TAB).build().perform();
				}
				ReportEvents.Done(callerClassName+":Browser","Spacebar Key is pressed successfully.");
				break;
				
				
				default: 
					ReportEvents.Fatal(callerClassName+":Browser", "Invalid key name.");		
					}	
			
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
		     Assert.fail();
		}
	}
	
	
}