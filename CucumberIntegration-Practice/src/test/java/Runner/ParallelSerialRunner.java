package Runner;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import org.junit.Test;

public class ParallelSerialRunner  
{
	
	@Test
	public void test() throws IOException, InterruptedException
	{
		FrameworkSource.global.reporter.ReportEvents.DeleteFolder(new File(System.getProperty("user.dir")+ "//src//test//java//test"));	
		 Runtime rt = Runtime.getRuntime();
		 // Serial Execution
		//Process p = rt.exec("cmd /c mvn -DfailIfNoTests=false -Dtest=TestRunner test");
		
		 // Parallel Execution
		Process p = rt.exec("cmd /c mvn test -Dclass.value=\"**/Parallel*IT.java\"");
		 
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while ((reader.readLine()) != null) {}
			p.waitFor();
	}

}
