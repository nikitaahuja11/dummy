package StepDefinations.Web_Tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import FrameworkSource.UI.Web.Browser;
import FrameworkSource.UI.Web.Page;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.ReportEvents;
import UserDefinedFunctions.Web.TwiiterFunctions;
import cucumber.api.java.en.*;

public class TwitterData_StepDef {

Browser browser = ClsInitialize.iobjBrowser;
Page page = ClsInitialize.iobjPage;
TwiiterFunctions twitter = new TwiiterFunctions(browser , page);
int MaxTweet = 0;
int MaxLikes =0;
Map<String,List<String>> Follows =new HashMap();
List<String> Top10HashTags = new ArrayList<String>();
@Given("^Twitter webpage is available with stepin page open$")
public void twitter_webpage_is_available_with_stepin_page_open() throws Throwable 
{
	ReportEvents.Reporter("Pass", "StepIn Page Open", "StepIn Page in Twitter is opened");
}
@When("^User extracts top (\\d+) tweets$")
public void user_extracts_top_tweets(int arg1) throws Throwable 
{
	for(int i = 0 ; i< 4 ; i++)
	{
	page.ScrollTillEnd();
	page.Wait(7);
	}
	if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
	{twitter.Tweets();}
	else
	{
		//COde for Mobile
	}
	
}
@When("^User finds tweet with highest retweets$")
public void user_finds_tweet_with_highest_retweets() throws Throwable
{
	if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
	MaxTweet=twitter.MaxRetweets();
	else
	{
		//COde for Mobile
	}
	
}
@When("^User finds tweet with highest likes$")
public void user_finds_tweet_with_highest_likes() throws Throwable 
{
	if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
	MaxLikes=twitter.MaxLikes();
	else
	{
		//COde for Mobile
	}
}
@When("^User finds tweet with (\\d+) highest used hashtags$")
public void user_finds_tweet_with_highest_used_hashtags(int arg1) throws Throwable
{
	if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
	Top10HashTags=twitter.HashTags();
	else
	{
		//COde for Mobile
	}
}
@When("^User finds the list of people under ‘who to follow’$")
public void user_finds_the_list_of_people_under_who_to_follow() throws Throwable {
	for(int i = 0 ; i< 2 ; i++)
	{
	page.ScrollToTop();
	page.Wait(2);
	}
	if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
	Follows=twitter.Follows();
	else
	{
		//COde for Mobile
	}
	
}
@Then("^User creates a Summary as a JSON$")
public void user_creates_a_Summary_as_a_JSON() throws Throwable
{
	if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
	twitter.JSONCreate(MaxTweet,MaxLikes,Top10HashTags,Follows.get("FollowerName"),Follows.get("HandleName"),Follows.get("FollowingName"),Follows.get("FollowerFollowName"));
	else
	{
		//COde for Mobile
	}
	
	
}
@Then("^Upload JSON summary to the server$")
public void upload_JSON_summary_to_the_server() throws Throwable {
	if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
	twitter.UploadJSON();
	else
	{
		//COde for Mobile
	}
}
}