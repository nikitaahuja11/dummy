package StepDefinations.Mobile_Tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.collect.Lists;

import FrameworkSource.global.Intialize.CommonImplementation;

public class Test {
	

public static void main(String[] a) throws InterruptedException
{
WebDriver driver =null;
System.setProperty("webdriver.chrome.driver",CommonImplementation.strChromeDriverPath);
driver = new ChromeDriver();
driver.get("https://twitter.com/stepin_forum");
driver.manage().window().maximize();
JavascriptExecutor js = (JavascriptExecutor) driver;
    //  js.executeScript("window.scrollBy(0,1000)");
      js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
      Thread.sleep(2000);
      //js.executeScript("window.scrollBy(0,1000)");
      js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
      Thread.sleep(2000);
      //js.executeScript("window.scrollBy(0,1000)");
      js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
      Thread.sleep(2000);
      js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
      Thread.sleep(2000);
      List<String> TweetList= new ArrayList<String>();
      List<String> ReTweetList= new ArrayList<String>();
      List<String> LikesList= new ArrayList<String>();
  	List<Integer> ListLikes =  new ArrayList<Integer>();
  	List<Integer> ListRetweet =  new ArrayList<Integer>();
      List<WebElement> Tweet =  driver.findElements(By.xpath("//div[@class='stream']/ol/li/div[1]/div[2]/div[2]/p[1]"));
      List<WebElement> Retweet =  driver.findElements(By.xpath("//div[@class='stream']/ol/li/div[1]/div[2]/div[4]/div[2]/div[2]/button[1]/span[1]/span[1]"));
      List<WebElement> Likes =  driver.findElements(By.xpath("//div[@class='stream']/ol/li/div[1]/div[2]/div[4]/div[2]/div[3]/button[1]/span[1]/span[1]"));
      //System.out.println(Retweet.size());
      
      for(int i=0;i<49;i++)
      {
      //a.get(i).ge
   	  TweetList.add(Tweet.get(i).getText());
   	  ReTweetList.add(Retweet.get(i).getText());
   	  LikesList.add(Likes.get(i).getText());
   	System.out.println("Tweet: "+ Tweet.get(i).getText()+", Retweet: "+Retweet.get(i).getText()+", Likes: "+Likes.get(i).getText());
   	ListLikes = Lists.transform(LikesList, Integer::parseInt);
   	ListRetweet = Lists.transform(ReTweetList, Integer::parseInt);
      }
      
      
      HashMap<String,Integer> likes=new HashMap<String, Integer>();
      HashMap<String,Integer> retweets=new HashMap<String, Integer>();
      
      for(int j= 0 ; j<LikesList.size() ; j++)
      {
    	    likes.put(TweetList.get(j), ListLikes.get(j));
    	    retweets.put(TweetList.get(j), ListRetweet.get(j));
    	  
      }
      
      int MaxLikes=(Collections.max(likes.values()));  // This will return max value in the Hashmap
      for (Entry<String, Integer> entry : likes.entrySet()) {  // Itrate through hashmap
          if (entry.getValue()==MaxLikes) {
              System.out.println("Maximun Likes are "+MaxLikes+" of this tweet : "+entry.getKey());     // Print the key with max value
          }
      }
      int MaxRetweets=(Collections.max(retweets.values()));  // This will return max value in the Hashmap
      for (Entry<String, Integer> entry : retweets.entrySet()) {  // Itrate through hashmap
          if (entry.getValue()==MaxRetweets) {
              System.out.println("Maximun Retweets are "+MaxRetweets+" of this tweet : "+entry.getKey());     // Print the key with max value
          }
      }
}
}
