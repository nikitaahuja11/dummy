package StepDefinations.Mobile_Tests;

import FrameworkSource.UI.Mobile.Device;

import org.openqa.selenium.By;

import FrameworkSource.UI.Common.Button;
import FrameworkSource.UI.Common.TextBox;
import FrameworkSource.UI.Mobile.App;
import FrameworkSource.global.Intialize.ClsInitialize;
import cucumber.api.java.en.*;

public class Test_StepDef {

Device device = ClsInitialize.iobjDevice;
App app = ClsInitialize.iobjApp;

@Given("^User is able to get the current temperature$")
public void user_is_able_to_get_the_current_temperature() throws Throwable
{
	
}
@When("^User enters the temperature as Celsius$")
public void user_enters_the_temperature_as_Celsius() throws Throwable {
	app.SetCurrentApp("Login");
	new TextBox(device , "Celsius").SendKeys("50");
}
@When("^User clicks on Convert button$")
public void user_clicks_on_Convert_button() throws Throwable {
	new Button(device , "Convert").Click();
}
@Then("^User should see the temperature changed to Fahrenheit$")
public void user_should_see_the_temperature_changed_to_Fahrenheit() throws Throwable {
	app.Wait(5); 
	String a = device.driver.findElement(By.name("y")).getText(); 
			 //new TextBox(device , "Fahrenheit").GetValue();
	 System.out.println("temp is "+a);
}
@Then("^User captures the converted value$")
public void user_captures_the_converted_value() throws Throwable {
}
}