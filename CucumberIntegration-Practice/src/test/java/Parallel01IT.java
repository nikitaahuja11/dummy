import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import com.cucumber.listener.Reporter;
import FrameworkSource.global.Intialize.ClsInitialize;
import java.util.HashMap;
import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"C:/Users/user/Documents/stepinforum/CucumberIntegration-Practice/src/test/resources/FeatureFiles/TwitterData.feature"},
        plugin = {"json:C:/Users/user/Documents/stepinforum/CucumberIntegration-Practice/target/cucumber-parallel/json/1.json", "com.cucumber.listener.ExtentCucumberFormatter:C:/Users/user/Documents/stepinforum/CucumberIntegration-Practice/target/1.html"},
        monochrome = true,
        tags = {"@Twitter"},
        glue = {"StepDefinations"})
public class Parallel01IT {

    @BeforeClass
    public static void beforeClass(){
      ClsInitialize.mapDBMetricesData = new HashMap<String,String>();
    }

    @AfterClass
    public static void afterClass(){
      Reporter.loadXMLConfig(new File("src/test/resources/configuration/extent-config.xml"));
    }
}