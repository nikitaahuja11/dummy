package Tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.itextpdf.text.log.SysoCounter;

import FrameworkSource.global.Intialize.CommonImplementation;

import static java.util.stream.Collectors.*;
import static java.util.Map.Entry.*;

public class FindFirstElement {

public static void main(String[] args) throws InterruptedException {
// TODO Auto-generated method stub

System.setProperty("webdriver.chrome.driver",CommonImplementation.strChromeDriverPath);
WebDriver driver = new ChromeDriver();
//comment the above 2 lines and uncomment below 2 lines to use Chrome
//System.setProperty("webdriver.chrome.driver","G:\\chromedriver.exe");
//WebDriver driver = new ChromeDriver();
   	
       String baseUrl = "https://twitter.com/stepin_forum";
       //String expectedTitle = "Welcome: Mercury Tours";
       //String actualTitle = "";

       // launch Fire fox and direct it to the Base URL
       driver.get(baseUrl);
       driver.manage().window().maximize();
       
       JavascriptExecutor js = (JavascriptExecutor) driver;
     //  js.executeScript("window.scrollBy(0,1000)");
       js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
       Thread.sleep(2000);
       //js.executeScript("window.scrollBy(0,1000)");
       js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
       Thread.sleep(2000);
       //js.executeScript("window.scrollBy(0,1000)");
       js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
       Thread.sleep(2000);
      // List<WebElement> a =  driver.findElements(By.xpath("//div[@class='stream']/ol/li"));
       List<WebElement> a =  driver.findElements(By.xpath("//div[@class='stream']/ol/li/div[1]/div[2]/div[2]/p[1]"));
       System.out.println(a.size());
       
       String splitString[];
       String tweet;
       
       int count=0;
       
       LinkedHashMap<String,Integer> retweet=new LinkedHashMap<String,Integer>(); 
String removeExtra;
       
       for(int i=0;i<49;i++)
       {
       	//a.get(i).ge
        //System.out.println(a.get(i).getText());
       	
       	tweet = a.get(i).getText().toLowerCase();
       	
       	
       	
       	splitString = tweet.split(" ");
       	
       	for(int j=0;j<splitString.length;j++)
       	{
        if(splitString[j].contains("#"))
       	{
       	removeExtra = splitString[j].replaceAll("[^a-zA-Z0-9]", "");
       	
       	
       	
        if(retweet.containsKey(removeExtra))
       	{
       	count = retweet.get(removeExtra);
        retweet.put(removeExtra,count+1);
       	}
       	
       	else	
       	{
        retweet.put(removeExtra,1);
       	}
       	}
       	}
       	
       	
       	}
       
       System.out.println(retweet);
       
       Map<String, Integer> sorted = retweet
               .entrySet()
               .stream()
               .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
               .collect(
                   toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                       LinkedHashMap::new));
    
       System.out.println("map after sorting by values: " + sorted);
       
     //  List<String> retweetTop10=new ArrayList<>();
       
       List<String> retweetTop10 = sorted.entrySet().stream()
    		   .map(Map.Entry::getKey)
    		   .sorted()
    		   .limit(10)
    		   .collect(Collectors.toList());
       for(int p=0;p<9;p++)
       {
    	   System.out.println(retweetTop10.get(p));
       }
      
       driver.quit();
       	
       }
       



}