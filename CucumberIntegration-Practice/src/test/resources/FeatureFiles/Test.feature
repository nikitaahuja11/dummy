Feature: Test feature file

@Android @Mobile @English @TC01
Scenario: User is able to convert current temperature from Celsius to Fahrenheit using Web Services
Given User is able to get the current temperature
When User enters the temperature as Celsius 
And User clicks on Convert button
Then User should see the temperature changed to Fahrenheit
And User captures the converted value