Feature: User is able to get data from StepIn page of twitter 
@Web @English @Twitter
Scenario:  Extract top 50 tweets and find the tweet with highest retweets, highest likes, 10 highest used hashtags, list of people under ‘who to follow’ and create summary as a JSON

Given Twitter webpage is available with stepin page open
When User extracts top 50 tweets
And User finds tweet with highest retweets
And User finds tweet with highest likes
And User finds tweet with 10 highest used hashtags
And User finds the list of people under ‘who to follow’
Then User creates a Summary as a JSON 
And Upload JSON summary to the server