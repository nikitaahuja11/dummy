package FrameworkSource.UI.Mobile;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.Intialize.ClsIntializeMobile;
import FrameworkSource.global.reporter.ReportEvents;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.LocksDevice;
import io.appium.java_client.PressesKeyCode;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.AndroidKeyMetastate;
import io.appium.java_client.android.HasDeviceDetails;
import io.appium.java_client.ios.IOSDriver;

/**
 * The class consists of all the customized functions related to browsers.
 */
public class Device extends ClsInitialize {
	
	public  AppiumDriver<WebElement> driver=null;
	public  String[][] PageValues = null;	
	public  String[] FoundValue = null;
	public  WebElement element;
	public List<WebElement> listOfElements = null;
	String udid = null;
	public String OS = null;
	
	/**
	* This function launches an application to Android or IOS device
	* @param AppType is a string parameter which takes either values of android or IOS.
	 * @param url 
	 * @param browser 
	* @throws InterruptedException
	 * @throws IOException 
	*/
	
	  
    /**
     *  This function Minimizes the keyboard opened on the device
     *  This works on both IOS and Anndroid Device
     * @throws IOException
     */
    public void HideKeyboard() throws IOException
    {
    	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
    	if(OS.equalsIgnoreCase("ios"))
    	{
    	 IOSDriver< WebElement> AD= (IOSDriver) driver;
 	    AD.getKeyboard().sendKeys(Keys.RETURN);
 	   ReportEvents.Done(callerClassName+":Device", "Hiding Keyboard "+ OS);
    	}
    	else if (OS.equalsIgnoreCase("android"))
    	{
			driver.hideKeyboard();
			 ReportEvents.Done(callerClassName+":Device", "Hiding Keyboard "+ OS);
		}
    }
    
/**
 *  This Function gives the device locked status of android device
 * @return
 * @throws IOException
 */
	public boolean IsLockedAnd () throws IOException 
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		ReportEvents.Done(callerClassName+":Device", "Returing  lock status");
		return ((LocksDevice) driver).isDeviceLocked();
	}	
	
 /**
 * This Function closes the driver session opened in the Device(Both Android and IOS device)	
 * @throws IOException
 */
	public void Quit() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		if (driver != null)
		{
		 driver.quit();
		 ReportEvents.Done(callerClassName+":Device", "Driver is closed ");
		}
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Device", e);
			Assert.fail();
		}
	}
	
	/**
	 * Quit Browser Driver session opened for Mobil Center URL
	 * @throws IOException
	 */
	public void Quit_Browser() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		ClsIntializeMobile.automationTempWebDriver.quit();
		ReportEvents.Done(callerClassName+":Device", "Browser Driver is closed ");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Device", e);
			Assert.fail();
		}
	}	
	
	
	/**
	 * This function returns device UDID
	 * @return
	 * @throws IOException
	 */
	public String GetDeviceUdid() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		ReportEvents.Done(callerClassName+":Device", "Returning device UDID ");
		return driver.getCapabilities().getCapability("deviceName").toString();
		 
	}
	
	
	/**
	 *  Printing the capabilities in android as well as iOS
	 * @throws IOException
	 */
	public void PrintCapabilities() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		 System.out.println(((RemoteWebDriver)driver).getCapabilities());
		 System.out.println(driver.getSessionDetails());
		 ReportEvents.Done(callerClassName+":Device", "Printing Capabilities ");
	}	
	
	/**
	 * TBD
	 * @param orientation
	 * @throws IOException
	 */
	
	public void SetOrientation(String orientation) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		if(orientation.equalsIgnoreCase("landscape"))
		{
			driver.rotate(ScreenOrientation.LANDSCAPE);
			 ReportEvents.Done(callerClassName+":Device", "Orientation Changed to: "+orientation);
		}
		
		else if(orientation.equalsIgnoreCase("potrait"))
		{
			driver.rotate(ScreenOrientation.PORTRAIT);
			ReportEvents.Done(callerClassName+":Device", "Orientation Changed to: "+orientation);
		}
	}
	/**
	 * The function inputs various keys from keyboard for Android Device
	 * @param command takes string value which needs to be input from keyboard like Down, Left, Up, Right, Backspace, Alt, Ctrl, Delete, Enter and Spacebar
	 * @throws IOException 
	 */
	public void PressKey(String key) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		switch(key.toLowerCase())
		{
		case "space":
			//((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.SPACE));
			
		 ((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.SPACE, AndroidKeyMetastate.META_SHIFT_ON);
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;
		
		case "enter":
			((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.ENTER);
			//((PressesKey) driver).pressKeyC(new KeyEvent(AndroidKey.ENTER));
		//((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.ENTER, AndroidKeyMetastate.META_SHIFT_ON);
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;
		
		case "delete":
			((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.DEL);
			//((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.DEL));
		//((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.DEL, AndroidKeyMetastate.META_SHIFT_ON);
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;
		
		case "tab":
			((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			//((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.TAB));
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;
		
		case "backspace":
			((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.BACKSPACE);
			//((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.BACK));
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;
		
		case "down":
			((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
			//((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.PAGE_DOWN));
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;

		case "up":
			((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_UP);
			//((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.PAGE_UP));
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;
		
		case "home":
			((PressesKeyCode) driver).pressKeyCode(AndroidKeyCode.HOME);
			/*((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.HOME));*/
		ReportEvents.Done(callerClassName+":Device", "Key Pressed: "+key);
		break;
		
		default:
		System.out.println("Invalid key name.");	
		System.exit(0);
		}
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Device", e);
			Assert.fail();
		}
	}
	
	/**
	 * This function returns status based on keyboard displayed in android device
	 * @return
	 * @throws IOException
	 */
	public boolean IsKeyboardShownAnd() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		ReportEvents.Done(callerClassName+":Device", "Giving the status that keyboard is displayed or not");
		return ((HasDeviceDetails) driver).isKeyboardShown();
		
	}
	
	/**
	 * This Function returns true if the given text exits in the Page source
	 * @param word
	 * @return
	 * @throws IOException
	 */

	public boolean GetSource(String word) throws IOException 
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean value = false;
		try
		{
		ReportEvents.Done(callerClassName+":Device", "Returning the page source");
		value = driver.getPageSource().contains(word);
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Device", e);
			//Assert.fail();
		}
	
		return value;
		
	}
	
	/**
	 * The function switches to desired open windows of browser.
	 * @param windownumber takes integer value. Eg.1 for Main window, 2 for second window
	 * @throws IOException 
	 */
	public void GetBrowser(int windownumber) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		System.out.println(tabs.size());
		ReportEvents.Done(callerClassName+":Browser",  "Total No. of windows opened" + tabs.size());
		driver.switchTo().window((String) tabs.get(windownumber-1));
		ReportEvents.Done(callerClassName+":Browser",  "Windows switched successfully");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			Assert.fail();
		}

	}	
	
	/**
	 * The function will kill any task.
	 * @throws IOException 
	 */	
	public void KillTask(String taskName) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		if(!taskName.contains(".exe"))
		{
			taskName = taskName + ".exe"; 
		}

		try{
		Runtime.getRuntime().exec("Taskkill /IM "+taskName+" /F");
		ReportEvents.Done(callerClassName+":Browser",   taskName+" killed successfully.");
		//Logger.INFO("Browser", "Browser Page Refresh successfully.");
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Browser", e);
			//Logger.ERROR("Browser",e);
		    //Assert.fail();
		}
	}
	
}