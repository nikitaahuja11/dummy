package FrameworkSource.UI.Common;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.Alert;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.*;
/**
 * Dialog class consists of all the functions related to web/javascript alerts, confirmation and prompts.
 */
public class Dialog{
	

	/**
	 * The function switches from the current web page to active open pop up and accepts it.
	 * @throws IOException 
	 */
	public void Click() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try {
				if(ClsInitialize.objWeb.automationWebDriver !=null)
					{
						try
							{
								Alert alert= ClsInitialize.objWeb.automationWebDriver.switchTo().alert();
								alert.accept();
								ReportEvents.Done(callerClassName+":Dialog",  "Pop up is accepted");
							}
						catch(Exception e)
							{
								ReportEvents.Error(callerClassName+":Dialog", e);
								Assert.fail();
							}
					}
				else if(ClsInitialize.objMobile.automationMobileDriver !=null)
				{
					try
						{
							Alert alert= ClsInitialize.objMobile.automationMobileDriver.switchTo().alert();
							alert.accept();
							ReportEvents.Done(callerClassName+":Dialog",  "Pop up is accepted");
						}
					catch(Exception e)
						{
							ReportEvents.Error(callerClassName+":Dialog", e);
							Assert.fail();
						}
				}
			}
		catch(Exception objDialog)
		{
			ReportEvents.Error(callerClassName+":DialogBox", objDialog);
		     Assert.fail();
		}
	}	
	/**
	 * The function switches from the current web page to active open pop up and closes it.
	 * @throws IOException 
	 */
	public void Close() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try 
		{
			if(ClsInitialize.objWeb.automationWebDriver !=null)
				{
					try
						{
							Alert alert= ClsInitialize.objWeb.automationWebDriver.switchTo().alert();
							alert.dismiss();
							ReportEvents.Done(callerClassName+":Dialog",  "Pop up is Closed");
						}
					catch(Exception e)
						{
							ReportEvents.Error(callerClassName+":Dialog", e);
							Assert.fail();
						}
				}
			else if(ClsInitialize.objMobile.automationMobileDriver !=null)
			{
				try
					{
						Alert alert= ClsInitialize.objMobile.automationMobileDriver.switchTo().alert();
						alert.dismiss();
						ReportEvents.Done(callerClassName+":Dialog",  "Pop up is Closed");
					}
				catch(Exception e)
					{
						ReportEvents.Error(callerClassName+":Dialog", e);
						Assert.fail();
					}
			}
		}	
		catch(Exception objDialog)
		{
			ReportEvents.Error(callerClassName+":DialogBox", objDialog);
		     Assert.fail();
		}
	}
	/**
	 * The function checks the existence of the alerts or pop up.
	 * @return boolean value. If alets are found then true value is returned else false.
	 * @throws IOException 
	 */
	public boolean Exists() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean flag = false;
		try {
			if(ClsInitialize.objWeb.automationWebDriver !=null)
				{
					try
						{
							flag = false;
							Alert alert= ClsInitialize.objWeb.automationWebDriver.switchTo().alert();
							if(alert != null)
							{
								flag = true;
							}
						}
					catch(Exception e)
						{
						flag = false;
						ReportEvents.Done(callerClassName+":Exist Function", "Error Checking the existence of the Alert.");
						}
				}
			else if(ClsInitialize.objMobile.automationMobileDriver !=null)
			{
				try
					{
						flag = false;
						Alert alert= ClsInitialize.objMobile.automationMobileDriver.switchTo().alert();
						if(alert != null)
						{
							flag = true;
						}
						
					}
				catch(Exception e)
					{
						flag = false;
						ReportEvents.Done(callerClassName+":Exist Function", "Error Checking the existence of the Alert.");
					}
			}
		}
			catch(Exception objDialog)
			{
				flag = false;
				ReportEvents.Done(callerClassName+":Exist Function", "Error Checking the existence of the Alert.");
			}
		
			return flag;
	}
	/**
	 * The function switches from the current web page to active open pop up and returns the test written in it.
	 * @return String value.
	 * @throws IOException 
	 */
	public String GetVisibleText() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		String value = null;
		try {
			if(ClsInitialize.objWeb.automationWebDriver !=null)
				{
					try
						{
							Alert alert= ClsInitialize.objWeb.automationWebDriver.switchTo().alert();
							value= alert.getText();
							ReportEvents.Done(callerClassName+":Dialog",   "Pop up text :" + value + " is returned successfully.");
						}
					catch(Exception e)
						{
							ReportEvents.Done(callerClassName+":GetVisibleText Function", "Error in getting the text from the alert.");
						}
				}
			else if(ClsInitialize.objMobile.automationMobileDriver !=null)
			{
				try
					{
						Alert alert= ClsInitialize.objMobile.automationMobileDriver.switchTo().alert();
						value= alert.getText();
						ReportEvents.Done(callerClassName+":Dialog",   "Pop up text :" + value + " is returned successfully.");
					}
				catch(Exception e)
					{
						ReportEvents.Done(callerClassName+":GetVisibleText Function", "Error in getting the text from the alert.");
					}
			}
		}
			catch(Exception e)
			{
				ReportEvents.Done(callerClassName+":GetVisibleText Function", "Error in getting the text from the alert.");
			}
			
		return value;	
	}
	
	/**
	 * The function switches from the current web page to active open pop up and pass the text to it.
	 * @return String value.
	 * @throws IOException 
	 */
	public void SendKeys(String value) throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try {
			if(ClsInitialize.objWeb.automationWebDriver !=null)
				{
					try
						{
							Alert alert= ClsInitialize.objWeb.automationWebDriver.switchTo().alert();
							alert.sendKeys(value);
							ReportEvents.Done(callerClassName+":Dialog",  "Pop up text :" + value + " is passed successfully.");
						}
					catch(Exception objDialog)
					{
						ReportEvents.Error(callerClassName+":DialogBox", objDialog);
					     Assert.fail();
					}
			}
			else if(ClsInitialize.objMobile.automationMobileDriver !=null)
			{
				try
					{
						Alert alert= ClsInitialize.objMobile.automationMobileDriver.switchTo().alert();
						alert.sendKeys(value);
						ReportEvents.Done(callerClassName+":Dialog",  "Pop up text :" + value + " is passed successfully.");
					}
				catch(Exception objDialog)
				{
					ReportEvents.Error(callerClassName+":DialogBox", objDialog);
				     Assert.fail();
				}
		}
		}
		catch(Exception objDialog)
		{
			ReportEvents.Error(callerClassName+":DialogBox", objDialog);
		     Assert.fail();
		}
	}
}
