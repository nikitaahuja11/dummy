package FrameworkSource.UI.Common;

import java.io.IOException;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;

/**
 * The class consists of all the functions relates to Image of web page.
 */
public class Image extends UIElement{
	/**
	 * Image class constructor, directly invoked when an object of Image class is created. Class Image extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	/*public Image(String Elementvalue) throws IOException, InterruptedException
	{
		//<img> tag
		super(Elementvalue);
	}*/
	
	public Image(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public Image( Device d, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	public Image( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	

}
