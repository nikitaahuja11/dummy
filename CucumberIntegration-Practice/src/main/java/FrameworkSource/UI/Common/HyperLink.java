package FrameworkSource.UI.Common;

import java.io.IOException;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;

/**
 * The class consists of all the functions relates to Hyperlinks of web page.
 */
public class HyperLink extends UIElement {
	/**
	 * Hyperlink class constructor, directly invoked when an object of Hyperlink class is created. Class Hyperlink extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	/*public HyperLink(String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}*/
	
	public HyperLink(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public HyperLink( Device d, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	public HyperLink( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}


}

