package FrameworkSource.UI.Common;

import java.io.IOException;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.global.reporter.ReportEvents;

/**
 * The class consists of all the functions relates to buttons of web page.
 */
public class Button extends UIElement{
	/**
	 * Button class constructor, directly invoked when an object of Button class is created. Class Button extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	/*public Button( String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}*/
	
	public Button(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public Button( Device d, String Elementvalue) throws IOException, InterruptedException 
	{
		super(Elementvalue);
	}
	
	public Button( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	
	
}