package FrameworkSource.UI.Common;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.*;
public class TextBox extends UIElement{
	/**
	 * TextBox class constructor, directly invoked when an object of TextBox class is created. Class TextBox extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	/*public TextBox (String Elementvalue) throws IOException, InterruptedException
	{
		
		super(Elementvalue);
	}*/
	
	public TextBox( Device d, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	

	public TextBox(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public TextBox( Browser b, String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
	}
	/**
	 * The function sends the string value to the text box.
	 * @param value takes string value.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	
	public void SendKeys(String value) throws IOException, InterruptedException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try {
			if((value != null) && (ClsInitialize.objWeb.automationWebDriver !=null))
			{	
				ClsInitialize.objWeb.func_getBrowser().element.clear();
				ClsInitialize.objWeb.func_getBrowser().element.sendKeys(value);
				ReportEvents.Done(callerClassName+":TextBox", "Values are sucessfully entered in textbox."+value);
			}
			else if((value != null) && (ClsInitialize.objMobile.automationMobileDriver !=null))
			{
				ClsInitialize.objMobile.deviceTest.element.clear();
				ClsInitialize.objMobile.deviceTest.element.sendKeys(value);
				//Done(callerClassName+":TextBox", "Values are sucessfully entered in textbox."+value,clsInitalize.deviceTest);
				ReportEvents.Done(callerClassName+":TextBox", "Values are sucessfully entered in textbox."+value);
			}
			else
			{
				ReportEvents.Fatal(callerClassName+":TextBox", "Value entered is not correct i.e. ."+value);
			}
		}
		catch(Exception e){
			if(ClsInitialize.objWeb.automationWebDriver !=null)
				ReportEvents.Error(callerClassName+":TextBoxWeb",e);
			else
				ReportEvents.Error(callerClassName+":TextBoxMobile",e);
			Assert.fail();
		}
		//Logger.INFO("TextBox", "Values are sucessfully entered in textbox.");
	}
	/**
	 * The function clears the value present in the text box.
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void Clear() throws IOException, InterruptedException
	{  		
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try {
				if((ClsInitialize.objWeb.automationWebDriver !=null))
				{	
					ClsInitialize.objWeb.func_getBrowser().element.clear();
					ReportEvents.Done(callerClassName+ ":TextBox", "Values are sucessfully cleared from textbox.");
				}
				else if(ClsInitialize.objMobile.automationMobileDriver != null)
				{
					ClsInitialize.objMobile.deviceTest.element.clear();
				}
			}catch(Exception e){
				if(ClsInitialize.objWeb.automationWebDriver !=null)
					ReportEvents.Error(callerClassName+":TextBox",e);
				else
					ReportEvents.Done(callerClassName+":TextBox", "Values are sucessfully cleared from textbox");
				Assert.fail();
			}
	}
}
