package FrameworkSource.UI.Common;


import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.lang.annotation.Annotation;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.POM.AndroidLocator;
import FrameworkSource.global.POM.IOSLocator;
import FrameworkSource.global.POM.WebLocator_Desktop;
import FrameworkSource.global.POM.WebLocator_Tab;
import FrameworkSource.global.Read_OR_Data.ORReader;
import FrameworkSource.global.reporter.ReportEvents;
import io.appium.java_client.TouchAction;
import FrameworkSource.UI.*;
import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;

public class UIElement extends ClsInitialize {

	static String[] FoundValue;
	Browser objBrowser;
	Device objDevice;

	public UIElement(Object... strFunctionName) throws IOException, InterruptedException {

		String className = new Exception().getStackTrace()[2].toString();
		String strFinalClassName = fnGetAnnotationPackage(className);
		String strTempParam;

		switch (ClsInitialize.strPlatform.toUpperCase()) {
		case "WEB":
			objBrowser = ClsInitialize.iobjBrowser;

		case "ENGLISH":
			fnGetProp_WebDesktop((String) strFunctionName[0], strFinalClassName);
			objBrowser.listOfElements = fnWebElementsLocation();
			try {
				if (objBrowser.listOfElements.size() > 0)
					objBrowser.element = objBrowser.listOfElements.get(0);
			} catch (Exception e) {
				objBrowser.element = null;
			}

			break;
		case "MOBILE":
			objDevice = ClsInitialize.iobjDevice;
			strTempParam = ClsInitialize.strLanguage + "_" + ClsInitialize.strDevice;
			switch (ClsInitialize.strDevice.toUpperCase()) {
                    case "ANDROID":
                        try{
							fnGetProp_Android(strFunctionName[0].toString(), strFinalClassName);
							objDevice.listOfElements = fnMobileElementsLocation();
							if (objDevice.listOfElements.size() > 0) {
								objDevice.element = objDevice.listOfElements.get(0);
								break;
							}else {
								objDevice.element = null;
							}
						}catch(Exception e)
						{
		                    objDevice.element = null;

						}

                    case "IOS":
                        
						try{
							fnGetProp_IOS(strFunctionName[0].toString(), strFinalClassName);
							objDevice.listOfElements = fnMobileElementsLocation();
							if (objDevice.listOfElements.size() > 0) {
								objDevice.element = objDevice.listOfElements.get(0);
								break;
							} else {
								objDevice.element = null;
							}
							
						}catch(Exception e)
						{
		                    objDevice.element = null;

						}

                }

			break;
		default:
			break;
		}

		// b.FoundValue
	}

	private void fnGetProp_WebDesktop(String strFunctionName, String strFinalClassName) {
		try {
			for (Method field : Class.forName(strFinalClassName).getDeclaredMethods()) {

				if (field.getName().equalsIgnoreCase(strFunctionName)) {
					Annotation annotation = field.getAnnotation(WebLocator_Desktop.class);
					if (annotation instanceof WebLocator_Desktop) {
						WebLocator_Desktop customAnnotation = (WebLocator_Desktop) annotation;
						this.FoundValue = new String[2];
						this.FoundValue[0] = customAnnotation.PropertyType();
						this.FoundValue[1] = customAnnotation.value();
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void fnGetProp_WebTab(String strFunctionName, String strFinalClassName) {
		try {
			for (Method field : Class.forName(strFinalClassName).getDeclaredMethods()) {

				if (field.getName().equalsIgnoreCase(strFunctionName)) {
					Annotation annotation = field.getAnnotation(WebLocator_Tab.class);
					if (annotation instanceof WebLocator_Tab) {
						WebLocator_Tab customAnnotation = (WebLocator_Tab) annotation;
						this.FoundValue = new String[2];
						this.FoundValue[0] = customAnnotation.PropertyType();
						this.FoundValue[1] = customAnnotation.value();
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void fnGetProp_Android(String strFunctionName, String strFinalClassName) {
		try {
			for (Method field : Class.forName(strFinalClassName).getDeclaredMethods()) {

				if (field.getName().equalsIgnoreCase(strFunctionName)) {
					Annotation annotation = field.getAnnotation(AndroidLocator.class);
					if (annotation instanceof AndroidLocator) {
						AndroidLocator customAnnotation = (AndroidLocator) annotation;
						this.FoundValue = new String[2];
						this.FoundValue[0] = customAnnotation.PropertyType();
						this.FoundValue[1] = customAnnotation.value();
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void fnGetProp_IOS(String strFunctionName, String strFinalClassName) {
		try {
			for (Method field : Class.forName(strFinalClassName).getDeclaredMethods()) {

				if (field.getName().equalsIgnoreCase(strFunctionName)) {
					Annotation annotation = field.getAnnotation(IOSLocator.class);
					if (annotation instanceof IOSLocator) {
						IOSLocator customAnnotation = (IOSLocator) annotation;
						this.FoundValue = new String[2];
						this.FoundValue[0] = customAnnotation.PropertyType();
						this.FoundValue[1] = customAnnotation.value();
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String fnGetAnnotationPackage(String className) {
		className = className.split("\\(")[0];
		String strFinalClassName = "";
		String[] arrPackage = className.split("\\.");
		for (int iCount = 0; iCount < arrPackage.length - 1; iCount++) {
			if (iCount != arrPackage.length - 2)
				strFinalClassName = strFinalClassName + arrPackage[iCount] + ".";
			else
				strFinalClassName = strFinalClassName + arrPackage[iCount];
		}
		return strFinalClassName;
	}

	public UIElement(String Elementvalue) throws IOException, InterruptedException {
		String ClassName = new Exception().getStackTrace()[2].getMethodName();
		boolean ElementProperty;
		if (ClsInitialize.objWeb.automationWebDriver != null) {
			objBrowser = ClsInitialize.objWeb.func_getBrowser();
			try {

				ElementProperty = new ORReader().FindProperty(Elementvalue);
				if (ElementProperty == true) {
					FoundValue = new ORReader().FindExactvalue(Elementvalue);
					if (FoundValue[1].length() < 10)
						ReportEvents.Done(ClassName + ":ElementValue", "Property Values returned from Properties.xlsx: "
								+ FoundValue[0] + " & " + FoundValue[1]);
					else
						ReportEvents.Done(ClassName + ":ElementValue", "Property Values returned from Properties.xlsx: "
								+ FoundValue[0] + ":: Property Value Too Long");

					objBrowser.listOfElements = fnWebElementsLocation();
					if (objBrowser.listOfElements.size() > 0)
						objBrowser.element = objBrowser.listOfElements.get(0);

				} else {
					// ReportEvents.Fatal(ClassName+":ElementValue", "Element
					// '"+ Elementvalue +"' Not Exists in the Properties Excel
					// sheet");
					objBrowser.element = null; // Setting element to null as if
												// user calls function like
												// exists. Then this function
												// will perform operation on
												// previous b.elment
				}
			} catch (Exception e) {
				objBrowser.element = null; // Setting element to null as if user
											// calls function like exists. Then
											// this function will perform
											// operation on previous b.elment
				// ReportEvents.Fatal(ClassName+":Error Operating on the
				// Element",e.getMessage());
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
			try {
				this.objDevice = ClsInitialize.objMobile.fnInitializeDevice();
				ElementProperty = new ORReader().FindProperty(Elementvalue);
				if (ElementProperty == true) {

					FoundValue = new ORReader().FindExactvalue(Elementvalue);
					ReportEvents.Done(ClassName + ":ElementValue",
							"Property Values returned from Properties.xlsx: " + FoundValue[0] + " & " + FoundValue[1]);
					objDevice.listOfElements = fnMobileElementsLocation();
					if (objDevice.listOfElements.size() > 0)
						objDevice.element = objDevice.listOfElements.get(0);

				} else {
					// ReportEvents.Fatal(ClassName+":ElementValue", "Element
					// '"+ Elementvalue +"' Not Exists in the Properties Excel
					// sheet");
					objDevice.element = null; // Setting element to null as if
												// user calls function like
												// exists. Then this function
												// will perform operation on
												// previous b.elment
				}
			} catch (Exception e) {
				// ReportEvents.Fatal(ClassName+":Error Operating on the
				// Element",e.getMessage());
				objDevice.element = null; // Setting element to null as if user
											// calls function like exists. Then
											// this function will perform
											// operation on previous b.elment
			}
		}

	}

	public UIElement() {
	}

	public List<WebElement> fnWebElementsLocation() throws IOException {
		String strProperty = FoundValue[0];
		String strPropertyValue = FoundValue[1];
		List<WebElement> element = null;
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		int iWaitTime = Integer.parseInt(ClsInitialize.properties.getProperty("WaitTime"));
		try {
			switch (strProperty.toUpperCase()) {
			case "ID":
				element = objBrowser.driver.findElements(By.id(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "XPATH":
				element = objBrowser.driver.findElements(By.xpath(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "CLASS":
				element = objBrowser.driver.findElements(By.className(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "LINKTEXT":
				element = objBrowser.driver.findElements(By.linkText(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "PARTIALLINKTEXT":
				element = objBrowser.driver.findElements(By.partialLinkText(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "CSSSELECTOR":
				element = objBrowser.driver.findElements(By.cssSelector(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "NAME":
				element = objBrowser.driver.findElements(By.name(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "TAGNAME":
				element = objBrowser.driver.findElements(By.tagName(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			default:

				ReportEvents.Done(callerClassName + ":UIElement: Finding Element",
						"UIStrategy: " + strProperty + " is not valid.");
				break;
			}
		} catch (Exception eIdentification) {
			element = null;
		}

		return element;

	}

	public List<WebElement> fnMobileElementsLocation() throws InterruptedException, IOException {
		String strProperty = FoundValue[0];
		String strPropertyValue = FoundValue[1];
		List<WebElement> element = null;
		boolean blnFlag = true;
		int iWaitTime = Integer.parseInt(ClsInitialize.properties.getProperty("WaitTime"));
		int iWaitTimeCounter = 0;
		try {
			switch (strProperty.toUpperCase()) {
			case "ID":
				element = objDevice.driver.findElements(By.id(strPropertyValue));
				new WebDriverWait(objDevice.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "XPATH":
				element = objDevice.driver.findElements(By.xpath(strPropertyValue));
				new WebDriverWait(objDevice.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "CLASS":
				element = objDevice.driver.findElements(By.className(strPropertyValue));
				new WebDriverWait(objDevice.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "LINKTEXT":
				element = objDevice.driver.findElements(By.linkText(strPropertyValue));
				new WebDriverWait(objDevice.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "PARTIALLINKTEXT":
				element = objDevice.driver.findElements(By.partialLinkText(strPropertyValue));
				new WebDriverWait(objDevice.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "CSSSELECTOR":
				element = objDevice.driver.findElements(By.cssSelector(strPropertyValue));
				new WebDriverWait(objDevice.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "NAME":
				element = objDevice.driver.findElements(By.name(strPropertyValue));
				new WebDriverWait(objDevice.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			case "ACCESSIBILITYID":

				while (blnFlag) {
					element = objDevice.driver.findElementsByAccessibilityId(strPropertyValue);
					if (!element.isEmpty() && element.get(0).isDisplayed()) {
						blnFlag = false;
						System.out.println(
								" ELEMENT WITH ACCESSIBILITYID " + element.get(0).getText() + " FOUND ON THE SCREEN ");
					} else {
						ClsInitialize.iobjApp.Wait(1);
						iWaitTimeCounter++;
					}

					if (iWaitTimeCounter == iWaitTime)
						break;
				}
				break;

			/*
			 * element =
			 * objDevice.driver.findElementsByAccessibilityId(strPropertyValue);
			 * new WebDriverWait(objDevice.driver,
			 * 5).until(ExpectedConditions.visibilityOfAllElements(element));
			 */

			case "TAGNAME":
				element = objDevice.driver.findElements(By.tagName(strPropertyValue));
				new WebDriverWait(objBrowser.driver, iWaitTime)
						.until(ExpectedConditions.visibilityOfAllElements(element));
				break;

			default:
				break;
			}
		} catch (Exception eIdentification) {
			element = null;
		}

		return element;

	}

	/**
	 * The function clicks on the web element.
	 * 
	 * @throws IOException
	 */
	public void Click() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				objBrowser.element.click();
				ReportEvents.Done(callerClassName + ":WebUIElement",
						"Element: " + objBrowser.element.toString().split("->")[1] + "is clicked successfully.");
			} catch (Exception e) {
				ReportEvents.Error(callerClassName + ":WebUIElement", e);
				// System.out.println(e.getMessage());
				Assert.fail();
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			try {

				objDevice.element.click();
				ReportEvents.Done(callerClassName + ":MobileUIElement",
						"Element: " + objDevice.element.toString().split("->")[1] + "is clicked successfully.");
			} catch (Exception e) {
				ReportEvents.Error(callerClassName + ":MobileUIElement", e);
				Assert.fail();
			}
		}
	}

	public boolean VerifyMessage(String exp_message) throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean flag = false;
		String actualmessage;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				actualmessage = objBrowser.element.getText();
				if (exp_message.equals(actualmessage)) {
					flag = true;
					ReportEvents.Done(callerClassName + ":WebUIElement", "Message " + exp_message + "is displayed.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Message " + exp_message + "is not displayed.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error(callerClassName + ": Verify Message Failed", e);
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {

			try {
				actualmessage = objDevice.element.getText();
				if (exp_message.equals(actualmessage)) {
					flag = true;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element text is compared with " + exp_message);
				} else {
					flag = false;
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error(callerClassName + ":Verify Message Failed", e);
			}
		}

		return flag;
	}

	public boolean Exists() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean flag = false;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				if (objBrowser.element != null) {
					flag = true;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element.toString().split("->")[1] + " exists.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":WebUIElement Exist Check",
							"Element: " + objBrowser.element.toString().split("->")[1] + " doesn't exists.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Done(callerClassName + ":Exist Function", "Error Checking the existence of the object.");
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {

			try {
				if (objDevice.element != null) {
					flag = true;
					ReportEvents.Done(callerClassName + ":MobileUIElement Exist Check",
							"Element: " + objDevice.element.toString().split("->")[1] + "is clicked successfully.");
				} else {
					flag = false;
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Done(callerClassName + ":Exist Function", "Error Checking the existence of the object.");
			}
		}

		return flag;
	}

	public boolean Highlight() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean blnResult = false;
		try {
			if (objBrowser.driver instanceof JavascriptExecutor) {
				((JavascriptExecutor) objBrowser.driver).executeScript("arguments[0].style.border='3px solid red'",
						objBrowser.element);
				ReportEvents.Done(callerClassName + ":WebUIElement",
						"Element :" + objBrowser.element + " highlighted.");
				blnResult = true;
			}
		} catch (Exception e) {
			blnResult = false;
			ReportEvents.Done(callerClassName + ":Highlight Function", "Error Highlighting the object.");
		}

		return blnResult;
	}

	public boolean Highlight(int seconds) throws InterruptedException, IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean blnResult = false;
		int total = Integer.parseInt(seconds + "000");

		try {
			if (objBrowser.driver instanceof JavascriptExecutor) {
				((JavascriptExecutor) objBrowser.driver).executeScript("arguments[0].style.border='3px solid red'",
						objBrowser.element);
				Thread.sleep(total);
				((JavascriptExecutor) objBrowser.driver)
						.executeScript("arguments[0].setAttribute('style',arguments[0])", objBrowser.element);
				ReportEvents.Done(callerClassName + ":WebUIElement",
						"Element :" + objBrowser.element + " successfully highlighted for " + seconds + " seconds.");
				blnResult = true;
			}
		} catch (Exception e) {
			ReportEvents.Done(callerClassName + ":Highlight Function", "Error Highlighting the object.");
			blnResult = false;
		}

		return blnResult;
	}

	public boolean IsSelected() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean flag = false;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				if (objBrowser.element.isSelected()) {
					flag = true;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is selected.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is not selected.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Fatal("WebUIElement", "Element doesn't exists so cannot perform IsSelected operation");
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			try {
				if (objDevice.element.isSelected()) {
					flag = true;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " is selected.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " is not selected.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error(
						"Exception::MobileUIElement::Element doesn't exists so cannot perform IsSelected operation", e);
			}
		}

		return flag;

	}

	public boolean IsEnabled() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean flag = false;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				if (objBrowser.element.isEnabled()) {
					flag = true;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is enabled.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is not enabled.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error(
						"Exception::WebUIElement::Element doesn't exists so cannot perform IsEnabled operation", e);
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			try {
				if (objDevice.element.isEnabled()) {
					flag = true;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " is enabled.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " is not enabled.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error(
						"Exception::MobileUIElement::Element doesn't exists so cannot perform IsEnabled operation", e);
			}
		}

		return flag;
	}

	public boolean IsDisplayed() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean flag = false;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				if (objBrowser.element.isDisplayed()) {
					flag = true;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is displayed.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is not displayed.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error(
						"Exception::WebUIElement::Element doesn't exists so cannot perform IsDisplayed operation", e);
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			try {
				if (objDevice.element.isDisplayed()) {
					flag = true;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " exists.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " doesn't exists.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error("Exception::MobileUIElement::Element doesn't exists.", e);
			}
		}

		return flag;
	}

	public boolean IsEmpty() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean flag = false;
		String text;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				text = objBrowser.element.getAttribute("value");
				if ((text.isEmpty())) {
					flag = true;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is Empty.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":WebUIElement",
							"Element: " + objBrowser.element + " is not Empty.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error("WebUIElement::Element doesn't exists so cannot perform IsEmpty check", e);
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			try {
				text = objDevice.element.getAttribute("value");
				if ((text.isEmpty())) {
					flag = true;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " is Empty.");
				} else {
					flag = false;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Element: " + objDevice.element + " is not Empty.");
				}
			} catch (Exception e) {
				flag = false;
				ReportEvents.Error("MobileUIElement::Element doesn't exists so cannot perform IsEmpty check", e);
			}
		}

		return flag;
	}

	public boolean MouseHover() throws IOException {
		boolean blnResult = false;
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		try {
			Actions Object = new Actions(objBrowser.driver);
			Object.moveToElement(objBrowser.element).build().perform();
			ReportEvents.Done(callerClassName + ":WebUIElement",
					"Mouse Hover to " + objBrowser.element + " done successfully.");
			blnResult = true;
		} catch (Exception e) {
			blnResult = false;
			ReportEvents.Error(callerClassName + ":WebUIElement", e);
		}

		return blnResult;
	}

	public String GetValue() throws IOException {

		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		String text = null;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				text = objBrowser.element.getText();
				ReportEvents.Done(callerClassName + ":WebUIElement", "Text Value: " + objBrowser.element.getText()
						+ " found successfully." + objBrowser.element.getText());
			} catch (Exception e) {
				ReportEvents.Error(callerClassName + ":WebUIElement", e);
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			try {
				ReportEvents.Done(callerClassName + ":MobileUIElement", "Text of the element is displayed.");
			} catch (Exception e) {
				ReportEvents.Error(callerClassName + ":MobileUIElement", e);
			}
			text = objDevice.element.getText();
		}

		return text;

	}

	public String AttributeValue(String AttributeName) throws IOException {
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		String attribute = null;

		if (ClsInitialize.objWeb.automationWebDriver != null) {
			try {
				attribute = objBrowser.element.getAttribute(AttributeName);
				ReportEvents.Done(callerClassName + ":WebUIElement",
						"Attribute Value Found successfully." + objBrowser.element.getAttribute(AttributeName));
			} catch (Exception e) {
				ReportEvents.Error(callerClassName + ":WebUIElement", e);
			}
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			try {
				/*
				 * StackTraceElement[] stElements =
				 * Thread.currentThread().getStackTrace();
				 * ClassName=stElements[2].getClassName();
				 */
				ReportEvents.Done(callerClassName + ":MobileUIElement",
						"Value of Attribute " + AttributeName + " is displayed");
			} catch (Exception e) {
				ReportEvents.Error(callerClassName + ":MobileUIElement", e);
			}
			attribute = objDevice.element.getAttribute(AttributeName);
		}

		return attribute;
	}

	public Dimension Size() {
		return objBrowser.element.getSize();
	}

	public Point Location() throws IOException {
		Point objPoint = null;
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		if (ClsInitialize.objWeb.automationWebDriver != null) {
			ReportEvents.Done(callerClassName + ":WebUIElement", "Location of the element is displayed");
			objPoint = objBrowser.element.getLocation();
		} else if (ClsInitialize.objMobile.automationMobileDriver != null) {
			ReportEvents.Done(callerClassName + ":MobileUIElement", "Location of the element is displayed");
			objPoint = objDevice.element.getLocation();
		}

		return objPoint;
	}

	public boolean Submit() {
		boolean blnResult = false;
		try {
			objDevice.element.submit();
			blnResult = true;
		} catch (Exception e) {
			blnResult = false;
			;
		}

		return blnResult;
	}

	public boolean Long_Press(int duration) throws IOException {
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		boolean blnResult = false;
		try {
			TouchAction action = new TouchAction(objDevice.driver);
			action.longPress(objDevice.element, Duration.ofSeconds(duration)).release().perform();
			ReportEvents.Done(callerClassName + ":MobileUIElement",
					"Long pressed on the element : " + objDevice.element);
			blnResult = true;
		} catch (Exception e) {
			blnResult = false;
			ReportEvents.Error(callerClassName + ":MobileUIElement", e);
		}

		return blnResult;
	}

	public boolean ScrollToElementUsingTouchActions() throws IOException {
		boolean blnResult = false;
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		int i = 1;
		try {
			while (i > 0) {
				if (!objDevice.element.isDisplayed()) {
					ClsInitialize.objMobile.appTest.ScrollDownUsingTouchactions(1);
					i++;
				} else {
					i = 0;
					ReportEvents.Done(callerClassName + ":MobileUIElement",
							"Scrolled till to the element : " + objDevice.element);
				}
			}
			blnResult = true;

		} catch (Exception e) {
			blnResult = false;
			ReportEvents.Error(callerClassName + ":MobileUIElement", e);
		}

		return blnResult;
	}

	public String GetTagName() throws IOException {
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		String ClassName = null;
		try {
			StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
			ClassName = stElements[2].getClassName();
			ReportEvents.Done(callerClassName + ":MobileUIElement", "Returning the tagname of the element.");
		} catch (Exception e) {
			ReportEvents.Error(callerClassName + ":MobileUIElement", e);
		}
		return objDevice.element.getTagName();
	}

	public boolean MouseClick(String type) throws IOException {
		boolean blnResult = false;
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		try {

			if (type.equalsIgnoreCase("Right")) {
				Actions action = new Actions(objBrowser.driver).contextClick(objBrowser.element);
				action.build().perform();
				ReportEvents.Done(callerClassName + ":WebUIElement",
						"Right Click is Pressed Successfully on element :" + objBrowser.element);
			} else if (type.equalsIgnoreCase("Double")) {
				Actions actions = new Actions(objBrowser.driver).contextClick(objBrowser.element);
				actions.moveToElement(objBrowser.element).doubleClick().build().perform();
				ReportEvents.Done(callerClassName + ":WebUIElement",
						"Double Click is Pressed Successfully on element :" + objBrowser.element);
			} else if (type.equalsIgnoreCase("Left")) {
				Actions actions = new Actions(objBrowser.driver).click(objBrowser.element);
				actions.moveToElement(objBrowser.element).click().build().perform();
				ReportEvents.Done(callerClassName + ":WebUIElement",
						"Left Click is Pressed Successfully on element :" + objBrowser.element);

			} else {
				ReportEvents.Fatal(callerClassName + ":WebUIElement", "Mouse");
				System.out.println("Invalid");
			}
			blnResult = true;
		} catch (Exception e) {
			ReportEvents.Error(callerClassName + ":WebUIElement", e);
			blnResult = false;

		}

		return blnResult;
	}

	/**
	 * The functions moved the mouse cursor to defined xoffset and yoffset.
	 * 
	 * @param xOffset
	 *            takes integer value.
	 * @param yOffset
	 *            takes integer value.
	 * @throws IOException
	 */
	public boolean MouseMoveTo(int xOffset, int yOffset) throws IOException {
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		boolean blnResult = false;
		try {
			Actions builder = new Actions(objBrowser.driver);
			builder.moveToElement(objBrowser.element, xOffset, yOffset).click().build().perform();
			ReportEvents.Done(callerClassName + ":WebUIElement",
					"Mouse successfully moved to location: " + xOffset + yOffset);
			blnResult = true;
		} catch (Exception e) {
			ReportEvents.Error(callerClassName + ":WebUIElement", e);
			blnResult = false;
		}

		return blnResult;
	}

	/**
	 * The function inputs Ctrl along with other key from keyboard.
	 * 
	 * @param command
	 *            takes string value Ctrl.
	 * @param keyvalue
	 *            takes string value which needs to be pressed along with Ctrl
	 *            key.
	 * @throws IOException
	 */
	public boolean PressKeysWithCtrl(String command, String keyvalue) throws IOException {
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		String command1 = command.substring(0, 1).toUpperCase() + command.substring(1).toLowerCase();
		String keyvalue1 = keyvalue.toLowerCase();
		boolean blnResult = false;
		try {
			if (command1.equalsIgnoreCase("Ctrl")) {
				Actions act = new Actions(objBrowser.driver);
				act.keyDown(Keys.CONTROL).sendKeys(keyvalue1).keyUp(Keys.CONTROL).perform();
				ReportEvents.Done(callerClassName + ":WebUIElement", "Control Key is pressed successfully.");
				blnResult = true;
			}
		} catch (Exception e) {
			ReportEvents.Error(callerClassName + ":WebUIElement", e);
			blnResult = false;
		}

		return blnResult;
	}

	/**
	 * The function inputs various keys from keyboard specific to a web element.
	 * 
	 * @param command
	 *            takes string value which needs to be input from keyboard like
	 *            Down, Left, Up, Right, Backspace, Alt, Ctrl, Delete, Enter and
	 *            Spacebar
	 * @param times
	 *            takes a integer value of number of times the command is to be
	 *            executed
	 * @throws IOException
	 */
	public boolean PressKeyOnElement(String command, int times) throws IOException {
		boolean blnResult = false;
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		String command1 = command.substring(0, 1).toUpperCase() + command.substring(1).toLowerCase();
		try {

			switch (command1) {
			case "Down":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.ARROW_DOWN);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Arrow Down Key is pressed successfully.");
				break;
			case "Left":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.ARROW_LEFT);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Arrow Left Key is pressed successfully.");
				break;
			case "Up":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.ARROW_UP);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Arrow Up Key is pressed successfully.");
				break;
			case "Right":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.ARROW_RIGHT);
					ReportEvents.Done(callerClassName + ":WebUIElement", "Arrow Right Key is pressed successfully.");
				}
				break;
			case "Backspace":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.BACK_SPACE);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Back space Key is pressed successfully.");
				break;
			case "Delete":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.DELETE);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Delete Key is pressed successfully.");
				// Logger.INFO(ClassName, "Delete Key is pressed
				// successfully.");
				break;
			case "Enter":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.ENTER);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Enter Key is pressed successfully.");
				// Logger.INFO(ClassName, "Enter Key is pressed successfully.");
				break;
			case "Tab":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.TAB);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Enter Key is pressed successfully.");
				// Logger.INFO(ClassName, "Enter Key is pressed successfully.");
				break;
			case "Spacebar":
				for (int i = 1; i <= times; i++) {
					objBrowser.element.sendKeys(Keys.SPACE);
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Space Key is pressed successfully.");
				blnResult = true;
				break;
			}
		} catch (Exception e) {
			ReportEvents.Error(callerClassName + ":WebUIElement", e);
			blnResult = false;
		}
		return blnResult;
	}

	/**
	 * The function inputs various keys from keyboard on Element using Action
	 * Class.
	 * 
	 * @param command
	 *            takes string value which needs to be input from keyboard like
	 *            Down, Left, Up, Right, Backspace, Alt, Ctrl, Delete, Enter and
	 *            Spacebar
	 * @param times
	 *            takes a integer value of number of times the command is to be
	 *            executed
	 * @throws IOException
	 */
	public boolean PressKeyonElement_Action(String command, int times) throws IOException {
		boolean blnResult = false;
		String callerClassName = new Exception().getStackTrace()[2].getMethodName();
		String command1 = command.substring(0, 1).toUpperCase() + command.substring(1).toLowerCase();
		Actions actions = new Actions(objBrowser.driver);
		try {
			switch (command1) {
			case "Enter":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.ENTER).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Enter Key is pressed successfully.");
				break;

			case "Down":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.DOWN).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "DownArrow Key is pressed successfully.");
				break;

			case "Up":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.UP).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "UpArrow Key is pressed successfully.");
				break;

			case "Left":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.LEFT).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "UpArrow Key is pressed successfully.");
				break;

			case "Right":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.RIGHT).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "UpArrow Key is pressed successfully.");
				break;

			case "Backspace":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.BACK_SPACE).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Backspace Key is pressed successfully.");
				break;

			case "Spacebar":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.SPACE).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Spacebar Key is pressed successfully.");
				break;

			case "Tab":
				for (int i = 1; i <= times; i++) {
					actions.sendKeys(objBrowser.element, Keys.TAB).build().perform();
				}
				ReportEvents.Done(callerClassName + ":WebUIElement", "Spacebar Key is pressed successfully.");
				break;

			default:
				ReportEvents.Fatal(callerClassName + ":WebUIElement", "Invalid key name.");
			}
			blnResult = true;

		} catch (Exception e) {
			ReportEvents.Error(callerClassName + ":WebUIElement", e);
			blnResult = false;
		}

		return blnResult;
	}

	/**
	 * The functions Presskey are added to provided backward compatibility to
	 * old scripts. Internally, they call the new function
	 */
	public boolean PressKey(String command, String keyvalue) throws IOException {
		boolean blnResult = false;
		try {
			PressKeysWithCtrl(command, keyvalue);
			blnResult = true;
		} catch (Exception ePressKey) {
			blnResult = false;
		}

		return blnResult;
	}

	public boolean PressKey(String command, int times) throws IOException {
		boolean blnResult = false;
		try {
			PressKeyOnElement(command, times);
			blnResult = true;
		} catch (Exception ePressKey) {
			blnResult = false;
		}

		return blnResult;

	}

	/**
	 * The function scrolls till the particular web element.
	 * 
	 * @throws IOException
	 */
	public boolean ScrollTillElement() throws IOException {
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		boolean blnResult = false;
		try {

			((JavascriptExecutor) objBrowser.driver).executeScript("arguments[0].scrollIntoView();",
					objBrowser.element);
			ReportEvents.Done(callerClassName + ":WebUIElement", "Element: " + objBrowser.element + "is reached.");
			blnResult = true;
		} catch (Exception e) {
			ReportEvents.Error(callerClassName + ":WebUIElement", e);
			blnResult = false;
		}

		return blnResult;
	}

}
