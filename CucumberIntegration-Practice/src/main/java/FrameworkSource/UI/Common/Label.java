package FrameworkSource.UI.Common;

import java.io.IOException;

import FrameworkSource.UI.Mobile.Device;
import FrameworkSource.UI.Web.Browser;

/**
 * The class consists of all the functions relates to Label of web page.
 */
public class Label extends UIElement {
	/**
	 * Label class constructor, directly invoked when an object of Label class is created. Class Label extends WebUIElement class.
	 * @param b is a static global variable in Browser class which maintains properties of open browser session.
	 * @param Elementvalue takes a string value.
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	
		/*public Label(String Elementvalue) throws IOException, InterruptedException
		{
			super(Elementvalue);
		}*/
		
		public Label(String strProp) throws IOException, InterruptedException
		{
			super((Object)strProp);
		}
		
		public Label( Device d, String Elementvalue) throws IOException, InterruptedException
		{
			super(Elementvalue);
		}
		
		public Label( Browser b, String Elementvalue) throws IOException, InterruptedException
		{
			super(Elementvalue);
		}
}


