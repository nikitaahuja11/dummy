package FrameworkSource.UI.Common;

import java.io.IOException;
import org.junit.Assert;
import org.openqa.selenium.By;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.*;
public class UIFrame extends UIElement
{
	
	/*public UIFrame(String Elementvalue) throws IOException, InterruptedException
	{
		super(Elementvalue);
		
	}*/
	
	public UIFrame(String strProp) throws IOException, InterruptedException
	{
		super((Object)strProp);
	}
	
	public UIFrame() throws IOException, InterruptedException 
	{
		
	}
	
	/**
	 * Switch to the Frame element found in the page
	 * new Frame(browser,"logicalname").SwitchFrame();
	 * @throws IOException 
	 * @throws InterruptedException 
	 * 
	 */
	public void SwitchFrame() throws IOException, InterruptedException 
	{
	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
			ClsInitialize.objWeb.func_getBrowser().driver.switchTo().frame(ClsInitialize.objWeb.func_getBrowser().element);
			ReportEvents.Done(callerClassName+ ":Frame", "Sucessfully switched to the Frame element: "+ClsInitialize.objWeb.func_getBrowser().element);
		}
		catch(Exception e)
		{
			ReportEvents.Fatal(callerClassName+":Frame","Cannot switch to Frame element: "+ClsInitialize.objWeb.func_getBrowser().element);
		     Assert.fail();
		}
	}
	
	/**
	 * Switch to the Frame directly by name or id. 
	 * For Example: new Frame(browser).SwitchFrame("IdOrName");
	 * @param IdOrName takes String values for id and name.
	 * @throws IOException 
	 * 
	 */
	public void SwitchFrame(String IdOrName) throws IOException 
	{
	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
			ClsInitialize.objWeb.func_getBrowser().driver.switchTo().frame(IdOrName);
		ReportEvents.Done(callerClassName+ ":Frame", "Sucessfully switched to the Frame: "+IdOrName);
		}
		catch(Exception e)
		{
			ReportEvents.Fatal(callerClassName+":Frame","Cannot switch to Frame: "+IdOrName);
		     Assert.fail();	
		}
	}
	
	/**
	 * Switch to the Frame directly index 
	 * @param index takes integer value for the frame. Eg.1 for first frame, 2 for second frame
	 * For Example: new Frame(browser).SwitchFrame(1);
	 * @throws IOException 
	 * 
	 */
	public void SwitchFrame(int index) throws IOException 
	{
	String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
			ClsInitialize.objWeb.func_getBrowser().driver.switchTo().frame(index-1);
		ReportEvents.Done(callerClassName+ ":Frame", "Sucessfully switched to the Frame: "+index);
		}
		catch(Exception e)
		{
			ReportEvents.Fatal(callerClassName+":Frame","Cannot switch to Frame: "+index);
		     Assert.fail();
		}
	}
	
	/**
	 * Find total number of frames in a page.
	 * int size = new Frame(browser).NoOfFrames();
	 * @throws IOException 
	 * 
	 */
	public int NoOfFrames() throws IOException 
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		int total = 0;
		try
		{
		total = ClsInitialize.objWeb.func_getBrowser().driver.findElements(By.tagName("iframe")).size();
		ReportEvents.Done(callerClassName+ ":Frame", "Total no. of frames are: "+total);
		}
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Frame",e);	
		    Assert.fail();
		}
		return total;
	}
	
	/**
	 * Switching to the Original page from Frame
	 * When Working on Frames need to come back to the Default content/page
	 * new Frame(browser).SwitchOutOfFrame();
	 * @throws IOException
	 */	
	public void SwitchOutOfFrame() throws IOException
	{
		String callerClassName = new Exception().getStackTrace()[1].getMethodName();
		try
		{
			ClsInitialize.objWeb.func_getBrowser().driver.switchTo().defaultContent();
		   ReportEvents.Done(callerClassName+":Page","Page Switched from Frame to Default content/page");
		}
		
		catch(Exception e)
		{
			ReportEvents.Error(callerClassName+":Page", e);
		     Assert.fail();
		}
		
		
	}
	
	
}
	
	
	

