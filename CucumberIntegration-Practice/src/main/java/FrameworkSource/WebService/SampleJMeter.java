package FrameworkSource.WebService;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

public class SampleJMeter {
	public List<String> JMeterTrigger() throws Exception {
  
		Runtime rt = Runtime.getRuntime();
		//Process p = rt.exec("cmd /c start cmd.exe /K \" cd C:\\TOOLS\\apache-jmeter-5.1.1\\bin && jmeter -n -t SampleTest.jmx ");
		Process p = rt.exec("cmd /c cd C:\\TOOLS\\apache-jmeter-5.1.1\\bin && jmeter -n -t SampleTest.jmx ");
		p.waitFor();
		CSVToExcelConvertor ob = new CSVToExcelConvertor();
		Thread.sleep(5000);
		List<String> OutPut= ob.CopyOutputJmeterATF("C:\\Users\\Sunlife\\Desktop\\New XLS Worksheet.xls");
		
		return OutPut;
    
}
}
