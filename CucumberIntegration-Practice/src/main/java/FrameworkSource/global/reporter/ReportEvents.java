package FrameworkSource.global.reporter;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import FrameworkSource.global.Intialize.ClsInitialize;
import com.cucumber.listener.Reporter;
import FrameworkSource.global.reporter.ReportEvents;

/**
 * ReportsEvents class consists of functions dealing with configuration related to screenshots and customized data in reports
 */
public class ReportEvents extends ClsInitialize{
	
	public static String strCaptureSnapshotPath = "";
	public static String FlagScreenShot= "";
	public static String FlagStepScreenShot= "";
	public static List<String> Trace =new ArrayList<String>();
	public static String imgLocation = "";
	private static String code;
	static String strStepScreenShotRequired = "false";
	static String strScreenShotRequired = "false"; 

	/**
	 * CaptureScreenShots signifies if ScreenShots needs to be captured during the run.
	 * @param flag Mandatory: Parameter takes either "True" or "False" which signifies if Screenshot needs to be captured or not respectively.
	 * @param flag1 Optional: Parameter takes either "True" or "False" which signifies if implicit Screenshots will be captured at each step in test scripts.
	 */
	public static void CaptureScreenShots(String flag,Object... flag1)
	{
		FlagScreenShot = flag;
		if(flag1.length>0)
		{
			if(flag.equalsIgnoreCase("True"))
			{
				FlagStepScreenShot=(String)flag1[0];
			}
		}
	}

	/**ScreenShotRequired is only to provide backward compatibility to the old framework
	This function initializes the flag to invoke captureScreenshot*/
	public static void ScreenShotRequired(String flag)
	{
		if(flag.equalsIgnoreCase("true"))
		{
			strScreenShotRequired = flag;
		}
		else if(flag.equalsIgnoreCase("false"))
		{
			strScreenShotRequired = flag;
		}
		
		CaptureScreenShots(strScreenShotRequired,strStepScreenShotRequired);
	}
	
	/**Step ScreenShotRequired is only to provide backward compatibility to the old framework
	This function initializes the flag to invoke captureScreenshot*/
	public static void StepScreenShotRequired(String flag)
	{
		if(flag.equalsIgnoreCase("true"))
		{
			strScreenShotRequired = "true";

			strStepScreenShotRequired = flag;
		}
		else if(flag.equalsIgnoreCase("false"))
		{
			strStepScreenShotRequired = flag;
		}
		
		CaptureScreenShots(strScreenShotRequired,strStepScreenShotRequired);
	}

	
	public static void DeleteFolder(File folder) {
		   if (folder.isDirectory()) {
		          File[] list = folder.listFiles();
		          if (list != null) {
		              for (int i = 0; i < list.length; i++) {
		                  File tmpF = list[i];
		                  if (tmpF.isDirectory()) {
		                	  DeleteFolder(tmpF);
		                  }
		                  tmpF.delete();
		              }
		          }
		   }
		
	}
	public static void Done(String StepName, String StepDescription) throws IOException
	{
		try{
			if(ClsInitialize.properties.get("ConsoleOutput").toString().equalsIgnoreCase("yes"))
				System.out.println("StepName:-" + StepName + "::::"+StepDescription);
			Logger.INFO(StepName, StepDescription);
			//test.log("", StepDescription);
			
			
			if(FlagStepScreenShot.equalsIgnoreCase("True"))
			{
				TakeScreenshot(StepName);
				try{
					code = "&nbsp;&nbsp;&nbsp;<a href='" + imgLocation + "' data-featherlight='image'><span class='label grey white-text'>S</span></a>";	
				}catch(Exception ReporterEx){}
			}
			else
			{
				code = "";
			}
			
			Reporter.addStepLog( StepDescription + code);
		}catch(Exception eMisc){}
		
		
	}
	public static void Error(String StepName, Exception e) throws IOException
	{
		try{
			if(ClsInitialize.properties.get("ConsoleOutput").toString().equalsIgnoreCase("yes"))
				System.out.println("StepName:-" + StepName + "::::"+e.getMessage());
			Logger.ERROR(StepName, e);
			
			if(FlagStepScreenShot.equalsIgnoreCase("True"))
			{
				TakeScreenshot(StepName);
				try
				{
					code = "&nbsp;&nbsp;&nbsp;<a href='" + imgLocation + "' data-featherlight='image'><span class='label grey white-text'>S</span></a>";
					
				}catch(Exception ReporterEx){}
			}
			else
			{
				code = "";
			}
			
			Reporter.addStepLog(e.getMessage() + code);
		}catch(Exception objMisc){}
		
	}
	public static void Fatal(String StepName, String StepDescription) throws IOException
	{
		try{
			if(ClsInitialize.properties.get("ConsoleOutput").toString().equalsIgnoreCase("yes"))
				System.out.println("StepName:-" + StepName + "::::"+StepDescription);
			Logger.FATAL(StepName, StepDescription);
			
			if(FlagStepScreenShot.equalsIgnoreCase("True"))
			{
				TakeScreenshot(StepName);
				try{
					code = "&nbsp;&nbsp;&nbsp;<a href='" + imgLocation + "' data-featherlight='image'><span class='label grey white-text'>S</span></a>";
					
				}catch(Exception ReporterEx){}
			}
			else
			{
				code = "";
			}
	
			Reporter.addStepLog(StepDescription + code);
		}catch(Exception objMisc){}
	}

	/**
	* The method writes data to logs based on status(Pass/Fail)
	* @param it takes different string values like status, stepname and stepdescription
	* @throws IOException 
	*/
	public static void Reporter(String status, String stepname, String stepdescription) throws IOException
	{
		stepname=stepname.replaceAll(" ", "_");
		
		if(status.equalsIgnoreCase("Pass"))
		{
			WritePassToLogger(stepname,stepdescription);
		}
		else if(status.equalsIgnoreCase("Fail"))
		{
			WriteFailToLogger(stepname,stepdescription);
		}
		if(FlagScreenShot.equalsIgnoreCase("True") )
		{
			TakeScreenshot(stepname);
		}
		
			
	}

	/**
	* The method takes screenshot and saves it to testcase folder with screenshot name as stepname  
	* @param Device expects the Device class object and stepname accepts the String value
	* @throws IOException 
	*/
	private static void TakeScreenshot(String stepname) throws IOException
	{
		DateFormat dateformat = new SimpleDateFormat("MM-dd-yyyy");
		Date date = new Date();
		String strStepDef = "";
		String strDef = "";
		String ImageLocation;
		/*StackTraceElement[] arrCallerFunctions = new Exception().getStackTrace();
		for(int iCountStackTrace = 0; iCountStackTrace<= arrCallerFunctions.length -1;iCountStackTrace++)
		{
			if(arrCallerFunctions[iCountStackTrace].toString().contains("UserDefinedFunctions"))
			{
				
				strDef = arrCallerFunctions[iCountStackTrace].getClassName();
			}
			
			if(arrCallerFunctions[iCountStackTrace].toString().contains("StepDefinations"))
			{
				strStepDef = arrCallerFunctions[iCountStackTrace].getClassName();
			}
			
			if ((strStepDef.length() > 0) && (strDef.length() > 0))
				break;
			else if(iCountStackTrace == arrCallerFunctions.length -1)
			{
				strStepDef = "Snapshot";
				strDef = "Snapshot";
			}
		}*/
		String LineNum = String.valueOf(new Exception().getStackTrace()[3].getLineNumber());
		File src = null;
		
		String datestring = date.toString();
		datestring = datestring.replaceAll(":", "");
		String class3 = datestring.split(" ")[3];
		//File folder = new File(System.getProperty("user.dir")+"//ExecutionReports");
		File folder = new File("" + ClsInitialize.fngetProperties().getProperty("LOB_Type") + "\\" + ClsInitialize.fngetProperties().getProperty("Project"));
		boolean Exsits = folder.exists();
		if(!Exsits)
		{
			folder.mkdir();
		}
		try{
			if (ClsInitialize.strPlatform.equalsIgnoreCase("web"))
				src = ((TakesScreenshot)ClsInitialize.iobjBrowser.driver).getScreenshotAs(OutputType.FILE);
			else
				src= ((TakesScreenshot)ClsInitialize.iobjDevice.driver).getScreenshotAs(OutputType.FILE);
			
		
			//ImageLocation = strCaptureSnapshotPath + ClsInitialize.fngetProperties().getProperty("LOB_Type") + "\\" + ClsInitialize.fngetProperties().getProperty("Project")+ "\\" + dateformat.format(date)+"\\" + ClsInitialize.strTestCaseName.replaceAll(" ", "_")+"_"+ "Iteration_"+ClsInitialize.mapIteration.get(ClsInitialize.strTestCaseName)+"_"+LineNum+".jpeg";
			ImageLocation = "C:\\RAHUL\\Screenshots"+"\\"+LineNum+".jpeg";
			ImageLocation =ImageLocation.replaceAll(" ", "%20");
			ImageLocation =ImageLocation.replaceAll("<","_");
			ImageLocation =ImageLocation.replaceAll(">","_");
			FileUtils.copyFile(src, new File(ImageLocation));
			Logger.INFO("ScreenShots", "Screen Shot taken and saved under path: "+ImageLocation);
			try{
				if(FlagStepScreenShot.equalsIgnoreCase("false"))
					Reporter.addScreenCaptureFromPath(ImageLocation, "ScreenShot");
				imgLocation = ImageLocation;
			}catch(Exception ReporterEx){imgLocation = null;}
		}catch(Exception e)
		{
			FlagStepScreenShot = "false";
			FlagStepScreenShot= "false";
			ReportEvents.Error("Capturing Snapshot::", e);
		}
		
		
	}

	/**
	 * The function writes FATAL logs in logger files.
	 * @param stepname takes the name which needs to be logged in Log4J.
	 * @param stepdescription takes the description of the logs which needs to be logged in Log4J.
	 */
	private static void WriteFailToLogger(String stepname, String stepdescription) 
	{
		if(ClsInitialize.properties.get("ConsoleOutput").toString().equalsIgnoreCase("yes"))
		System.out.println("StepName:-" + stepname + "::::"+stepdescription);
		Logger.FATAL(stepname, stepdescription);	
		try{Reporter.addStepLog(stepdescription);}catch(Exception ReporterEx){}
		Assert.fail();
	}
	/**
	 * The function writes INFO logs in logger files.
	 * @param stepname takes the name which needs to be logged in Log4J.
	 * @param stepdescription takes the description of the logs which needs to be logged in Log4J.
	 */
	private static void WritePassToLogger(String stepname, String stepdescription) {
		if(ClsInitialize.properties.get("ConsoleOutput").toString().equalsIgnoreCase("yes"))
		Logger.INFO(stepname, stepdescription);	
		try{Reporter.addStepLog(stepdescription);}catch(Exception ReporterEx){}
		
		System.out.println("StepName:-" + stepname + "::::"+stepdescription);
		
	}
	
}
