package FrameworkSource.global.reporter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import FrameworkSource.global.Intialize.ClsInitialize;

public class ReportGenerator {

public static void Generate(String status) throws IOException
	{
	String CallerMethod=new Exception().getStackTrace()[1].getMethodName();
	
	String projectPath = System.getProperty("user.dir");
		 String DeletePath = projectPath+ "//ExecutionReports";
		FilesDelete(DeletePath);
		File input = new File(projectPath+"\\log" + File.separator+"application"+".html");
		Document htmlFile = null;
		try {
		     htmlFile = Jsoup.parse(input, "ISO-8859-1");
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			String date[] = htmlFile.body().getElementsByTag("body").text().split("time");		
			String date1[] = date[1].split("Time");
			Element table = htmlFile.select("table").get(0);
			Elements rows = table.select("tr");
			Element row = rows.get(rows.size()-1);
		    Elements cols = row.select("td");
		    String stringDate = cols.get(0).text();
		    int intDate = Integer.parseInt(stringDate)/1000;
		    String totalTime = intDate+" secs";
		   
		    List<String> tests = new ArrayList<String>();
			String s = htmlFile.body().getElementsByTag("body").text();
			
			String[] arr = s.split(" ");  
			for ( String ss : arr) {
				
			       if(ss.contains("Tests."))
			       {
			    	   	long count=ss.chars().filter(ch->ch=='.').count();
			    	   	String ClassName = ss.split("\\.")[(int) count];
			    	   	String value=ClassName.split("\\$")[0];
			    		  

			    	   if(tests.contains(value)==false) //If that string is not added in List previously
			    	   {
			    		   tests.add(value); 
			    	   }
			       }
			  }
			tests = tests.parallelStream()
				      .filter(Objects::nonNull)
				      .collect(Collectors.toList());
			//System.out.println(tests.size());
			String[] TestArray = TestCaseOrder(htmlFile,tests);
		
		
		      
		      int PassFail[]=PassFailCounter(htmlFile,tests);
		      String IndPass[]=IndividualPassFail(htmlFile,tests);
		   
		      int startCount = 0;
		      int lastCount = 0;
		      Element table1 = htmlFile.select("table").get(0); //select the first table.
		      Elements rows1 = table1.select("tr");
		      List<Integer> indTime = new ArrayList<Integer>(); //This list stores time for individual test
		      for(int i=1;i<=tests.size();i++)
		      {
		    	  for (int j = 1; j < rows1.size(); j++)
		    	  { //first row is the col names so skip it.
		  		    Element row1 = rows1.get(j);
		  		    Elements cols1 = row1.select("td");
		  		    
		  		    if(startCount==0) {
		  		    	if(cols1.get(1).text().equals("pool-"+i+"-thread-1") ||cols1.get(1).text().equals("main")||cols1.get(1).text().contains("pool-1-thread-"+i))
		  		    	{
		  		    		startCount = Integer.parseInt(cols1.get(0).text());
		  		    	}
		  		    }
		  		    
		  		    else
		  		    {
		  		    	if(cols1.get(1).text().equals("pool-"+i+"-thread-1")||cols1.get(1).text().equals("main")||cols1.get(1).text().contains("pool-1-thread-"+i))
		  		    	{
		  		    		lastCount = Integer.parseInt(cols1.get(0).text());
		  		    	}
		  		    }

		  		    }
		    	  int total = (lastCount - startCount)/1000;
		    	 
		    	  indTime.add(total);
		    	  startCount = 0;
		    	  }
		      int[] IndTime = new int[indTime.size()];
		      for(int i =0;i<indTime.size();i++)
		      {
		    	  IndTime[i]=indTime.get(i);
		      }
		      String type =ExecutionType(htmlFile);
		      createReportPage1(tests.size(),PassFail, intDate, TestArray,IndTime,IndPass,type);
		      //Test Step Details
		      
		      List<String> message = new ArrayList<String>(); //This list stores time for individual test
		      List<String> StepName = new ArrayList<String>();
		      List<Integer> stepTime = new ArrayList<Integer>(); //This list stores time for individual test
		      Element table2 = htmlFile.select("table").get(0); //select the first table.
		      Elements rows2 = table2.select("tr");
		     // String StepNameValue;
		      List<String> StepNameValue = new ArrayList<String>();
		     // String StepTestValue;
		      List<String> StepTestValue = new ArrayList<String>();
		      List<String> StepPassValue = new ArrayList<String>();
		      
		      int count = 0;
		      for(int i=1;i<=tests.size();i++)
		      {
		    	
		    	
		    	  String a;
		    	  Integer b;
		    	  String c;
		    	  String f;
		    	  String d=null;
		    	  String e=null;
		    
		    	  
		    	  int counter = 1;
		    	  
		    	  for (int j = 1; j < rows2.size(); j++)
		    	  	{ //first row is the col names so skip it.
		    		  Element row2 = rows2.get(j);
		  		    	Elements cols2 = row2.select("td");
		  		    	 
		  		    	if(cols2.get(1).text().contains("pool-"+i+"-thread-1")||cols2.get(1).text().contains("main")||cols2.get(1).text().contains("pool-1-thread-"+i))
		  		    		{	
		  		    		message.add(cols2.get(5).text());
		  		    		a= message.get(count);
		  		    		int time =  Integer.parseInt(cols2.get(0).text());
		  		    		stepTime.add(time);
		  		    		b= stepTime.get(count);
		  		    		StepPassValue.add(cols2.get(2).text());
		  		    		f=StepPassValue.get(count);
		  		    		if(f.equals("INFO")||(f.equals("DEBUG")))
		  		    			{
		  		    				f="Pass";
		  		    			}
		  		    		else
		  		    			{
		  		    				f="Fail";
		  		    			}
		  		    		StepName.add(cols2.get(3).text());
		  		    		c=StepName.get(count);
		  		    		if(c.contains(":"))
		  		    			{
		  		    				String[] stepn = c.split(" ");  
		  		    				for ( String ss : stepn) 
		  		    					{
		  					
		  		    						if((ss.contains("Tests."))&&(ss.contains(":")))
		  		    							{
		  		    								StepNameValue.add("PreRequisites");
		  		    								d=StepNameValue.get(count);
		  				    		
		  		    								StepTestValue.add("DataReader");
		  		    								e=StepTestValue.get(count);
		  				    		
		  		    							}
		  		    						else if(ss.contains(":")&&!((ss.contains("Tests."))))
		  		    							{
		  				    	
		  		    								StepNameValue.add(ss.substring(0, ss.indexOf(":")));
		  		    								d=StepNameValue.get(count);
		  		    								//System.out.println(count+"gagan");
		  		    								//	System.out.println(d);
		  		    								StepTestValue.add(ss.substring(ss.indexOf(":")+1, ss.length()));
		  		    								e=StepTestValue.get(count);
		  				    
		  		    								//System.out.println(ss.length()-ss.indexOf(":"));
		  		    							}
		  		    						if(count>0)
			  		    					{
			  		    						if(StepNameValue.get(count).equals(StepNameValue.get(count-1)))
			  		    						{
			  		    						StepNameValue.remove(count);
			  		    							StepNameValue.add(StepNameValue.get(count-1));
			  		    							d=" ";
			  		    			
			  		    							StepTestValue.remove(count);
			  		    							StepTestValue.add(ss.substring(ss.lastIndexOf(":")+1, ss.length()));
		  		    								e=StepTestValue.get(count);
			  						
			  						
			  		    							// System.out.println(d);
			  		    						}
			  				
			  		    					}
		  		    					}
		  		    				
		  		    			}
		  		     	else
		  		     		{	
		  		     			StepNameValue.add(StepName.get(count));
		  		     			d=StepNameValue.get(count);
		  		     			StepTestValue.add("No SubStep");
		  		     			e=" ";
		  		     			
		  		     		
		  		     		}
		  		    		ReportPageUpdate(TestArray[i-1],d,e,f,a);
		  			//System.out.println("Test Case:"+TestArray[i-1]+"StepName:"+d+":SubStepName:"+e+":Status:"+f+":Message:"+a);
		  				++count;
				    		
		  		    	
		  		    	counter++;  
		  			
		  		    	
			    	}
		  		    	
		    	  }
		    	  
	  		    	 
		      }
		    // CheckPassFailMethod(TestArray); 	
	
			
	}
private static void CheckPassFailMethod(String[] TestsName) throws IOException {
	List<String> BRTag = new ArrayList<String>();
	
	for(int i=0;i<TestsName.length;i++)
	{
		List<String> count = new ArrayList<String>();
		List<String> count1 = new ArrayList<String>();
		List<String> count2 = new ArrayList<String>();
		String projectPath = System.getProperty("user.dir");
		File input = new File(projectPath+"\\ExecutionReports" + File.separator+TestsName[i]+".html");
		String htmlstring = FileUtils.readFileToString(input);
		Document htmlFile = null;
		
		try {
		     htmlFile = Jsoup.parse(input, "ISO-8859-1");
			} catch (Exception e) {
				System.out.println(e.toString());
			}
				
				Elements table = htmlFile.getElementsByTag("tbody");
				Elements rows=table.get(0).getAllElements();
				for(int j=0;j<rows.size();j++)
				{
					if(rows.get(j).className().equals("breakrow"))
					{
						String ff= Integer.toString(j);
						count.add(ff+":Header");
					
					}
					if(rows.get(j).className().equals("datarow"))
					{
						if(rows.get(j).text().contains("Fail"))
						{
							String ff= Integer.toString(j);
							count.add(ff+":Fail");
							
						}
					}
				
				}
				
				for(int j=0;j<count.size();j++)
				{
					if(count.get(j).contains("Fail"))
					{
						for(int ii=j-1;ii>=0;ii--)
						{
							if(count.get(ii).contains("Header"))
							{
								
								count1.add(count.get(ii).substring(0, count.get(ii).indexOf(":")));
								
								break;
							}
						}
					}
					
					
				}
				List<String> deduped = count1.stream().distinct().collect(Collectors.toList());
				System.out.println(deduped);
				for(int ii=0;ii<deduped.size();ii++)
				{
					int line = Integer.valueOf(deduped.get(ii));
					Elements a=rows.get(line).getElementsByTag("tr");
					System.out.println(a);
					String ff=a.toString();
					
					String hh=ff.replaceAll("</span>", "");
					//System.out.println("1"+hh);
					String text=hh.replace("\n", "");
					
					text=text.replaceAll("> <", "><");
					text=text.trim();
					System.out.println("1"+text);
					String gg=hh.replaceAll("Pass", "Fail");
					System.out.println("2"+gg);
					htmlstring = htmlstring.replaceAll(text, gg);
			System.out.println(htmlstring);
					//FileUtils.writeStringToFile(input, htmlstring);
					
				}
				
			}
	
	

	
}
/*public static String[][] Generate1(String status) throws IOException
{
	String projectPath = System.getProperty("user.dir");
	File input = new File(projectPath+"\\log" + File.separator+"application"+".html");
	Document htmlFile = null;
	try {
	     htmlFile = Jsoup.parse(input, "ISO-8859-1");
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		String date[] = htmlFile.body().getElementsByTag("body").text().split("time");		
		String date1[] = date[1].split("Time");
		Element table = htmlFile.select("table").get(0);
		Elements rows = table.select("tr");
		Element row = rows.get(rows.size()-1);
	    Elements cols = row.select("td");
	    String stringDate = cols.get(0).text();
	    int intDate = Integer.parseInt(stringDate)/1000;
	    String totalTime = intDate+" secs";
	   
	    List<String> tests = new ArrayList<String>();
		String s = htmlFile.body().getElementsByTag("body").text();
		
		String[] arr = s.split(" ");  
		for ( String ss : arr) {
			
		       if(ss.contains("Tests."))
		       {
		    		  String value =  between(ss, "Tests.", "$");

		    	   if(tests.contains(value)==false) //If that string is not added in List previously
		    		   tests.add(value); 
		       }
		  }
		//System.out.println(tests.size());
		String[] TestArray = TestCaseOrder(htmlFile,tests);
	
	
	      
	      int PassFail[]=PassFailCounter(htmlFile,tests);
	      String IndPass[]=IndividualPassFail(htmlFile,tests);
	   
	      int startCount = 0;
	      int lastCount = 0;
	      Element table1 = htmlFile.select("table").get(0); //select the first table.
	      Elements rows1 = table1.select("tr");
	      List<Integer> indTime = new ArrayList<Integer>(); //This list stores time for individual test
	      for(int i=1;i<=tests.size();i++)
	      {
	    	  for (int j = 1; j < rows1.size(); j++)
	    	  { //first row is the col names so skip it.
	  		    Element row1 = rows1.get(j);
	  		    Elements cols1 = row1.select("td");
	  		    
	  		    if(startCount==0) {
	  		    	if(cols1.get(1).text().equals("pool-"+i+"-thread-1") ||cols1.get(1).text().equals("main")||cols1.get(1).text().contains("pool-1-thread-"+i))
	  		    	{
	  		    		startCount = Integer.parseInt(cols1.get(0).text());
	  		    	}
	  		    }
	  		    
	  		    else
	  		    {
	  		    	if(cols1.get(1).text().equals("pool-"+i+"-thread-1")||cols1.get(1).text().equals("main")||cols1.get(1).text().contains("pool-1-thread-"+i))
	  		    	{
	  		    		lastCount = Integer.parseInt(cols1.get(0).text());
	  		    	}
	  		    }

	  		    }
	    	  int total = (lastCount - startCount)/1000;
	    	 
	    	  indTime.add(total);
	    	  startCount = 0;
	    	  }
	      int[] IndTime = new int[indTime.size()];
	      for(int i =0;i<indTime.size();i++)
	      {
	    	  IndTime[i]=indTime.get(i);
	      }
	      String type =ExecutionType(htmlFile);
	      createReportPage1(tests.size(),PassFail, intDate, TestArray,IndTime,IndPass,type);
	      //Test Step Details
	      int count = 0;
	      List<String> message = new ArrayList<String>(); //This list stores time for individual test
	      
	      List<Integer> stepTime = new ArrayList<Integer>(); //This list stores time for individual test
	      Element table2 = htmlFile.select("table").get(0); //select the first table.
	      Elements rows2 = table2.select("tr");
	    
	      for(int i=1;i<=tests.size();i++)
	      {
	    	 
	    	  String a;
	    	  Integer b;
	    	  int counter = 1;
	    	  for (int j = 1; j < rows2.size(); j++)
	    	  		{ //first row is the col names so skip it.
	  		    		Element row2 = rows2.get(j);
	  		    		Elements cols2 = row2.select("td");
	  		
	  		    		if(cols2.get(1).text().contains("pool-"+i+"-thread-1")||cols2.get(1).text().contains("main")||cols2.get(1).text().contains("pool-1-thread-"+i))
	  		    			{	
	  		    				message.add(cols2.get(5).text());
	  		    				a= message.get(count);
	  		    				int time =  Integer.parseInt(cols2.get(0).text());
	  		    				stepTime.add(time);
	  		    				b= stepTime.get(count);
	  		    				++count;
	  		    				//ReportPageUpdate(TestArray[i-1],a,b,counter,cols2.get(3).text());
	  		    				counter++;
	  		    	
	  		    			}
	  		  
	    	  		}
	    	  
	      }
	      String Status[][]=TestCaseStatus(TestArray,IndPass);
			return Status;
	      

		}*/
public static String[][] Generate1() throws IOException
{
	String[][] array = new String[ClsInitialize.mapDBMetricesData.size()][2];
	int count = 0;
	String final_status = null;
	for(Map.Entry<String,String> entry : ClsInitialize.mapDBMetricesData.entrySet()){
	    array[count][0] = entry.getKey();
	    String CombinedStatus = entry.getValue();
	    String[] Statusarr = CombinedStatus.split(";");
	    if(Statusarr[2].equalsIgnoreCase("Passed"))
	    {
	    	final_status = "Pass";
	    }
	    else if(Statusarr[2].equalsIgnoreCase("Failed"))
	    {
	    	final_status = "Fail";
	    }
	    else
	    	System.out.println("Scenario Status is different than pass or fail");
	    	
	    	
	    
	    array[count][1] = final_status;
	    count++;
	}
	return array;
	
	
}
		private static void ReportPageUpdate(String TestCaseName,String StepName, String SubStepName, String Status, String Message) throws IOException {
		
			String projectPath = System.getProperty("user.dir");
			File file = new File(projectPath+"\\ExecutionReports" + File.separator+TestCaseName+".html");
			
			if(!StepName.equals(" "))
			{
				CreateCollapseButton(file,StepName,SubStepName,Status, Message);
				
			}
			else
			{
				CreateContainer(file,StepName,SubStepName,Status, Message);
				
			}
		
			
		}

		private static void CreateContainer(File file, String StepName, String SubStepName, String Status,
				String Message) throws IOException {
			String UpdatedString2=null;
			//String Updated1 =null;
			String FinalString=null;
			String htmlstring = FileUtils.readFileToString(file);
			//String Updated1="<table style=\"height: 1px;\" width=\"153\";\"text align: center\"><table style=\"height: 1px; margin-left: auto; margin-right: auto;\" border=\"2\" width=\"653\" cellpadding=\"2\"><tbody><tr><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>Step Name</strong></span></em></td><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>Details</strong></span></em></td><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>Status</strong></span></em></td><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>ScreenShots</strong></span></em></td></tr>";
			if(Message.contains("Screen Shot taken and saved under path:"))
			{
				String SubString = Message.substring(40);
				
				if(Status.equals("Pass"))
				{
			//	UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #008000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></div></body></html>";
				UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left;\"><span style=\"color: #008000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></body></html>";
				}
				else
				{
					//UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #ff0000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left\"><span style=\"color: #ff0000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></body></html>";
				}
			}
			else 
			{
				if(Status.equals("Pass"))
				{
				//UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #008000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left;\"><span style=\"color: #008000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
				}
				else
				{
					//	UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #ff0000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left\"><span style=\"color: #ff0000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
				}
				
			}
			
			FinalString=UpdatedString2;
			htmlstring = htmlstring.replace("</tbody></table></div></body></html>",FinalString);
			FileUtils.writeStringToFile(file, htmlstring);
			
		}
		private static void CreateCollapseButton(File file, String StepName, String SubStepName, String Status,
				String Message) throws IOException {
			String htmlstring = FileUtils.readFileToString(file);
			String UpdatedString=null;
			String UpdatedString1=null;
			String UpdatedString2=null;
			String FinalString=null;
			if(Status.equals("Pass"))
			{
			UpdatedString="<tr class=\"breakrow\"><td style=\"width: 148.8px; text-align: center;\" colspan=\"4\"><span style=\"color: #3366ff\">"+StepName+"</td></tr>";
			//UpdatedString1="    <table style=\"height: 1px;\" width=\"153\";\"text align: center\"><br>&nbsp;</br><table style=\"height: 1px; margin-left: auto; margin-right: auto;\" border=\"2\" width=\"653\" cellpadding=\"2\"><tbody><tr><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>Step Name</strong></span></em></td><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>Details</strong></span></em></td><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>Status</strong></span></em></td><td style=\"width: 156.8px; text-align: center; background-color: #ffcc00;\"><em><span style=\"text-decoration: underline;\"><strong>ScreenShots</strong></span></em></td></tr>";
			}
			else if(Status.equals("Fail"))
			{
				UpdatedString="<tr class=\"breakrow\"><td style=\"width: 148.8px; text-align: center;\" colspan=\"4\"><span style=\"color: #3366ff\">"+StepName+"</td></tr>";
			}

			if(Message.contains("Screen Shot taken and saved under path:"))
			{
				String SubString = Message.substring(40);
				if(Status.equals("Pass"))
				{
			//	UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #008000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></div></body></html>";
				UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left;\"><span style=\"color: #008000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></body></html>";
				}
				else
				{
					//UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #ff0000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left\"><span style=\"color: #ff0000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"<a href="+SubString+">"+"Link </a></td></tr></tbody></table></div></body></html>";
				}
			}
			if(!Message.contains("Screen Shot taken and saved under path:"))
			{
				if(SubStepName.equals(" "))
				{
				if(Status.equals("Pass"))
				{
				//UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #008000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+StepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left;\"><span style=\"color: #008000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
				}
				else
				{
					//	UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #ff0000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+StepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left\"><span style=\"color: #ff0000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
				}
				}
				else
				{
					if(Status.equals("Pass"))
					{
					//UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #008000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
						UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left;\"><span style=\"color: #008000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
					}
					else
					{
						//	UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #ff0000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
						UpdatedString2="<tr class=\"datarow\"><td>"+SubStepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left\"><span style=\"color: #ff0000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
					}
				}
			}
			
			
			else
			{
				if(SubStepName.equals(" "))
				{
				if(Status.equals("Pass"))
				{
				//UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #008000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+StepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left;\"><span style=\"color: #008000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
				}
				else
				{
					//	UpdatedString2="<tr><td style=\"width: 156.8px; text-align: center;\">"+SubStepName+"</td><td style=\"width: 156.8px; text-align: center;\">"+Message+"</td><td style=\"width: 156.8px; color: #ff0000; text-align: center;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">N/A</td></tr></tbody></table></div></div></body></html>";
					UpdatedString2="<tr class=\"datarow\"><td>"+StepName+"</td><td>"+Message+"</td><td style=\"width: 148.8px; text-align: left\"><span style=\"color: #ff0000;\">"+Status+"</td><td style=\"width: 156.8px; text-align: center;\">"+"N/A</td></tr></tbody></table></div></body></html>";
				}
				}
			}
			FinalString=UpdatedString+UpdatedString2;
			htmlstring = htmlstring.replace("</tbody></table></div></body></html>",FinalString);
			FileUtils.writeStringToFile(file, htmlstring);
		}
		static String between(String value, String a, String b) {
	     
	        int posA = value.indexOf(a);
	        if (posA == -1) {
	            return "";
	        }
	        int posB = value.lastIndexOf(b);
	        if (posB == -1) {
	            return "";
	        }
	        int adjustedPosA = posA + a.length();
	        if (adjustedPosA >= posB) {
	            return "";
	        }
	        return value.substring(adjustedPosA, posB);
	    
	}

		private static void createReportPage1(int size, int[] PassFail, int Time,String[] TestName,int[] IndTime,String[] IndPass,String type) throws IOException
		{
			String projectPath = System.getProperty("user.dir");
	        String[] tempUrl=ReportGenerator.createReportPage3(size, TestName);
	        
	        String[] screenshotUrl = new String[size];
	        int Minute = Time/60;
	        int Seconds = Time%60;
	        String Path1 ="https://www.gstatic.com/charts/loader.js";
	        StringBuilder htmlStringBuilder=new StringBuilder();
			htmlStringBuilder.append("<html><head><style type=\"text/css\">#box1 {border-radius: 15px 50px;background: #ffcc00;padding: 20px; width: 150px;height: 80px; float: left;margin-top: 25px;margin-left: 200px;position:relative;text-align: center;}#box2 {height: 200px;width: 250px;border-radius: 15px 50px;background: #ffcc00;    padding: 20px;     width: 150px;    height: 80px;float: left;	margin-top: 25px;	margin-left: 90px;	position:relative;	text-align: center;}.outer{margin-left: 60px;}#box3 {	height: 200px;	width: 250px;	border-radius: 15px 50px;    background: #ffcc00;    padding: 20px;     width: 150px;    height: 80px;	float: left;	margin-top: 25px;	margin-left: 90px;	position: relative;text-align: center;	}</style>");
			htmlStringBuilder.append("</head><body><h1 style=\"text-align: center; font-size: 40px;\"><img style=\"float: left;margin-left: 90px;\"src=\"http://asc-sharepoint.sunlifecorp.com/sites/it/ASCITCOE/ASCITCoESite/Shared%20Documents/Agile%20Test%20Framework/Heading.png\" alt=\"Logo\" width=\"1380\" height=\"70\" /><h2 style=\"text-align: center; font-size: 23px;\"><span style=\"color: #33cccc;\"><em><span style=\"text-decoration: underline;\">Test Execution Summary</span></em></span></h2><div id=\"box1\"><font size=\"+1.4\" color=\"#3366ff\" ><center><b><i><u>Test Cases Executed</u>:</i></b></center></font><font size=\"+3.4\" color=\"#3366ff\">");
			htmlStringBuilder.append("<center>"+size+"</center></font></div><div id=\"box2\"><font size=\"+1.4\" color=\"#3366ff\" ><center><b><i><u>Pass/Fail Count </u>:</i></b></center></font><center><font size=\"+3.4\" color=\"#1ABC24\">"+PassFail[1]+"</font><font size=\"+3.4\" color=\"#3366ff\">/</font><font size=\"+3.4\" color=\"#BC1A1A\">"+PassFail[0]+"</font></center></div><div id=\"box3\"><font size=\"+1.4\" color=\"#3366ff\" ><center><b><i><u>Execution Time(Mins:Secs</u>):</i></b></center></font><font size=\"+3.4\" color=\"#3366ff\"><center>"+Minute+":"+Seconds+"</center></font></div><div id=\"box2\"><font size=\"+1.4\" color=\"#3366ff\" ><center><b><i><u>Execution Type</u>:</i></b></center></font><center><font size=\"+3\"color=\"#3366ff\">"+type+"</font></center></div><br>&nbsp;</br><br>&nbsp;</br><br>&nbsp;</br><br>&nbsp;</br><br>&nbsp;</br><h2 style=\"text-align: center; font-size: 20px;\"><span style=\"color: #33cccc;\"><em><span style=\"text-decoration: underline;\">Success Rate</span></em></span></h2><script type=\"text/javascript\">window.onload = function () {	var chart = new CanvasJS.Chart(\"chartContainer\",	{				legend: {			maxWidth: 100,			itemWidth: 50		},		data: [		{			type: \"pie\",			showInLegend: true,			legendText: \"{indexLabel}\",			dataPoints: [				{ y: "+PassFail[1]+", indexLabel: \"Pass\" },				{ y: "+PassFail[0]+", indexLabel: \"Fail\" },							]		}		]	});	chart.render();}</script><script type=\"text/javascript\" src=\"https://canvasjs.com/assets/script/canvasjs.min.js\"></script><div class =\"outer\"><div id=\"chartContainer\" style=\"height: 250px; width: 100%;\"</div></div><h2 style=\"text-align: center; font-size: 20px;\"><span style=\"color: #33cccc;\"><em><span style=\"text-decoration: underline;\">Test Case Executions</span></em></span></h2><table style=\"height: 71px; margin-left: auto; margin-right: auto;\" border=\"2\" width=\"977\" cellpadding=\"8px\">");
			htmlStringBuilder.append("<tbody><tr><td style=\"background-color: #ffcc00; width: 142px; text-align: center;\"><span style=\"color: #3366ff;\"><em><span style=\"text-decoration: underline;\"><strong>S.No</strong></span></em></span></td><td style=\"background-color: #ffcc00; width: 145.2px; text-align: center;\"><span style=\"color: #3366ff;\"><em><span style=\"text-decoration: underline;\"><strong>Test Case Name</strong></span></em></span></td><td style=\"background-color: #ffcc00; width: 150px; text-align: center;\"><span style=\"color: #3366ff;\"><em><span style=\"text-decoration: underline;\"><strong>Execution Status</strong></span></em></span></td><td style=\"background-color: #ffcc00; width: 150px; text-align: center;\"><span style=\"color: #3366ff;\"><em><span style=\"text-decoration: underline;\"><strong>Execution Time(Seconds)</strong></span></em></span></td></tr> ");
			
			for(int i=0;i<size;i++)
			{
				
				screenshotUrl[i]=projectPath+"\\ExecutionReports" + File.separator+TestName[i];
				
				if(IndPass[i]==null)
				{
					IndPass[i]="Pass";
				}
				htmlStringBuilder.append("<tr><td style=\"width: 144.4px; text-align: center;\">"+(i+1)+"</td>");
				htmlStringBuilder.append("<td style=\"width: 145.2px; text-align: center;\"><a href="+tempUrl[i]+">"+TestName[i]+"</a></td>");
				if(IndPass[i].equalsIgnoreCase("Fail"))
				{
					
				htmlStringBuilder.append("<td style=\"width: 148.4px; color:#ff0000; text-align: center;\">"+IndPass[i]+"</span></td><td style=\"width: 149.2px; text-align: center;\">"+IndTime[i]+" Seconds</td>");
							
				}
				else if(IndPass[i]=="Pass")
				{
					
					htmlStringBuilder.append("<td style=\"width: 148.4px; color:#339966; text-align: center;\">"+IndPass[i]+"</span></td><td style=\"width: 149.2px; text-align: center;\">"+IndTime[i]+" Seconds</td>");
					
				}
			}
			htmlStringBuilder.append("</tbody></table>");
			WriteToFile(htmlStringBuilder.toString(),"MainReport.html");
			
		}
		private static String[] createReportPage3(int size,String[] TestCaseNamePlaceHolder) throws IOException
				{
			String projectPath = System.getProperty("user.dir");
					String[] tempFile = new String[size];
				
					for(int i=0;i<size;i++)
					{
						StringBuilder htmlStringBuilder=new StringBuilder();
						//htmlStringBuilder.append("<h1 style=\"color: #5e9ca0; text-align: center;\"><span style=\"text-decoration: underline;\"><em><strong>Test Step Details</strong></em></span></h1><p>&nbsp;</p><p><strong>&nbsp;</strong></p><table style=\"height: 119px; margin-left: auto; margin-right: auto;\" border=\"1px\" width=\"691\" cellpadding=\"8px\"><tbody><tr style=\"height: 23.3px;\"><td style=\"width: 123.6px; height: 23.3px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong>Test Case Name</strong></span></em></td><td style=\"width: 117.2px; height: 23.3px; text-align: center;\"><strong><em><u>Step Number</u></em></strong></td><td style=\"width: 117.2px; height: 23.3px; text-align: center;\"><strong><em><u>Step Name</u></em></strong></td><td style=\"width: 114px; height: 23.3px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong>Message Logs</strong></span></em></td><td style=\"width: 126.8px; height: 23.3px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong>Execution Time</strong></span></em></td><td style=\"width: 126.8px; height: 23.3px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong>Screen Shots</strong></span></em></td></tr><tr style=\"height: 11px;\"></tbody></table>");
						htmlStringBuilder.append("<html> <head> <h1 style=\"text-align: center; font-size: 40px;\"><img style=\"float: left;margin-left: 90px;\"src=\"http://asc-sharepoint.sunlifecorp.com/sites/it/ASCITCOE/ASCITCoESite/Shared%20Documents/Agile%20Test%20Framework/Heading.png\" alt=\"Logo\" width=\"1400\" height=\"70\" /><h2 style=\"text-align: center; font-size: 23px;\"><span style=\"color: #33cccc;\"><em><span style=\"text-decoration: underline;\">Test Execution Summary</span></em></span></h2><meta charset=\"utf-8\" /> <meta name=\"viewport\" content=\"width=device-width\"> <style> #container{ margin:0 auto; width:80%; overflow:auto; } table.gridtable { margin:2 auto; width:95%; overflow:auto; font-family: helvetica,arial,sans-serif; font-size:14px; color:#333333; border-width: 1px; border-color: #ffcc00; border-collapse: collapse; text-align: left; } table.gridtable th { border-width: 1px; padding: 8px; border-style: solid; border-color: #666666; background-color: #ffcc00; } table.gridtable td { border-width: 1px; padding: 8px; border-style: solid; border-color: #666666; } </style> </head> <body> <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script> <script> $( document ).ready(function() { ");
						htmlStringBuilder.append("$('#tableMain').on('click', 'tr.breakrow',function(){ $(this).nextUntil('tr.breakrow').slideToggle(20); }); }); </script> <div class=\"container\" id=\"container\"> <table class=\"gridtable\" id=\"tableMain\"> <thead> <tr class=\"tableheader\"> <th style=\"width: 148.8px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong><span style=\"color: #3366ff; text-decoration: underline;\">Step Name</th> <th style=\"width: 148.8px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong><span style=\"color: #3366ff; text-decoration: underline;\">Step Message</th> <th style=\"width: 148.8px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong><span style=\"color: #3366ff; text-decoration: underline;\">Step Status</th> <th style=\"width: 148.8px; text-align: center;\"><em><span style=\"text-decoration: underline;\"><strong><span style=\"color: #3366ff; text-decoration: underline;\">ScreenShots</th> </tr> </thead> <tbody></tbody></table></div></body></html>");
						WriteToFile(htmlStringBuilder.toString(),TestCaseNamePlaceHolder[i]+".html");
					
						//String projectPath ="http://asc-sharepoint.sunlifecorp.com/sites/it/ASCITCOE/ASCITCoESite/SiteAssets";
						tempFile[i] = projectPath+"\\ExecutionReports" + File.separator+TestCaseNamePlaceHolder[i]+".html";
						
						tempFile[i]=tempFile[i].replaceAll(" ", "%20");
						
						//tempFile[i] = projectPath+ File.separator+TestCaseNamePlaceHolder[i]+".html";
					}
					return tempFile;
				}
		private static void WriteToFile(String fileContent, String fileName) throws IOException {
		String projectPath = System.getProperty("user.dir");
		
		File folder = new File(projectPath+"//ExecutionReports");
		boolean Exsits = folder.exists();
		if(!Exsits)
		{
			folder.mkdir();
		}
        String tempFile = projectPath+"//ExecutionReports" + File.separator+fileName;
       
     


        
       
        File file = new File(tempFile);
        if (file.exists()) {
            try {
                File newFileName = new File(projectPath +"//BackUpReports"+ File.separator+ "backup_"+fileName);
                file.renameTo(newFileName);
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        OutputStream outputStream = new FileOutputStream(file.getAbsoluteFile());
        Writer writer=new OutputStreamWriter(outputStream);
        writer.write(fileContent);
        writer.close();	
		}
		private static String ExecutionType(Document htmlFile)
		{
			Element table = htmlFile.select("table").get(0); 
			Element row1 = table.select("tr").get(1);
			Element row2 = table.select("tr").get(3);
	  		 Elements cols1 = row1.select("td");
	  		Elements cols2 = row2.select("td");
	  		String type=" ";
	  		if(cols1.get(1).text().equalsIgnoreCase(cols2.get(1).text()))
	  		{
	  			if(!cols1.get(1).text().contains("main"))
	  			{
	  			type ="Serial";
	  			}
	  			else
	  			{
	  				type = "N/A";
	  			}
	  		}
	  		else
	  		{
	  			type = "Parallel";
	  		}
			return type;
		}
		private static int[] PassFailCounter(Document htmlFile, List<String> tests)
		{
			List<String> message = new ArrayList<String>(); 
		      
		      List<Integer> stepTime = new ArrayList<Integer>();
		      Element table2 = htmlFile.select("table").get(0); 
		      Elements rows2 = table2.select("tr");
		      int count =0;
		      int Passcount=0;
		      String[] FailedCase= new String[tests.size()];
		      String[] interm = new String[tests.size()];
		      	for(int i=1;i<=tests.size();i++)
		      	{
		    	  for (int j = 1; j < rows2.size(); j++)
		    	  { 
		  		    Element row2 = rows2.get(j);
		  		    Elements cols2 = row2.select("td");
		  		
		  		    if(cols2.get(1).text().contains("pool-"+i+"-thread-1")||cols2.get(1).text().contains("main")||cols2.get(1).text().contains("pool-1-thread-"+i))
			    	{	
		  		    	 if(cols2.get(2).text().contains("ERROR")|| (cols2.get(2).text().contains("FATAL")))
				  		    {
		  		    		
		  		    		count++;
		  		    		  FailedCase[i-1]="Fail";
		  		    		  break;
				  		    }	
			    	}
		    	  }
		      }
		      	 
		      Passcount = tests.size()-count;
		      int[] PassFail = new int[2];
		      PassFail[0]=count;
		      PassFail[1]=Passcount;
		     
		return PassFail;
		}
		private static String[] IndividualPassFail(Document htmlFile, List<String> tests)
		{
			List<String> message = new ArrayList<String>(); 
		      List<Integer> stepTime = new ArrayList<Integer>(); 
		      Element table2 = htmlFile.select("table").get(0);
		      Elements rows2 = table2.select("tr");
		      int count =0;
		      int Passcount=0;
		      String[] FailedCase= new String[tests.size()];
		      String[] interm = new String[tests.size()];
		      	for(int i=1;i<=tests.size();i++)
		      	{
		    	  for (int j = 1; j < rows2.size(); j++)
		    	  { 
		  		    Element row2 = rows2.get(j);
		  		    Elements cols2 = row2.select("td");
		  		
		  		    if(cols2.get(1).text().contains("pool-"+i+"-thread-1")||cols2.get(1).text().contains("main")||cols2.get(1).text().contains("pool-1-thread-"+i))
			    	{	
		  		    	
		  		    	 if(cols2.get(2).text().contains("ERROR")|| (cols2.get(2).text().contains("FATAL")))
				  		    {
		  		    		
		  		    		  FailedCase[i-1]="Fail";
				  		    }	
			    	}
		    	  }
		      }
		     
		      
		     
		return FailedCase;
		}
	private static String[] TestCaseOrder(Document htmlFile,List<String> tests)
	{
		List<String> message = new ArrayList<String>(); 
	      List<String> tests1 = new ArrayList<String>(); 
	      Element table2 = htmlFile.select("table").get(0);
	      Elements rows2 = table2.select("tr");
	      String s = htmlFile.body().getElementsByTag("body").text();
			
			String[] arr = s.split(" ");  
			String ss = null;
	  	for(int i=1;i<=tests.size();i++)
      	{
    	  for (int j = 1; j < rows2.size(); j++)
    	  { 
  		    Element row2 = rows2.get(j);
  		    Elements cols2 = row2.select("td");
  		
  		    if(cols2.get(1).text().contains("pool-"+i+"-thread-1")||cols2.get(1).text().contains("main")||cols2.get(1).text().contains("pool-1-thread-"+i))
	    	{	
  		    	if(cols2.get(3).text().contains("Tests."))
	  		    {
  		    		String value =  between(cols2.get(3).text(), "Tests.", "$");

			    	  // if(tests.contains(value)==false) //If that string is not added in List previously
			    		   tests1.add(cols2.get(3).text());
		    		  
	  		    }}	
	    	}}
	  	
	  	String[] a= new String[tests1.size()];
	  	String[] b= new String[tests1.size()];
	  	List<String> tests2 = new ArrayList<String>(); 
	  	for(int i=0;i<tests1.size();i++)
	  	{
	  		a[i]=tests1.get(i);
	  		long count=a[i].chars().filter(ch->ch=='.').count();
    	   	String ClassName = a[i].split("\\.")[(int) count];
    	   	b[i]=ClassName.split("\\$")[0];
	  		//b[i]=between(a[i], "Tests.", "$");
	  	
	  		//System.out.println(b[i]);
	  	}
	  	Set<String> temp = new LinkedHashSet<String>(Arrays.asList(b));
	  	String[] result = temp.toArray( new String[temp.size()] );
	  	
		return result;
  		   
	  
	}
	private static String[][] TestCaseStatus(String[] testArray, String[] indPass) {
		
		String TwodArray[][]= new String[2][testArray.length];
		for(int i =0;i<testArray.length;i++)
		{
			if(indPass[i].equalsIgnoreCase(null))
			{
				indPass[i]="Pass";
			}
			TwodArray[0][i]=testArray[i];
			TwodArray[1][i]=indPass[i];
			
		}
	
		return TwodArray;

}
	private static void FilesDelete(String ReportPath)
	{
		File file = new File(ReportPath);      
	
	           if(file.isDirectory()){
	        	   String[]  myFiles = file.list();
	               for (int i=0; i<myFiles.length; i++) {
	                   File myFile = new File(file, myFiles[i]); 
	                   myFile.delete();
	               }
	            }
	}
	public static void main(String[] a) throws IOException
	{
		ReportGenerator.Generate("true");
	}
	
	public static  HashMap<String, String> GetResult(String status) throws IOException, ParseException
	{	
		String projectPath = System.getProperty("user.dir");
		File input = new File(projectPath+"\\log" + File.separator+"application"+".html");
		Document htmlFile = null;
		try {
		     htmlFile = Jsoup.parse(input, "ISO-8859-1");
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			String date[] = htmlFile.body().getElementsByTag("body").text().split("time");		
			String date1[] = date[1].split("Time");
			Element table = htmlFile.select("table").get(0);
			Elements rows = table.select("tr");
			Element row = rows.get(rows.size()-1);
		    Elements cols = row.select("td");
		    String stringDate = cols.get(0).text();
		    int intDate = Integer.parseInt(stringDate)/1000;
		    String totalTime = intDate+" secs";
		    

		  //Getting Session start Date and Time from applicationlog and Convert into TimeStamp
		  	    String[] CurrDate = date1[0].split(",");
		  	    SimpleDateFormat sdf1=new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		        Date currentdate=sdf1.parse(CurrDate[0].trim());
		        Timestamp StarttimeStamp = new Timestamp(currentdate.getTime());
		        //System.out.println("Starting Time = "+StarttimeStamp );
		        List<Integer> startTime = new ArrayList<Integer>();
		        List<String> tests = new ArrayList<String>();
		        String s = htmlFile.body().getElementsByTag("body").text();		
		        String[] arr = s.split(" ");  
		        for ( String ss : arr) 
		        {
				  if(ss.contains("Tests."))
			       {
					  String value =  between(ss, "Tests.", "$");
			    	   if(tests.contains(value)==false) //If that string is not added in List previously
			    		   tests.add(value); 
			       }
			  }
			  //System.out.println(tests.size());
		      String[] TestArray = TestCaseOrder(htmlFile,tests);	      
		      int PassFail[]=PassFailCounter(htmlFile,tests);
		      String IndPass[]=IndividualPassFail(htmlFile,tests);	   
		      int startCount = 0;
		      int lastCount = 0;
		      Element table1 = htmlFile.select("table").get(0); //select the first table.
		      Elements rows1 = table1.select("tr");
		      List<Integer> indTime = new ArrayList<Integer>(); //This list stores time for individual test
		      for(int i=1;i<=tests.size();i++)
		      {
		    	  for (int j = 1; j < rows1.size(); j++)
		    	  { //first row is the col names so skip it.
		  		    Element row1 = rows1.get(j);
		  		    Elements cols1 = row1.select("td");
		  		    
		  		    if(startCount==0) {
		  		    	if(cols1.get(1).text().equals("pool-"+i+"-thread-1") ||cols1.get(1).text().equals("main")||cols1.get(1).text().contains("pool-1-thread-"+i))
		  		    	{	  		    		  		    		
		  		    		startCount = Integer.parseInt(cols1.get(0).text());
		  		    		if(startCount!=0)
		  		    		{
		  		    		startTime.add(Integer.parseInt(cols1.get(0).text()));
		  		    		}
		  		    	}
		  		    }
		  		    
		  		    else
		  		    {
		  		    	if(cols1.get(1).text().equals("pool-"+i+"-thread-1")||cols1.get(1).text().equals("main")||cols1.get(1).text().contains("pool-1-thread-"+i))
		  		    	{
		  		    		lastCount = Integer.parseInt(cols1.get(0).text());
		  		    	}
		  		    }

		  		    }
		    	  int total = (lastCount - startCount)/1000;
		    	 
		    	  indTime.add(total);
		    	  //System.out.println("Report Generate1 Total time(get) = " +indTime);
		    	  startCount = 0;
		    	  }
		      int[] IndTime = new int[indTime.size()];
		      for(int i =0;i<indTime.size();i++)
		      {
		    	  IndTime[i]=indTime.get(i);
		      }
		      //Adding Test Results in Array
		      for(int i =0;i<IndPass.length;i++)
		      {
		    	  if(IndPass[i] == null)
		    	  {
		    		  IndPass[i] = "Pass";
		    	  }
		      }      
		
		  		 int[] arr_StartTime = new int[startTime.size()];
		  		 
			      for(int st =0;st<startTime.size();st++)
			      {
			    	  arr_StartTime[st]=startTime.get(st);
			    	  //System.out.println("Starttime Array = " +startTime.get(st));
			    	  
			      }
		// Store the Data(Tests, CurentData,StartTime,TotalExetime and Test Execution Status) in Hashmap      			
		 HashMap<String, String> Resultrec = new HashMap<String,String>();
		 for(int i = 0; i< TestArray.length;i++)
		  {		 
			Resultrec.put(TestArray[i],CurrDate[0].trim() +";"+arr_StartTime[i] +";"+IndTime[i] + ";" +IndPass[i] );		
			 
		  }	  
		  return  Resultrec;   
	      
	}

	
}