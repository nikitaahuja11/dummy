package FrameworkSource.global.almbridge;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import FrameworkSource.global.restutilities.*;
import org.junit.Assert;

/**
 * This class shows how to login/logout/authenticate to the server with REST.
 */
public class Authentication {


	public void authenticateLoginLogoutExample(final String serverUrl,
			final String domain, final String project, String username,
			String password) throws Exception {

		RestConnector con =
				RestConnector.getInstance().init(
						new HashMap<String, String>(),
						serverUrl,
						domain,
						project);

		Authentication example =
				new Authentication();

		//if we're authenticated we'll get a null, otherwise a URL where we should login at (we're not logged in, so we'll get a URL).
		String authenticationPoint = example.isAuthenticated();
		Assert.assertTrue(
				"response from isAuthenticated means we're authenticated. that can't be.",
				authenticationPoint != null);

		//now we login to previously returned URL.
		boolean loginResponse =
				example.login(authenticationPoint, username, password);
		Assert.assertTrue("failed to login.", loginResponse);
		Assert.assertTrue(
				"login did not cause creation of Light Weight Single Sign On(LWSSO) cookie.",
				con.getCookieString().contains("LWSSO_COOKIE_KEY"));

		//proof that we are indeed logged in
		Assert.assertNull(
				"isAuthenticated returned not authenticated after login.",
				example.isAuthenticated());

	}

	private RestConnector con;

	public Authentication() {
		con = RestConnector.getInstance();
	}

	/**
	 * @param username
	 * @param password
	 * @return true if authenticated at the end of this method.
	 * @throws Exception
	 *
	 * convenience method used by other examples to do their login
	 */
	public boolean login(String username, String password) throws Exception {

		/**
		 * Get the current authentication status.
		 */
		String authenticationPoint = this.isAuthenticated();
	//	System.out.println("login1"+authenticationPoint);

		
		/**
		 * If the authenticationPoint is null, the user is already
		 * authenticated. In this case no login necessary.
		 */
		if (authenticationPoint != null) {
			return this.login(authenticationPoint, username, password);
		}
		return true;
	}

	/**
	 * @param loginUrl
	 *            to authenticate at
	 * @param username
	 * @param password
	 * @return true on operation success, false otherwise
	 * @throws Exception
	 *
	 * Logging in to our system is standard http login (basic authentication),
	 * where one must store the returned cookies for further use.
	 */
	public boolean login(String loginUrl, String username, String password)
			throws Exception {

		//create a string that lookes like:
		// "Basic ((username:password)<as bytes>)<64encoded>"
		byte[] credBytes = (username + ":" + password).getBytes();
		String credEncodedString = "Basic " + Base64Encoder.encode(credBytes);

		Map<String, String> map = new HashMap<String, String>();
		map.put("Authorization", credEncodedString);

		Response response = con.httpGet(loginUrl, null, map);
		//System.out.println("login2response"+response.getStatusCode());
		//System.out.println("login2ok"+HttpURLConnection.HTTP_OK);


		boolean ret = response.getStatusCode() == HttpURLConnection.HTTP_OK;
	//	System.out.println("login2"+ret);

		return ret;
	}

	/**
	 * @return true if logout successful
	 * @throws Exception
	 *             close session on server and clean session cookies on client
	 */
	public boolean logout() throws Exception {

		//note the get operation logs us out by setting authentication cookies to:
		// LWSSO_COOKIE_KEY="" via server response header Set-Cookie
		Response response =
				con.httpGet(con.buildUrl("authentication-point/logout"),
						null, null);

		return (response.getStatusCode() == HttpURLConnection.HTTP_OK);

	}

	/**
	 * @return null if authenticated.<br>
	 *         a url to authenticate against if not authenticated.
	 * @throws Exception
	 */
	public String isAuthenticated() throws Exception {

		String isAuthenticateUrl = con.buildUrl("rest/is-authenticated");
		String ret;

		Response response = con.httpGet(isAuthenticateUrl, null, null);
		int responseCode = response.getStatusCode();

		//if already authenticated
		if (responseCode == HttpURLConnection.HTTP_OK) {

			ret = null;
		}

		//if not authenticated - get the address where to authenticate
		// via WWW-Authenticate
		else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

			Iterable<String> authenticationHeader =
					response.getResponseHeaders().get("WWW-Authenticate");

			String newUrl =
					authenticationHeader.iterator().next().split("=")[1];
			newUrl = newUrl.replace("\"", "");
			newUrl += "/authenticate";
			ret = newUrl;
		}

		//Not ok, not unauthorized. An error, such as 404, or 500
		else {

			throw response.getFailure();
		}

		return ret;
	}

	
	//Base64Encoder class
	public static class Base64Encoder {

		private final static char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
				.toCharArray();

		private static int[] toInt = new int[128];

		static {
			for (int i = 0; i < ALPHABET.length; i++) {
				toInt[ALPHABET[i]] = i;
			}
		}

		/**
		 * Translates the specified byte array into Base64 string.
		 *
		 * @param buf
		 *            the byte array (not null)
		 * @return the translated Base64 string (not null)
		 */
		public static String encode(byte[] buf) {
			int size = buf.length;
			char[] ar = new char[((size + 2) / 3) * 4];
			int a = 0;
			int i = 0;
			while (i < size) {
				byte b0 = buf[i++];
				byte b1 = (i < size) ? buf[i++] : 0;
				byte b2 = (i < size) ? buf[i++] : 0;

				int mask = 0x3F;
				ar[a++] = ALPHABET[(b0 >> 2) & mask];
				ar[a++] = ALPHABET[((b0 << 4) | ((b1 & 0xFF) >> 4)) & mask];
				ar[a++] = ALPHABET[((b1 << 2) | ((b2 & 0xFF) >> 6)) & mask];
				ar[a++] = ALPHABET[b2 & mask];
			}
			switch (size % 3) {
			case 1:
				ar[--a] = '=';
			case 2:
				ar[--a] = '=';
			}
			return new String(ar);
		}
	}
	
	
	
}
