package FrameworkSource.global.Read_OR_Data;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.ReportEvents;
import FrameworkSource.UI.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;

//import com.steadystate.css.parser.ParseException;
/**
 * ORReader class consists of all the functions related to OR(Object Repository) i.e Properties.xlsx.
 */
public class ORReader extends ClsInitialize{
	

	ReadJSONOR objRead = new ReadJSONOR();
	static LinkedHashMap<String, Object> mapOR;
	private static final String STRLOGDEBUG = "debug";
	private static final String STRLOGTYPE = "LogType";
	private static final String STRLOGMESSAGE = "Unable to Locate the Element";
	private static final String STRLOGMESSAGEOR = "Element with ";

	
	public String[][] Properties(String page) throws IOException,InterruptedException
	{
		try{
			if(ClsInitialize.properties.get("Use_JSON_As_OR").toString().trim().equalsIgnoreCase("YES"))
			{
				try{
				ReportEvents.Done("Reading the OR File Using JSON", "Starting the JSON-OR Reading");
				func_ReadJSON(page);
				ReportEvents.Done("Reading the OR File Using JSON", "The OR Reading Completed");
				}catch(Exception objJson)
				{
					ReportEvents.Error("Reading the JSON-OR File", objJson);
				}
			}
			else
			{
				try{
					//ReportEvents.Done("Reading the OR File Using Excel", "Starting the Excel-OR Reading"); 
					if (ClsInitialize.strFilePathOR.contains(";"))
					{
						String[] arrMulPath = ClsInitialize.strFilePathOR.split(";");
						for(int iCount = 0;iCount<arrMulPath.length;iCount++)
						{
							if (arrMulPath[iCount].length() > 0 && func_ReadExcel(arrMulPath[iCount],page))
							{
								//break;
							}
						}
					}
					else
					{
						func_ReadExcel(ClsInitialize.strFilePathOR,page);
					}
					//ReportEvents.Done("Reading the OR File Using Excel", "Completed the Excel-OR Reading");
				}catch(Exception objORExcel)
				{
					ReportEvents.Error("Reading the Excel-OR File", objORExcel);
				}
			}
		}catch(Exception objRead)
		{
			ReportEvents.Error("Reading the OR File", objRead);
		}
		return null;
		
	}
	
	private boolean func_ReadExcel(String strSheet,String orSheet) throws IOException, InterruptedException
	{
		
		boolean blnResult = false;
		mapOR = new LinkedHashMap<String, Object>();
		File file =    new File(strSheet);
	    FileInputStream inputStream = new FileInputStream(file);
	    Workbook workbook = null;
	    workbook = new XSSFWorkbook(inputStream);
	    Sheet sheet;

	    try{
	    	sheet = workbook.getSheet(orSheet);
	    	if(sheet == null)
	    	{
	    		ReportEvents.Done("WARNING::Error Locationg the sheet ","Sheet: "+orSheet+" is not present in excel " + strSheet);
	    		//Assert.fail();
	    		
	    	}
	    	else
	    	{
			    for (int i = 1; i < sheet.getLastRowNum()-sheet.getFirstRowNum() +1; i++) 
			    {
			    	Row row = sheet.getRow(i);
			    	
			    	switch (ClsInitialize.strPlatform.toUpperCase()) {
		            case "WEB": 
		            		if(ClsInitialize.strLanguage.equalsIgnoreCase("english"))
		            		{
		            			mapOR.put(row.getCell(0).getStringCellValue().trim(), row.getCell(1).getStringCellValue().trim() + ";" + row.getCell(2).getStringCellValue().trim());
		            		}
		            		else
		            		{
		            			mapOR.put(row.getCell(0).getStringCellValue().trim(), row.getCell(3).getStringCellValue().trim() + ";" + row.getCell(4).getStringCellValue().trim());
		            		}
		                     break;
		            case "MOBILE":
			            	if(ClsInitialize.strDevice.equalsIgnoreCase("android"))
			        		{
			            		mapOR.put(row.getCell(0).getStringCellValue().trim(), row.getCell(1).getStringCellValue().trim() + ";" + row.getCell(2).getStringCellValue().trim());
			        		}
			        		else
			        		{
			        			mapOR.put(row.getCell(0).getStringCellValue().trim(), row.getCell(3).getStringCellValue().trim() + ";" + row.getCell(4).getStringCellValue().trim());
			        		}
			                 break;
		            default: break;
			    	}	 
			    }
	    	}
		    workbook.close();//Newly added
	    	blnResult = true;
	    }catch(Exception obj)
	    {
	    	blnResult = false;
			//ReportEvents.Done("Error Reading the excel File", "Returning False");
	    }
	    
	   return blnResult;
	  
	}
	

	
	private void func_ReadJSON(String strPage) throws IOException 
	{
		try{
			mapOR = objRead.fnReadOR(strPage, ClsInitialize.strPlatform);
		}catch(Exception objfunc_ReadJSON)
		{
			ReportEvents.Error("Reading JSON", objfunc_ReadJSON);
		}
		
	}

	public boolean FindProperty(String Value) throws IOException
	{
		boolean blnResult = false;
		try{
			
			if(mapOR.get(Value)!= null)
			{
				blnResult = true;
			}
			else
			{
				blnResult = false;
			}
			
		}catch(Exception eOR)
		{
			blnResult = false;
			ReportEvents.Done("WARNING::Error Locationg Object","Unable to Locate the Element " + Value);
		}
		return blnResult;
	}
	
	public String[] FindExactvalue(String Value) throws IOException, org.json.simple.parser.ParseException
	{

		JSONObject objLangProp,objProp;
		String[] FoundValue = new String[2];
		try{
			if(ClsInitialize.properties.get("Use_JSON_As_OR").toString().trim().equalsIgnoreCase("Yes"))
			{
				objLangProp =  (JSONObject) mapOR.get(Value);
				if(ClsInitialize.strPlatform.equalsIgnoreCase("Web"))
				{
				if (ClsInitialize.strLanguage.equalsIgnoreCase("english"))
				{
					
		            objProp = (JSONObject)objLangProp.get("English");
				}
				else
				{
					objProp = (JSONObject)objLangProp.get("French");
				}
				}
				else
				{
					if(ClsInitialize.strDevice.equalsIgnoreCase("Android"))
					{
						objProp = (JSONObject)objLangProp.get("Android");
					}
					else
						objProp = (JSONObject)objLangProp.get("iOS");
				}
				FoundValue[0] = (String) objProp.get("Property");
				FoundValue[1] = (String) objProp.get("Value");
			}
			else
			{
				FoundValue[0] = mapOR.get(Value).toString().trim().split(";")[0];
				FoundValue[1] = mapOR.get(Value).toString().trim().split(";")[1];
			}
			ReportEvents.Done("Element Located in OR","Element with "+Value+" Found in OR");
		}catch(Exception objFindExactValue)
		{
			ReportEvents.Done("WARNING::Error Locationg Object","Unable to Locate the Element " + Value);
			FoundValue = null;
		}
		return FoundValue;
	
	}
	
	public boolean fnSetProperty(String strLogicalName, String strPropertyID, String strPropValue) throws IOException
	{
		boolean blnUpdateProp = false;
		try{
			if(mapOR == null)
			{
				mapOR = new LinkedHashMap<String, Object>();
			}
			if(mapOR.containsKey(strLogicalName)){
				mapOR.put(strLogicalName, strPropertyID + ";" + strPropValue);
				ReportEvents.Done("Element Located in OR",STRLOGMESSAGEOR+strLogicalName+" Updated in Run Time OR: " + strPropertyID + " : " + strPropValue);
			}
			else{
				mapOR.put(strLogicalName, strPropertyID + ";" + strPropValue);
				ReportEvents.Done("Element Added in OR",STRLOGMESSAGEOR+strLogicalName+" Updated in Run Time OR: " + strPropertyID + " : " + strPropValue);
			}

		}catch(Exception objFindExactValue){
			
			ReportEvents.Done("WARNING::Error Adding Object to Runtime OR",STRLOGMESSAGE + " " + strLogicalName);
		}
		return blnUpdateProp;
	
	}
}
