package FrameworkSource.global.Read_OR_Data;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import FrameworkSource.global.Intialize.ClsInitialize;




public class ReadJSONOR {
	
	
	public static LinkedHashMap<String, Object> mapOR = null;
	
    public static LinkedHashMap fnReadOR(String strPage,String strPlatform) {

    	
    	LinkedHashMap<String, Object> mapTestData = new LinkedHashMap<String, Object>();
        JSONParser parser = new JSONParser();
        String key;
        Object keyvalue,obj = null;
        JSONObject objParentProp,objPropPlatform,objPage = null;
       

        try {
        	if (ClsInitialize.strFilePathOR.contains(";"))
			{
				String[] arrMulPath = ClsInitialize.strFilePathOR.split(";");
				for(int iCount = 0;iCount<arrMulPath.length;iCount++)
				{
					if (arrMulPath[iCount].length() > 0)
					{
						obj = parser.parse(new FileReader(arrMulPath[iCount]));	
						 objParentProp =  (JSONObject) obj;
				            if (strPlatform.equalsIgnoreCase("web"))
				            {
				            	objPropPlatform = (JSONObject)objParentProp.get("Properties_Web");
				            }
				            else
				            {
				            	objPropPlatform = (JSONObject)objParentProp.get("Properties_Mobile");
				            }
				            try{
				            	objPage = (JSONObject)objPropPlatform.get(strPage);
				            	break;
				            }catch(Exception e)
				            {}
					}
				}
			}
			else
			{
				obj = parser.parse(new FileReader(ClsInitialize.strFilePathOR));
				 objParentProp =  (JSONObject) obj;
		            if (strPlatform.equalsIgnoreCase("web"))
		            {
		            	objPropPlatform = (JSONObject)objParentProp.get("Properties_Web");
		            }
		            else
		            {
		            	objPropPlatform = (JSONObject)objParentProp.get("Properties_Mobile");
		            }
		            
		            objPage = (JSONObject)objPropPlatform.get(strPage);
			}
        	
        	
           

        	//JSONObject jsonObject = objPage.get(0);
			Iterator<String> keysItr = objPage.keySet().iterator();
            while(keysItr.hasNext()) {
                key = keysItr.next();
                keyvalue = objPage.get(key);
                mapTestData.put(key, keyvalue);
            }
            	
           
            	
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch(Exception eGenericException)
        {
        	mapTestData= null;
        	eGenericException.printStackTrace();
        }
        
      
        
        return mapTestData;	
    }

}
