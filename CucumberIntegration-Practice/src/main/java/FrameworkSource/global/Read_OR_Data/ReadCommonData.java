package FrameworkSource.global.Read_OR_Data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.reporter.ReportEvents;




public class ReadCommonData {
	
	
	public static LinkedHashMap<String, Object> mapCommonData = null;
	
	
    public static LinkedHashMap fnReadCommonData(String strParam, String strRoot) throws IOException 
    {
    	LinkedHashMap<String, Object> mapTestData = new LinkedHashMap<String, Object>();
    	JSONParser parser = new JSONParser();
        String key;
        Object keyvalue,obj;
    	
    	if(ClsInitialize.properties.getProperty("Use_JSON_As_Environment").equalsIgnoreCase("yes"))
    	{

            JSONObject objParentProp,objPropPlatform,objPage;
           

            try {     
                obj = parser.parse(new FileReader("src\\test\\resources\\Data\\CommonData.json"));
                objParentProp =  (JSONObject) obj;
                
            	objPropPlatform = (JSONObject)objParentProp.get(strRoot);
                
            	if(strParam.length()!=0)
            		objPage = (JSONObject)objPropPlatform.get(strParam);
            	else
            		objPage = objPropPlatform;
    			Iterator<String> keysItr = objPage.keySet().iterator();
                while(keysItr.hasNext()) {
                    key = keysItr.next();
                    keyvalue = objPage.get(key);
                    mapTestData.put(key, keyvalue);
                }
                	
               
                	
            } catch (FileNotFoundException e) {
            	ReportEvents.Error("Exception::Reading Common Data", e);
            	mapTestData= null;
            } catch (IOException e) {
            	ReportEvents.Error("Exception::Reading Common Data", e);
            	mapTestData= null;
            }catch(Exception eGenericException)
            {
            	ReportEvents.Error("Exception::Reading Common Data", eGenericException);
            	mapTestData= null;
            }
    	}
    	else
    	{
    		String DataTable = System.getProperty("user.dir") + "\\src\\test\\resources\\Data\\CommonData.xlsx";
    	    XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(new File(DataTable)));
    		XSSFSheet sheet = workbook.getSheet(strRoot);
    	    DataFormatter formatter = new DataFormatter();
    	    Row row,headerrow;
    	    String firstRow = "";
    	    
    	    int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
    	    
    	    for (int i = 1; i < rowCount+1; i++) 
    	    {

	    	     row = sheet.getRow(i);
	    	     headerrow = sheet.getRow(0);
	    	     if(strParam.length()!=0)
	    	    	  firstRow = formatter.formatCellValue(row.getCell(0)); 
	    	    	 
	    	     if(firstRow.equalsIgnoreCase(strParam))
	    	     {
	    	    	 for(int j=0 ; j<headerrow.getLastCellNum() ; j++)
	    	    	 {
	    	    		 if(j==4 || j==5)
	    	    		 	{
	    	    			 	key = formatter.formatCellValue(headerrow.getCell(j));
	    	    			 	keyvalue = row.getCell(j).getRichStringCellValue();
	    	    			 
	    	    		 	}
	    	    		 else
	    	    		 	{
	    	    			 	key = formatter.formatCellValue(headerrow.getCell(j));
	    	    			 	keyvalue = formatter.formatCellValue(row.getCell(j));
	    	    		 	}
	    	    		 mapTestData.put(key, keyvalue);
	    	    	 }
	    	        	break;
	    	      }
	    	        
	    	    }//End of for    		
    	}

    	
    	
        return mapTestData;	
    }
}
