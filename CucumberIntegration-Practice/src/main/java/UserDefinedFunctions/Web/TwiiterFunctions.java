package UserDefinedFunctions.Web;

import static java.util.stream.Collectors.toMap;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.common.collect.Lists;

import FrameworkSource.UI.Common.Button;
import FrameworkSource.UI.Common.Label;
import FrameworkSource.UI.Common.TextBox;
import FrameworkSource.UI.Web.Browser;
import FrameworkSource.UI.Web.Page;
import FrameworkSource.global.Intialize.ClsInitialize;
import FrameworkSource.global.Intialize.CommonImplementation;
import FrameworkSource.global.reporter.ReportEvents;

public class TwiiterFunctions
{
	Browser b = ClsInitialize.iobjBrowser;
	Page p= ClsInitialize.iobjPage;
	   HashMap<String,Integer> likes=new HashMap<String, Integer>();
	      HashMap<String,Integer> retweets=new HashMap<String, Integer>();
	public TwiiterFunctions(Browser browser, Page page) 
	{
		this.b = browser;
		this.p = page;
		
		
	}

	
	public void Tweets() throws InterruptedException, IOException
	{
		p.SetCurrentPage("STEPIN");
		 List<String> TweetList= new ArrayList<String>();
	      List<String> ReTweetList= new ArrayList<String>();
	      List<String> LikesList= new ArrayList<String>();
	  	List<Integer> ListLikes =  new ArrayList<Integer>();
	  	List<Integer> ListRetweet =  new ArrayList<Integer>();
	  	System.out.println("Akshey");
	  	List<WebElement> Tweet =  new TextBox(b, "tweet").fnWebElementsLocation();
	  	
	  	List<WebElement> Retweet =  new TextBox(b, "Retweet").fnWebElementsLocation();
	  	List<WebElement> Likes =  new TextBox(b, "Likes").fnWebElementsLocation();
	  	if((Tweet==null)||(Retweet==null)||(Likes==null))
	  	{
	  		ReportEvents.Reporter("Fail", "TweetsList", "TweetList Retuns null values");
	  	}
	  	else
	  	{
	  		ReportEvents.Reporter("Pass", "TweetsList", "All Data loaded in Web List.");
	  	}
	      for(int i=0;i<49;i++)
	      {
	      //a.get(i).ge
	    	 
	   	  TweetList.add(Tweet.get(i).getText());
	   	  if(!Retweet.get(i).getText().equals(""))
	   	  ReTweetList.add(Retweet.get(i).getText());
	   	 if(!Likes.get(i).getText().equals(""))
	   	  LikesList.add(Likes.get(i).getText());
	    	  
	   	//System.out.println("Tweet: "+ Tweet.get(i).getText()+", Retweet: "+Retweet.get(i).getText()+", Likes: "+Likes.get(i).getText());
	   
	      }
	      
	  	ListLikes = Lists.transform(LikesList, Integer::parseInt);
	   	ListRetweet = Lists.transform(ReTweetList, Integer::parseInt);
	   
	      
	      for(int j= 0 ; j<LikesList.size() ; j++)
	      {
	    	     likes.put(TweetList.get(j), ListLikes.get(j));
	    	    retweets.put(TweetList.get(j), ListRetweet.get(j));
	    	  
	      }
	      
	      
	     
	}
	public int MaxLikes() throws IOException
	{
		int MaxLikes=(Collections.max(likes.values()));  // This will return max value in the Hashmap
	      for (Entry<String, Integer> entry : likes.entrySet()) {  // Itrate through hashmap
	          if (entry.getValue()==MaxLikes) {
	              System.out.println("Maximun Likes are "+MaxLikes+" of this tweet : "+entry.getKey());     // Print the key with max value
	          }
	      }
	      if(MaxLikes>0)
	      {
	      ReportEvents.Reporter("Pass", "MaxLikes", "MaxLikes have value "+MaxLikes+"");
	      }
	      else
	      {
	      ReportEvents.Reporter("Fail", "MaxLikes", "MaxLikes do not have value.");
	      }
	      return MaxLikes;
	}
	public int MaxRetweets() throws IOException
	{
	
		 int MaxRetweets=(Collections.max(retweets.values()));  // This will return max value in the Hashmap
	      for (Entry<String, Integer> entry : retweets.entrySet()) {  // Itrate through hashmap
	          if (entry.getValue()==MaxRetweets) {
	              System.out.println("Maximun Retweets are "+MaxRetweets+" of this tweet : "+entry.getKey());     // Print the key with max value
	          }
	      }
	      if(MaxRetweets>0)
	      {
	      ReportEvents.Reporter("Pass", "MaxRetweets", "MaxTweets have value "+MaxRetweets+"");
	      }
	      else
	      {
	      ReportEvents.Reporter("Fail", "MaxRetweets", "MaxTweets do not have value.");
	      }
	      
	      return MaxRetweets;
	}
	
	public List<String> HashTags() throws InterruptedException, IOException {
		// TODO Auto-generated method stub

		
		      // List<WebElement> a =  driver.findElements(By.xpath("//div[@class='stream']/ol/li"));
		      // List<WebElement> tweetlog =  b.driver.findElements(By.xpath("//div[@class='stream']/ol/li/div[1]/div[2]/div[2]/p[1]"));
		List<WebElement> tweetlog = new Button(b, "tweetlog").fnWebElementsLocation();
		       System.out.println(tweetlog.size());
		       
		       String splitString[];
		       String tweet;
		       
		       int count=0;
		       
		       LinkedHashMap<String,Integer> retweet=new LinkedHashMap<String,Integer>(); 
		String removeExtra;
		       
		       for(int i=0;i<49;i++)
		       {
		       	//a.get(i).ge
		        //System.out.println(a.get(i).getText());
		       	
		       	tweet = tweetlog.get(i).getText().toLowerCase();
		       	
		       	
		       	
		       	splitString = tweet.split(" ");
		       	
		       	for(int j=0;j<splitString.length;j++)
		       	{
		        if(splitString[j].contains("#"))
		       	{
		       	removeExtra = splitString[j].replaceAll("[^a-zA-Z0-9]", "");
		       	
		       	
		       	
		        if(retweet.containsKey(removeExtra))
		       	{
		       	count = retweet.get(removeExtra);
		        retweet.put(removeExtra,count+1);
		       	}
		       	
		       	else	
		       	{
		        retweet.put(removeExtra,1);
		       	}
		       	}
		       	}
		       	
		       	
		       	}
		       
		       System.out.println(retweet);
		       
		       Map<String, Integer> sorted = retweet
		               .entrySet()
		               .stream()
		               .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
		               .collect(
		                   toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
		                       LinkedHashMap::new));
		    
		       System.out.println("Toatl Hashtags in STEPIN page are: " + sorted);
		       Set<String> keys=sorted.keySet();
		       List<String> Top10= new ArrayList<String>();
		       List<String> FinalList = new ArrayList<String>();
		       for(String key:keys)
		       {
		    	   Top10.add(key);
		       }
		       for(int i=0;i<10;i++)
		       {
		    	   FinalList.add(Top10.get(i));
		       }
		     //  List<String> retweetTop10=new ArrayList<>();
		       
		      
		      return Top10;
		       
		       	
		       }
	
	public Map<String,List<String>> Follows() throws InterruptedException, IOException
	{
		List<String> FollowerName = new ArrayList<String>();
		 List<String> HandleName = new ArrayList<String>();
		 List<String> FollowingName = new ArrayList<String>();
		 List<String> FollowerFollowName = new ArrayList<String>();
		 for(int i=1;i<4;i++)
		 {
			 Thread.sleep(2000);
			 b.driver.findElement(By.xpath("//*[@id=\"page-container\"]/div[2]/div/div/div[2]/div/div[3]/div/div/div/div/div/div[2]/div[2]/div["+i+"]/div[2]/a[1]/span[1]/strong[1]")).click();
			 Thread.sleep(2000);
			 //String FolloweNamer=b.driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h1[1]/a[1]")).getText();
			 //String Handle=b.driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/h2[1]/a[1]/span[1]/b[1]")).getText();
			 //String FollowingText=b.driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/ul[1]/li[2]/a[1]/span[3]")).getText();
			 //String FollowingFollName=b.driver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/ul[1]/li[3]/a[1]/span[3]")).getText();
			 String FolloweNamer= new TextBox(b, "FolloweNamer").GetValue();
			 String Handle= new TextBox(b, "Handle").GetValue();
			 String FollowingText= new TextBox(b, "FollowingText").GetValue();
			 String FollowingFollName= new TextBox(b, "FollowingFollName").GetValue();
			 //.out.println(FolloweNamer+Handle+FollowingText+FollowingFollName);
			 FollowerName.add(FolloweNamer);
			 HandleName.add(Handle);
			 FollowingName.add(FollowingText);
			 FollowerFollowName.add(FollowingFollName);
			 Thread.sleep(5000);
			 b.driver.navigate().back();
			 Thread.sleep(5000);
			 
		 }
		 
		 Map<String,List<String>> map =new HashMap();
		 map.put("FollowerName",FollowerName);
		 map.put("HandleName", HandleName);
		 map.put("FollowingName", FollowingName);
		 map.put("FollowerFollowName", FollowerFollowName);
		 
		 return map;
	}
	
	public void JSONCreate(int retweetCount,int likeCount,List<String> l,List<String> l1,List<String> l2,List<String> l3,List<String> l4) throws IOException {
		// TODO Auto-generated method stub
        try{    
            FileWriter fw=new FileWriter( System.getProperty("user.dir")+"\\src\\test\\resources\\test.json");    
            fw.write("{\n\t \"top_retweet_count\":\""+likeCount+"\",\n\t \"top_like_count\":\""+retweetCount+"\",\n\t \"top_10_hashtags\":[\""+l.get(0)+"\",\""+l.get(1)+"\",\""+l.get(2)+"\",\""+l.get(3)+"\",\""+l.get(4)+"\",\""+l.get(5)+"\",\""+l.get(6)+"\",\""+l.get(7)+"\",\""+l.get(8)+"\",\""+l.get(9)+"\"],"
            		+ "\n\t \"biographies\":[\n\t\t{\n\t\t\t\"name\":\""+l1.get(0)+"\"," 
            		+ "\n\t\t\t\"handle_name\":\""+l2.get(0)+"\","
            		+ "\n\t\t\t\"follower_count\":\""+l3.get(0)+"\","
            		+ "\n\t\t\t\"following_count\":\""+l4.get(0)+"\""
            		+ "\n\t\t},"
            		+ "\n\t\t{"
            		+ "\n\t\t\t\"name\":\""+l1.get(1)+"\","
            		+ "\n\t\t\t\"handle_name\":\""+l2.get(1)+"\","
            		+ "\n\t\t\t\"follower_count\":\""+l3.get(1)+"\","
            		+ "\n\t\t\t\"following_count\":\""+l4.get(1)+"\""
            		+ "\n\t\t},"
            		+ "\n\t\t{"
            		+ "\n\t\t\t\"name\":\""+l1.get(2)+"\","
            		+ "\n\t\t\t\"handle_name\":\""+l2.get(2)+"\","
            		+ "\n\t\t\t\"follower_count\":\""+l3.get(2)+"\","
            		+ "\n\t\t\t\"following_count\":\""+l4.get(2)+"\""
            		+ "\n\t\t}"
            		+ "\n\t]\n"
            		+ "}");	            		

            		
            fw.close();    
           }catch(Exception e){
        	   ReportEvents.Error("JSON Creation", e);
        	   }    
        ReportEvents.Reporter("Pass", "JSON Creation", "Test.JSON created successfully.");
             

	} 
	
	public void UploadJSON() throws IOException, InterruptedException
	{
		b.driver.get("https://cgi-lib.berkeley.edu/ex/fup.html");
		p.Wait(4);
		new TextBox(b, "UploadJsonText").SendKeys(System.getProperty("user.dir")+"\\src\\test\\resources\\test.json");
		new TextBox(b,"UploadJsonNotes").SendKeys("Summary of DSTC Output");
		new TextBox(b,"Upload").Click();	
		ReportEvents.Reporter("Pass", "JSON Upload", "JSON File Uploaded successfully.");
	}
	
		       

	}


